require 'rack/utils'
 
class FlashSessionCookieMiddleware
  def initialize(app, session_key = '_session_id')
    @app = app
    @session_key = session_key
  end
 
  def call(env)
  

    if env['HTTP_USER_AGENT'] =~ /^(Adobe|Shockwave) Flash/
      params = ::Rack::Request.new(env).params
      #params = ::Rack::Utils.parse_query(env['QUERY_STRING'])
    #RAILS_DEFAULT_LOGGER.error("debugg: " + env.to_yaml)
      env["HTTP_ACCEPT"] = "application/json" if env["REQUEST_PATH"] =~ /^\/admin\//
      env['HTTP_COOKIE'] = [ @session_key, params[@session_key] ].join('=').freeze unless params[@session_key].nil?
    end
    @app.call(env)
  end
end