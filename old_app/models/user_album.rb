class UserAlbum < ActiveRecord::Base
  validates_presence_of     :title
  validates_length_of       :title,    :within => 1..100
  belongs_to :user
  has_many :user_gallery_photos, :order => "position", :dependent => :destroy
end
