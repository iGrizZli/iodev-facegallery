class Event < ActiveRecord::Base
  belongs_to :place
  localizes :name, :description
  
  default_scope :order => 'date ASC'

  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  named_scope :fgc, :conditions => { :fgc => true }
  named_scope :upcoming, :conditions => "events.date >= DATE(NOW())"
  
  has_attached_file :event_picture, :styles => { :original => ["500x250>", :jpg], :thumbnail => ["240x110^", :jpg] }, :convert_options => { :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity north -extent 240x110" },
                                    :url => "/images/events/:id/:style.:extension",
                                    :path => ":rails_root/public/images/events/:id/:style.:extension"
                                    
  define_index do
    indexes :name
    indexes description
    indexes place.name, :as => :place_name
    indexes place.address, :as => :place_address
    where "events.published_ru = '1'"
    set_property :field_weights => {
      :name => 10,
      :place_name    => 6,
      :place_address    => 4,
      :description    => 2
    }
  end
end
