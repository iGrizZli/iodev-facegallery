class Comment < ActiveRecord::Base
  acts_as_nested_set :scope => [:commentable_id, :commentable_type]

  validates_presence_of :body
  validates_presence_of :user

  belongs_to :commentable, :polymorphic => true
  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  acts_as_voteable

  # NOTE: Comments belong to a user
  belongs_to :user, :class_name => "User"

  has_one :fg_contest_question, :foreign_key => 'winner_comment_id'
  has_one :fg_contest_winner

  named_scope :not_deleted, :conditions => {:deleted => false}
  

  def to_label
    user.surname + ":"+ body[0..70]
  end

  # Helper class method that allows you to build a comment
  # by passing a commentable object, a user_id, and comment text
  # example in readme
  def self.build_from(obj, user_id, comment)
    c = self.new
    c.commentable_id = obj.id
    c.commentable_type = obj.class.name
    c.body = comment
    c.user_id = user_id
    c
  end

  #helper method to check if a comment has children
  def has_children?
    self.children.size > 0
  end

  def destroy(id)
    if id.is_a?(Array)
      id.map { |one_id| destroy(one_id) }
    else
     find(id).update_attribute(:deleted, true)
    end
  end

  def destroy
    update_attribute(:deleted, true)
  end

  # Helper class method to lookup all comments assigned
  # to all commentable types for a given user.
  def self.find_comments_by_user(user)
    find(:all,
      :conditions => ["user_id = ?", user.id],
      :order => "created_at DESC"
    )
  end

  # Helper class method to look up all comments for
  # commentable class name and commentable id.
  def self.find_comments_for_commentable(commentable_str, commentable_id)
    find(:all,
      :conditions => ["commentable_type = ? and commentable_id = ?", commentable_str, commentable_id],
      :order => "created_at DESC"
    )
  end

  # Helper class method to look up a commentable object
  # given the commentable class name and id
  def self.find_commentable(commentable_str, commentable_id)
    commentable_str.constantize.find(commentable_id)
  end
end

