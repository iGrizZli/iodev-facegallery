require 'digest/sha1'

class User < ActiveRecord::Base
  include Authentication
  include Authentication::ByPassword
  include Authentication::ByCookieToken

  #validates_presence_of     :login
  #validates_length_of       :login,    :within => 3..40
  #validates_uniqueness_of   :login
  #validates_format_of       :login,    :with => Authentication.login_regex, :message => Authentication.bad_login_message

  validates_format_of       :name,     :with => Authentication.name_regex,  :message => Authentication.bad_name_message, :allow_nil => true
  validates_length_of       :name,     :within => 1..50, :allow_nil => false
  validates_format_of       :surname,     :with => Authentication.name_regex,  :message => Authentication.bad_name_message, :allow_nil => true
  validates_length_of       :surname,     :within => 1..50, :allow_nil => false

  validates_presence_of     :email
  validates_length_of       :email,    :within => 6..100 #r@a.wk
  validates_uniqueness_of   :email
  validates_format_of       :email,    :with => Authentication.email_regex#, :message => Authentication.bad_email_message
  
  validates_presence_of     :name
  #validates_length_of       :name,    :within => 1..100
  validates_presence_of     :surname
  #validates_length_of       :surname,    :within => 1..100

  validates_inclusion_of :gender, :in => ['m','f',nil]

  before_create :make_activation_code 

  scoped_search :on => [:name, :surname]

  acts_as_voter
  acts_as_voteable

  has_karma :blogs
  has_karma :comments


  auto_strip :name, :surname, :email

  serialize :comments_view_times, Hash

  has_and_belongs_to_many :daily_events
  has_and_belongs_to_many :trips
  has_many :site_invites
  has_many :photo_tags, :as => :person_taggable, :dependent => :destroy
  has_one :givenchy_status
  
  # HACK HACK HACK -- how to do attr_accessible from here?
  # prevents a user from submitting a crafted form that bypasses activation
  # anything else you want your user to change should be added here.
  attr_accessible :email, :name, :surname, :password, :password_confirmation, :city, :phone, :education, :occupation, :interests, :quote, :birthday, :avatar,
                  :profile_access, :friends_access, :portfolio_access, :gallery_access, :blog_access, :show_birthday, :no_message_notifications,
                  :preferred_profile_access,:preferred_friends_access, :preferred_portfolio_access, :preferred_gallery_access, :preferred_blog_access,
                  :prefers_message_notifications, :prefers_blog_comment_notifications, :prefers_photo_comment_notifications, :prefers_bio_comment_notifications, :prefers_comment_comment_notifications, :prefers_user_vote_notifications,
                  :comments_view_times, :gender     

  preference :message_notifications, :default => true
  preference :blog_comment_notifications, :default => true
  preference :photo_comment_notifications, :default => true
  preference :bio_comment_notifications, :default => true
  preference :comment_comment_notifications, :default => true
  preference :user_vote_notifications, :default => true

  preference :profile_access, :integer, :default => 0
  preference :friends_access, :integer, :default => 0
  preference :portfolio_access, :integer, :default => 0
  preference :gallery_access, :integer, :default => 0
  preference :blog_access, :integer, :default => 0

  has_attached_file :avatar, :styles => { :original => ["230x400>", :jpg], :small => ["50x50^", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 ", :small => "-gravity north -extent 50x50 -unsharp 0.3x0.3+2+0.05" },
                             :url => "/images/users/avatars/:id/:style.:extension",
                             :path => ":rails_root/public/images/users/avatars/:id/:style.:extension",
                             :default_url => "/i/default_:style_avatar.png"

  has_one :editor, :dependent => :destroy
  has_one :best_blogger, :dependent => :destroy
  has_one :bio
  has_one :user_portfolio, :dependent => :destroy
  has_many :user_portfolio_photos, :dependent => :destroy
  has_many :user_albums, :dependent => :destroy
  has_many :user_gallery_photos, :through => :user_albums, :dependent => :destroy
  has_many :blogs, :dependent => :destroy
  has_many :messages, :dependent => :destroy
  has_many :sent_messages, :class_name => 'Message', :foreign_key => 'sender_id', :dependent => :destroy
  
  has_many :posts, :dependent => :destroy
  has_many :comments, :foreign_key => "user_id", :dependent => :destroy
  
  has_one :contest_session_as_model, :class_name => "ContestSession", :foreign_key => 'model_id'
  has_one :contest_session_as_photographer, :class_name => "ContestSession", :foreign_key => 'photographer_id'

  has_many :apple_questions

  has_many :videos

  has_many :contest_quote_votes
  has_many :contest_pic_votes
  has_many :contest_pics

  acts_as_network :friends, :through => :friend_invites, :conditions => ["is_accepted = ?", true]

  named_scope :activated, :conditions => {:activation_code => nil}
  named_scope :with_avatar, :conditions => ["NOT ISNULL(avatar_file_name)"]
  named_scope :online, lambda { { :conditions => ["last_activity > ? ", 20.minutes.ago] } }

  define_index do
    indexes [:name, :surname], :as => :user_name
    indexes city
    indexes occupation
    indexes education
    indexes interests
    indexes about
    indexes quote
    
    set_property :field_weights => {
      :user_name => 15,
      :city    => 4,
      :occupation    => 2,
      :education    => 1,
      :interests => 4,
      :about => 2,
      :quote => 2
    }
  end

  def age
    if birthday
      ((Date.today - birthday) / 365).floor
    else
      nil
    end
  end

  def mamba_geo
    if ['Москва', 'москва', 'msc', 'msk', 'Msc', 'Msk', 'Мск', 'мск'].include? city
      :moscow
    else
      :russia
    end
  end

  def has_friends?
    self.friend_count > 0
  end
  
  def update_friend_count
    self.friend_count = self.friends_in.count + self.friends_out.count
    save
  end
  
  # Activates the user in the database.
  def activate!
    @activated = true
    self.activated_at = Time.now.utc
    self.activation_code = nil
    save(false)
  end

  # Returns true if the user has just been activated.
  def recently_activated?
    @activated
  end

  def is_concierge?
    id == 5 or id == 858 or id == 2117
  end

  def active?
    # the existence of an activation code means they have not activated yet
    activation_code.nil?
  end

  def tagged_on?(taggable)
    0 < PhotoTag.count(:all, :conditions => [
      "person_taggable_id = ? AND person_taggable_type = ? AND photo_taggable_id = ? AND photo_taggable_type = ?",
      self.id, self.class.name, taggable.id, taggable.class.name
    ])
  end

  # Authenticates a user by their login name and unencrypted password.  Returns the user or nil.
  #
  # uff.  this is really an authorization, not authentication routine.  
  # We really need a Dispatch Chain here or something.
  # This will also let us return a human error message.
  #
  def self.authenticate(email, password)
    return nil if email.blank? || password.blank?
    u = find :first, :conditions => ['email = ? and activated_at IS NOT NULL', email] # need to get the salt
    u && u.authenticated?(password) ? u : nil
  end

  def login=(value)
    write_attribute :login, (value ? value.downcase : nil)
  end

  def email=(value)
    write_attribute :email, (value ? value.downcase : nil)
  end

  def name=(value)
    write_attribute :name, value
    update_displayname
  end

  def surname=(value)
    write_attribute :surname, value
    update_displayname
  end
  
  def is_online?
    last_activity && (last_activity >= 20.minutes.ago)
  end
  
  def to_label
    displayname
  end
  
  def show_birthday
    self[:show_birthday] || 0
  end
  
  def blog_access
    preferred_blog_access
  end
  
  def friends_access
    preferred_friends_access
  end
  
  def portfolio_access
    preferred_portfolio_access
  end
  
  def gallery_access
    preferred_gallery_access
  end

  def no_message_notifications
    !prefers_message_notifications?
  end
  
  def rating
    (karma/3) + (blogs.count*2) + (comments.count / 5) + self.votes_for
  end

  def calculate_rating
    self.cached_rating = rating
    save
    cached_rating
  end

  def can_vote_for(voteable)
    !voteable.voted_by?(self) &&
    !((voteable.respond_to?(:user) and voteable.user and voteable.user.id == self.id) or (voteable.class == User and voteable == self))
    #true
  end

  def reset_password!
    # First update the password_reset_code before setting the
    # reset_password flag to avoid duplicate email notifications.
    update_attribute(:password_reset_code, nil)
    @reset_password = true
  end

  #used in user_observer
  def recently_forgot_password?
    @forgotten_password
  end
   
  def recently_reset_password?
    @reset_password
  end
  
  def self.find_for_forget(email)
    u = find :first, :conditions => ['email = ? and activated_at IS NOT NULL', email]
    return false if (email.blank? || u.nil?)
    (u.forgot_password && u.save) ? true : false
  end
  
  def forgot_password
    self.make_password_reset_code
    @forgotten_password = true
  end

  def mark_comments_as_read(object)
    self.comments_view_times ||= Hash.new
    self.comments_view_times[object.type.to_s] ||= Hash.new
    self.comments_view_times[object.type.to_s][object.id] = Time.now
    save 
  end

  # Password reset method, only available to SiteUsers
  # yield error, message, path, failure
  def self.find_and_reset_password(password, password_confirmation, reset_code, &block) 
    u = find :first, :conditions => ['password_reset_code = ?', reset_code]
    case 
    when (reset_code.blank? || u.nil?)
      yield :error, "Неверный код сброса пароля, пожалуйста, проверьте почту и попробуйте еще раз.", "root_path", true
    when (password.blank? || (password != password_confirmation))
      yield :error, "Введенные пароли не совпадают.", nil, false
    else
      u.password = password
      u.password_confirmation = password_confirmation
      if u.save
        u.reset_password!
        yield :notice, "Новый пароль сохранен. Пожалуйста, авторизуйтесь.", "login_path", false
      else
        yield :error, "Возникла проблема при сохранении пароля.", "root_path", false
      end       
    end         
  end

   def provides_access(acc)
      access = self.send(acc)
      result = true
      unless current_user == self
        if access > 0
          if current_user
           case
            when access == 1
              result = false unless (self.friends.include?(current_user) || current_user.bio)
            when access == 2
              result = false unless self.friends.include?(current_user)
            else
              result = false
            end
          else
            result = false
          end
        end
      end
     result
    end


  def has_bio?
    Bio.exists?(:user_id => id)
  end

  def is_admin?
    AdminUser.exists?(:user_id => id)
  end

  def self.set_gender_from_bio!
    bios = Bio.find(:all, :conditions => ['NOT(ISNULL(gender)) AND NOT (ISNULL(user_id))'])
    bios.each do |b|
      if b.user
        b.user.gender = b.gender
        b.user.save
      end
    end
  end

  protected

    def make_activation_code
        self.activation_code = self.class.make_token unless self.activated_at
    end
    
    def make_password_reset_code
        self.password_reset_code = self.class.make_token
    end

    def update_displayname
      self.displayname =  [self.name.to_s, self.surname.to_s].compact.join(" ")
    end
  
  private
  
    #def after_initialize
    #  self.preferences ||= Hash.new
    #end
end
