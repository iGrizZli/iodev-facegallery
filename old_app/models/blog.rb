class Blog < ActiveRecord::Base
  #include ActionView::Helpers::SanitizeHelper
  
  require 'rubygems'
  require 'sanitize'

  acts_as_taggable
  acts_as_commentable
  acts_as_voteable

  validates_presence_of     :title
  validates_length_of       :title,    :within => 1..255
  validates_presence_of     :text
  
  default_scope :order => 'created_at DESC'
  named_scope :public, :joins => 'INNER JOIN users ON (users.id=blogs.user_id)',  :conditions => 'blog_access < 1 OR ISNULL(blog_access)'
  belongs_to :user
  has_one :fg_guest

  define_index do
    indexes title
    indexes text
    indexes [user.name, user.surname], :as => :user_name
    set_property :field_weights => {
      :title => 10,
      :user_name    => 6,
      :text    => 5,
    }
  end

  def to_label
    title[0..101]
  end

  def text=(new_text)
    #write_attribute :text, HTML::WhiteListSanitizer.new.sanitize(new_text)
    
    transformer =  lambda do |env|
      node      = env[:node]
      node_name = env[:node].name.downcase
      parent    = node.parent
  
      # Since the transformer receives the deepest nodes first, we look for a
      # <param> element or an <embed> element whose parent is an <object>.
      return nil unless (node_name == 'param' || node_name == 'embed') &&
          parent.name.to_s.downcase == 'object'
  
      if node_name == 'param'
        # Quick XPath search to find the <param> node that contains the video URL.
        return nil unless movie_node = parent.search('param[@name="movie"]')[0]
        url = movie_node['value']
      else
        # Since this is an <embed>, the video URL is in the "src" attribute. No
        # extra work needed.
        url = node['src']
      end
      # Verify that the video URL is actually a valid YouTube video URL.
      return nil unless url =~ /^http:\/\/(?:www\.)?youtube\.com\/v\// or url =~ /^http:\/\/(?:video\.)?rutube\.ru/ or url =~ /^http:\/\/(?:www\.)?vimeo\.com/ or url =~ /^http:\/\/(?:www\.)?vogue\.ru/
      # We're now certain that this is a YouTube embed, but we still need to run
      # it through a special Sanitize step to ensure that no unwanted elements or
      # attributes that don't belong in a YouTube embed can sneak in.
      Sanitize.clean_node!(parent, {
        :elements   => ['embed', 'object', 'param'],
        :attributes => {
          'embed'  => ['allowfullscreen', 'allowscriptaccess', 'height', 'src', 'type', 'width'],
          'object' => ['height', 'width'],
          'param'  => ['name', 'value']
        }
      })
  
      # Now that we're sure that this is a valid YouTube embed and that there are
      # no unwanted elements or attributes hidden inside it, we can tell Sanitize
      # to whitelist the current node (<param> or <embed>) and its parent
      # (<object>).
      {:whitelist_nodes => [node, parent]}
    end

    
    #sanitize and remove comments also
    write_attribute :text,  Sanitize.clean(new_text.gsub(/<!--.*?-->/m,""), Sanitize::Config::RELAXED.merge({:transformers => transformer}) ).gsub(/<p>\w*?<\/p>/,"")
  end

  def text_stripped_tags
    ActionController::Base.helpers.strip_tags(text.gsub(/<br[\s\/]*>/, " "))
  end
  def text_shortened(count = 350, end_string = ' ...')
    string = text_stripped_tags
    string = string.gsub(/&#13;/, "")
    if string.length >= count
        shortened = string[0, count]
        splitted = shortened.split(/\s/)
        words = splitted.length
        splitted[0, words-1].join(" ") + end_string
    else
        string
    end

  end

  def self.find_most_commented(limit=3, offset=0, daysago=3)
    Blog.public.find(:all, :conditions => ['blogs.created_at > ?', daysago.days.ago], :select => 'count(*) as comment_count, blogs.*',:joins => :comments, :group => 'blogs.id', :order => 'comment_count DESC', :limit => limit, :offset => offset)
  end
  
  def self.most_commented
    Blog.public.find(:first, :conditions => ['blogs.created_at > ?', 1.days.ago], :select => 'count(*) as comment_count, blogs.*',:joins => :comments, :group => 'blogs.id', :order => 'comment_count DESC')
  end
  
  def self.find_friends_blogs(user)
    Blog.find(:all, :conditions => ["user_id in (?)", user.friends.collect(&:id)], :include => :user)
  end

  def self.find_latest_editor_blog
    Blog.find(:first, :conditions => ["user_id in (?)", Editor.active.collect(&:user_id)], :include => :user)
  end
  
end
