class BioMailer < ActionMailer::Base
  

  def site_invite(user)

    setup_email(user)
    content_type "text/html"
    @subject    += "[report] Фотоотчет с поэтического вечера Олега Груза"
    
  end
  
  def event(user)

    setup_email(user)
    content_type  "multipart/mixed"
    part "text/html" do |p|
      p.body = render_message 'event', {}
    end
    @subject    += 'FG ART. Fashion is My Profession'
    
  end
  
  def register_invite(user)
    setup_email(user)
    content_type "text/html"
    @subject    += "BIO / Приглашение на сайт"
    @body[:url]  = "http://#{SITE_URL}/bio_signup/#{user.invite_hash}"
  end
  
  protected
    def setup_email(user)
      @recipients  = "#{user.email}"
      #@recipients  = "needto@gmail.com"
      @from        = "info@facegallery.ru"
      @subject     = "FaceGallery.ru - "
      @sent_on     = Time.now
      @body[:user] = user
    end

   
end
