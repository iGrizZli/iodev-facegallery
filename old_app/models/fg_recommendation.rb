class FgRecommendation < ActiveRecord::Base
  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  has_attached_file :image1, :styles => { :original => ["230x150^", :jpg] }, :convert_options => { :original => "-gravity north -extent 230x150" },
                                    :url => "/images/fg_recommendations/:id/:style_1.:extension",
                                    :path => ":rails_root/public/images/fg_recommendations/:id/:style_1.:extension"

  has_attached_file :image2, :styles => { :original => ["230x150^", :jpg] }, :convert_options => { :original => "-gravity north -extent 230x150" },
                                    :url => "/images/fg_recommendations/:id/:style_2.:extension",
                                    :path => ":rails_root/public/images/fg_recommendations/:id/:style_2.:extension"
end
