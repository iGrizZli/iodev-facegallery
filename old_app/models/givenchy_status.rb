class GivenchyStatus < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :text
  validates_length_of :text, :within => 1..250
end
