class ContestSession < ActiveRecord::Base
  has_many :contest_photos
  #acts_as_list
  acts_as_voteable
  belongs_to :photographer, :class_name => "User"
  belongs_to :model, :class_name => "User"
  has_attached_file :photo, :styles => { :original => ["1024x1024>", :jpg], :smaller => ["450x450>", :jpg], :small => ["120x150>", :jpg], :thumbnail => ["75x75^", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 ", :small => "-unsharp 0.3x0.3+5+0", :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity north -extent 75x75" },
                                :url => "/images/contests/talenthunt/:id.:style.:extension",
                                :path => ":rails_root/public/images/contests/talenthunt/:id.:style.:extension"
                                
  def current_user_can_vote?
    return false #golosovanie okoncheno
    if current_user and !self.voted_by?(current_user) and current_user.votes.find(:all, :conditions => {:voteable_type => "ContestSession"}).length < 2
      return true
    else
      return false
    end
    false
  end
  
  def self.votes_remaining
    if current_user
      2 - current_user.votes.find(:all, :conditions => {:voteable_type => "ContestSession"}).length
    end
  end
end
