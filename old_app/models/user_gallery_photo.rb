class UserGalleryPhoto < ActiveRecord::Base
  belongs_to :user_album
  belongs_to :user
  acts_as_list :scope => :user_album
  has_attached_file :photo, :styles => { :original => ["1024x1024>", :jpg], :smaller => ["450x450>", :jpg], :small => ["120x150>", :jpg], :thumbnail => ["75x75^", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 ", :small => "-unsharp 0.3x0.3+5+0", :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity north -extent 75x75" },
                                :url => "/images/users/galleries/:user_id/:user_album_id/:id.:style.:extension",
                                :path => ":rails_root/public/images/users/galleries/:user_id/:user_album_id/:id.:style.:extension"
  validates_attachment_presence :photo
  
  acts_as_commentable
  
  named_scope :latest, :order => "created_at DESC"

  has_many :photo_tags, :as => :photo_taggable, :dependent => :destroy

  def title
    read_attribute(:title) || ""
  end
end
