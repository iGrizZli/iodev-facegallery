class RegisterApplication < ActiveRecord::Base
  has_attached_file :photo, :styles => { :original => ["1024x768>", :jpg], :thumbnail => ["150x150", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 " },
                             :url => "/images/register_applications/:id/:style.:extension",
                             :path => ":rails_root/public/images/register_applications/:id/:style.:extension"
  validates_attachment_content_type :photo, :content_type=>['image/jpeg', 'image/png', 'image/gif'], :message => 'должно быть формата jpeg, gif или png'
  
  validates_presence_of     :name
  validates_presence_of     :surname
  

  validates_presence_of     :email
  validates_length_of       :email,    :within => 6..100 #r@a.wk
end
