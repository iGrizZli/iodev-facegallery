class MagazineInterview < ActiveRecord::Base
  belongs_to :author, :class_name => 'User', :foreign_key => 'author_id'
  acts_as_commentable
  
  localizes :name, :title, :preface, :text, :quote
  default_scope :order => 'created_at DESC'  
  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  
  named_scope :current_issue, lambda { { :conditions => { :magazine_issue_id => (I18n.locale.to_s == "ru" ? MAGAZINE_ISSUE : MAGAZINE_ISSUE_EN) } } }
  named_scope :past_issues, lambda { { :conditions => [ "magazine_issue_id != ?", (I18n.locale.to_s == "ru" ? MAGAZINE_ISSUE : MAGAZINE_ISSUE_EN) ] } }
  
  has_attached_file :interview_photo, :styles => { :original => ["850x520>", :jpg], :small => ["250x250^", :jpg], :thumbnail => ["115x140^", :jpg], :mainpage => ["110x110^", :jpg] }, :convert_options => { :small => "-gravity north -extent 250x250" , :thumbnail => "-gravity north -extent 115x140",  :mainpage => "-unsharp 0.3x0.3+5+0 -gravity north -extent 110x110" },
                            :url => "/images/interviews/:id/:style.:extension",
                            :path => ":rails_root/public/images/interviews/:id/:style.:extension"
  belongs_to :magazine_section
  belongs_to :magazine_issue
  has_many :article_images
  
  define_index do
    indexes :name
    indexes title
    indexes preface
    indexes text
    where "published_ru = '1'"
    
    set_property :field_weights => {
      :name => 10,
      :title    => 6,
      :text => 5,
      :preface => 5
    }
  end
  
  def magazine_section_id
    2
  end
  def magazine_section
    MagazineSection.find(2)
  end

  def pages_in_article
    text.scan(/\[page\s*?(\d+)\s*?\]/m).size  
  end
    
  def text_page(page_number)
    if pages_in_article > 0
      page_number = page_number || 1
      res = ""
      text.scan(/\[page\s*?(\d+)\s*?\](.+?)\[\/page\]/m){|page, text| res = text if page.to_i == page_number.to_i}
      res
    else
      text
    end
  end

  def to_param
    "#{id}-#{name.parameterize}"
  end
end
