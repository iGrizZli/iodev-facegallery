class Place < ActiveRecord::Base
  belongs_to :place_category
  has_many :place_photos
  has_many :events

  has_many :daily_events
  has_many :place_addresses

  localizes :name, :description, :address, :phone, :food, :working_hours, :parking
  has_many :news_items
  has_many :reports
  #default_scope :conditions => {"published_#{I18n.locale}".to_sym => true}
  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  named_scope :fgc, :conditions => { :fgc => true }
  
  has_attached_file :main_photo, :styles => { :original => ["230x400>", :jpg], :thumbnail => ["50x50>", :jpg] }, :convert_options => { :thumbnail => "-unsharp 0.3x0.3+4+0" },
                                 :url => "/images/places/:id/:style.:extension",
                                 :path => ":rails_root/public/images/places/:id/:style.:extension"
                                 
  define_index do
    indexes :name
    indexes description
    indexes address
    where "published_ru = '1'"
    
    set_property :field_weights => {
      :name => 10,
      :description    => 6,
      :address    => 6
    }
  end
  
  def to_param
    "#{id}-#{name.parameterize}"
  end

end
