class FgSurveyAnswer < ActiveRecord::Base
  serialize :answers
  
  
  private
  
  def after_initialize
    self.answers ||= Hash.new
  end
end
