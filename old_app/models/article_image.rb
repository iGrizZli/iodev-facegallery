class ArticleImage < ActiveRecord::Base
  belongs_to :magazine_article
  belongs_to :magazine_interview
  
  has_attached_file :image, :styles => { :original => ["1000x1000>", :jpg], :thumbnail => ["230x200>", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 -unsharp 0.3x0.3+2+0.05" },
                            :url => "/images/magazine/articles/:magazine_article_id/images/:id_:style.:extension",
                            :path => ":rails_root/public/images/magazine/articles/:magazine_article_id/images/:id_:style.:extension"
  validates_attachment_presence :image
end
