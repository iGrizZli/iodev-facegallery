class Bio < ActiveRecord::Base
  belongs_to :user
  belongs_to :bio_category
  belongs_to :manager, :class_name => "User"
  belongs_to :author, :class_name => "User"
  belongs_to :company
  has_many :photo_tags, :as => :person_taggable
  has_many :videos
  has_one :contest_quote
  
  acts_as_commentable

  localizes :name, :surname, :quote, :text, :occupation

  acts_as_voteable

  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  named_scope :random, :order => 'rand()'
  named_scope :last_two_weeks, lambda { { :conditions => ["created_at > ?", 14.days.ago ] } }
  named_scope :last_week, lambda { { :conditions => ["created_at > ?", 7.days.ago ] } }
  
  named_scope :facecontrolled, :conditions => {:not_on_main_page => nil}
  
  # TODO: add 170x290
  # TODO: add 100x100
  has_attached_file :photo, :styles => { :original => ["850x520>", :jpg], :small => ["230x340>", :jpg], :smaller => ["120x200>", :jpg], :thumbnail => ["80x80^", :jpg], :thumb80 => ["80x100^", :jpg], :thumb70 => ["70x70^", :jpg], :thumb150 => ["150x150^", :jpg], :thumb180 => ["180x180^", :jpg] }, :convert_options => { :small => "-strip -quality 88 -unsharp 0.3x0.3+4+0", :smaller => "-strip -quality 80 -unsharp 0.3x0.3+4+0", :thumbnail => "-gravity north -extent 80x80 -strip -quality 86 -unsharp 0.3x0.3+4+0", :thumb80 => "-gravity north -extent 80x100 -strip -quality 80 -unsharp 0.3x0.3+4+0", :thumb70 => "-gravity north -extent 70x70 -strip -quality 80 -unsharp 0.3x0.3+4+0", :thumb150 => "-gravity north -extent 150x150 -strip -quality 85 -unsharp 0.3x0.3+4+0", :thumb180 => "-gravity north -extent 180x180 -strip -quality 85 -unsharp 0.3x0.3+4+0" },
                            :url => "/images/bio/:id/:style.:extension",
                            :path => ":rails_root/public/images/bio/:id/:style.:extension"
  validates_presence_of :surname
  validates_presence_of :bio_category
  
  before_create :make_invite_hash
  #after_create :process_user_invite
  #before_save :process_user_invite

  define_index do
    indexes [:name, :surname], :as => :bio_name
    indexes text
    indexes occupation
    indexes bio_category.name, :as => :category 
    where "published_ru = '1'"
    
    set_property :field_weights => {
      :bio_name => 15,
      :text    => 6,
      :category    => 4,
      :occupation => 3
    }
  end

  def to_label
    displayname
  end

  def to_param
    "#{card_id}-#{displayname.parameterize}"
  end
  
  def displayname
    [self.name, self.surname].compact.join(" ")
  end
  
  def connect_to_user(user)
    self.user = user
    self.invite_hash = nil
  end
  
  #protected

  def make_invite_hash
    self.invite_hash = Digest::SHA1.hexdigest([Time.now, (1..10).map{ rand.to_s }].flatten.join('--')) unless self.user
  end
  
  def process_user_invite
    if self.user && self.invite_hash
      self.invite_hash = nil
    elsif self.invite_hash && self.sent_invite != true && self.email
      BioMailer.deliver_register_invite(self)
      RAILS_DEFAULT_LOGGER.error("Sent bio site invite to " + self.email.to_s)
      self.sent_invite = true
    end
  end

end

