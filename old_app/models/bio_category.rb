class BioCategory < ActiveRecord::Base
  has_many :bios
  localizes :name
  default_scope :order => 'position'

  def to_param
    "#{urlname}"
  end
end
