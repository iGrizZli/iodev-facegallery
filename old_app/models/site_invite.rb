class SiteInvite < ActiveRecord::Base
  belongs_to :user
  
  named_scope :unused, :conditions => { :used => false }
  validates_presence_of :email
  
  before_create :make_invite_code
  
  def make_invite_code
    self.code = Digest::SHA1.hexdigest([Time.now, (1..10).map{ rand.to_s }].flatten.join('--'))
  end
end
