class BarentzCategory < ActiveRecord::Base
	has_many :barentz_articles
	default_scope :order => 'position ASC'
  acts_as_list
  named_scope :visible, :conditions => { :visible => true }
end
