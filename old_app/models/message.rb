class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :sender, :class_name => 'User'
  
  default_scope :order => 'created_at DESC'
  named_scope :unread, :conditions => "ISNULL(messages.read)"
  named_scope :need_to_notify, :conditions => "messages.notify_by_email='1'"
  named_scope :not_deleted_by_user, :conditions => "ISNULL(`messages`.deleted_by_user)"
  named_scope :not_deleted_by_sender, :conditions => "ISNULL(`messages`.deleted_by_sender)"
  
  
  def self.find_history(user_id, sender_id) #owner goes first
    Message.find(:all, :conditions => ["(user_id = ? AND sender_id = ? AND ISNULL(deleted_by_user)) OR (sender_id = ? AND user_id = ? AND ISNULL(deleted_by_sender))", user_id, sender_id, user_id, sender_id])
  end
end
