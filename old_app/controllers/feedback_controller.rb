class FeedbackController < ApplicationController
  def index
    if request.post? || request.xhr?
      @message = FeedbackMessage.new(params[:message])
      @message.user = current_user
      @message.ip = request.remote_ip
      @message.browser = request.env["HTTP_USER_AGENT"]
      if @message.save
#        flash[:notice] = 'Новый альбом создан!'
        redirect_to url_for(:action => 'sent')
      else
        flash[:error] = 'Ошибка при отправке сообщения'
        redirect_to url_for(:action => 'index')
      end
      
    end
  end

  def sent
  end

end
