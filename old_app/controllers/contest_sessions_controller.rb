class ContestSessionsController < ApplicationController
  def index
    @sessions = ContestSession.find(:all)
    @page_title = "FG Talent Hunting"
  end

  def vote
    @session = ContestSession.find(params[:id])
    if @session.current_user_can_vote?
      current_user.vote_for(@session)
      flash[:notice] = "Спасибо, Ваш голос принят!"
    else
      flash[:notice] = "Голос не учтен"
    end
    redirect_to :back
  end
end
