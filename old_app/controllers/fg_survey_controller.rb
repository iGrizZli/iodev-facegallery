class FgSurveyController < ApplicationController
  
  def index
    @questions = FgSurveyQuestion.paginate :page => params[:q], :per_page => 5
    
    unless session[:fg_survey_id1]
      @survey = FgSurveyAnswer.new
      @survey.user_id = current_user.id if current_user
      @survey.session_id = session.session_id
      if @survey.save
        session[:fg_survey_id1] = @survey.id
      else
        RAILS_DEFAULT_LOGGER.error("failed to initialize survey somehow")
      end
    end
    
    @survey = FgSurveyAnswer.find_by_id session[:fg_survey_id1]
    if request.post? && session[:fg_survey_id1]
      
      if @survey
        @survey.answers ||= Hash.new
        params[:fg_survey].each do |key, value|
          @survey.answers[key.to_sym] = value
        end
        @survey.save
      else
        RAILS_DEFAULT_LOGGER.error("failed to find survey from session")
      end
    end
    
    session[:fg_survey_id1] = nil if @questions.current_page > @questions.total_pages
  end
end
