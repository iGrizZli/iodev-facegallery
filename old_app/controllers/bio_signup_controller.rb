class BioSignupController < ApplicationController
  before_filter :init_signup
  
  def index
    @user = User.new
  end
  
  def create
    @user = User.new(params[:user])
    @user.activated_at = Time.now.utc
    @user.activation_code = nil
    success = @user && @user.save
    if success && @user.errors.empty?
      self.current_user = @user
      @bio.connect_to_user @user
      @bio.save
      flash[:notice] = "Ваш BIO теперь связан с Вашим профайлом!"
      redirect_to '/welcome'
    else
      render :action => 'index'
    end
  end
  
  def connect_current_user
    if current_user
      @bio.connect_to_user current_user
      @bio.save
      flash[:notice] = "Ваш BIO теперь связан с Вашим профайлом!"
    else
      flash[:notice] = "Произошла ошибка при связи BIO с профайлом!"
    end
    redirect_to user_path(current_user)
  end
  
  def check_login
    user = User.authenticate(params[:email], params[:password])
    if user
      self.current_user = user
      @bio.connect_to_user user
      @bio.save
      flash[:notice] = "Ваш BIO теперь связан с Вашим профайлом!"
      #redirect_to user_path(@user)
      @login_error = false
      respond_to do |format|
        format.html { redirect_to user_path(user) }
        format.js
      end
    else
      @login_error = true
      respond_to do |format|
        format.html { redirect_back_or_default('/') }
        format.js
      end
    end
    
  end
  
  def init_signup
    @bio = Bio.find_by_invite_hash(params[:invite_hash]) unless params[:invite_hash].blank?
    unless @bio
      flash[:notice] = "Такой код не найден, возможно BIO уже связано с существующим профайлом"
      redirect_to '/'
    end
  end
end
