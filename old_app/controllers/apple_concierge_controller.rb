class AppleConciergeController < ApplicationController
  before_filter :check_concierge, :only => [:update, :destroy]

  def index
    @questions = AppleQuestion.find(:all, :conditions => {:visible => true}, :order => 'created_at DESC')
  end

  def create
    @question = AppleQuestion.new(params[:apple_question])
    @question.user = current_user
    if @question.save
      flash[:notice] = "Спасибо за ваш вопрос!"
      UserMailer.deliver_concierge_notification(@question)
      redirect_to apple_questions_path
    else
      flash[:notice] = "Произошла ошибка при отправке вопроса"
      redirect_to apple_questions_path
    end
  end

  def update
    @question = AppleQuestion.find(params[:id])
    if @question.update_attributes(params[:apple_question])
      flash[:notice] = "Ответ размещен!"
      redirect_to apple_questions_path
    else
      flash[:notice] = "Произошла ошибка при размещении ответа"
      redirect_to apple_questions_path
    end
  end

  def destroy
    @question = AppleQuestion.find(params[:id])
    if @question.destroy
      flash[:notice] = "Вопрос удален"
      redirect_to apple_questions_path
    else
      flash[:notice] = "Произошла ошибка при удалении вопроса"
      redirect_to apple_questions_path
    end
  end

  def check_concierge
    current_user && current_user.is_concierge? || access_denied
  end
end
