class DocumentsController < ApplicationController
  def view
    @document = Document.find_by_alias(params[:alias])
    @page_title = "#{@document.name} " + @page_title.to_s
    if @document.alias == "bio_howto" && current_user && !current_user.has_bio?
      current_user.update_attribute(:bio_wished, Time.now)
    end
  end

end
