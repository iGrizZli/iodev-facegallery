class CommentsController < ApplicationController
  before_filter :login_required

  def new
    commentable_type = params[:commentable][:commentable]
    commentable_id = params[:commentable][:commentable_id]
    if (!params[:comment][:body].to_str.strip.empty?) 
      # Get the object that you want to comment
      commentable = Comment.find_commentable(commentable_type, commentable_id)
    
      # Create a comment with the user submitted content
      @comment = Comment.build_from(commentable, current_user.id, params[:comment][:body])


      #store id of the commentable's owner
      if commentable.respond_to?(:user) and commentable.user
        @comment.commentable_user_id = commentable.user.id
      end

      if @comment.save
        parent_comment_notification = false
        if params[:comment][:parent_id] && params[:comment][:parent_id].to_i > 0
          @parent_comment = Comment.find(params[:comment][:parent_id])
          @comment.move_to_child_of(@parent_comment)
          if(@parent_comment.user.prefers_comment_comment_notifications && (@parent_comment.user_id != current_user.id))
            UserMailer.deliver_comment_comment_notification(commentable, @comment, @parent_comment)
            #RAILS_DEFAULT_LOGGER.error("comment answer notification sent to #{@parent_comment.user.email}")
            parent_comment_notification = true
          end
        end
        # Email notification
        if commentable.respond_to?('user') && commentable.user != current_user && !(parent_comment_notification &&  @parent_comment.user_id == commentable.user.id)

          case commentable.class.to_s
            when "Blog"

             # RAILS_DEFAULT_LOGGER.error("blog comment notification sent to #{commentable.user.email}")
              UserMailer.deliver_blog_comment_notification(commentable, @comment) if commentable.user.prefers_blog_comment_notifications
            when "UserGalleryPhoto"
              UserMailer.deliver_gallery_photo_comment_notification(commentable, @comment)  if commentable.user.prefers_photo_comment_notifications
              #RAILS_DEFAULT_LOGGER.error("photo comment notification sent to #{commentable.user.email}")
            when "Bio"
              UserMailer.deliver_bio_comment_notification(commentable, @comment)  if commentable.user.prefers_bio_comment_notifications
              #RAILS_DEFAULT_LOGGER.error("bio comment notification sent to #{commentable.user.email}")
          end
        end
      end
      render :layout=>false
    else
      render :nothing=>true
    end

  end

  def delete
    @comment = Comment.find_by_id params[:id]
    if @comment.user_id == current_user.id
      @comment.destroy
    end
  end

end
