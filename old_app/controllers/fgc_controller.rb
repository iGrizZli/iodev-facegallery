class FgcController < ApplicationController
  def index
    @news = NewsItem.published.find(:all, :limit => 2)
    @top_event = DailyEvent.upcoming.fgc.published.find :first
    @events = DailyEvent.upcoming.fgc.published.find :all, :offset => 1, :limit => 2
    @top_report = Report.fgc.published.find :first
    @reports = Report.fgc.published.find :all, :offset => 1, :limit => 2
  end

  def card
    @fgc_text = Document.find_by_alias('fgc')
  end

  def events
    @events = DailyEvent.published.fgc.upcoming.find(:all)
  end

  def reports
    @reports = Report.published.fgc.paginate :page => params[:page], :order => 'reports.date DESC', :per_page => 6
  end

  #def places
  #  unless params[:place_category]
  #    @place_categories = PlaceCategory.find(:all, :select => 'place_categories.name, place_categories.urlname, count(*) as place_count', :joins => 'INNER JOIN places ON (places.place_category_id = place_categories.id)', :conditions => 'places.fgc = 1', :group => 'place_categories.id')
  #  else
  #    @current_category = PlaceCategory.find_by_urlname(params[:place_category])
  #    @places = Place.published.fgc.find(:all, :conditions => {:place_category_id => @current_category})
  #  end
  #end

  def fgroom
    if params[:fg_room_question] || request.post?
      @fgq = FgRoomQuestion.new
      @fgq.user = current_user if current_user
      @fgq.update_attributes(params[:fg_room_question])
    end
    @events = Event.upcoming.find_all_by_place_id(326)
    @reports = Report.find_all_by_place_id(326)
    @page_title = "FG ROOM"
  end

  def no_discount
    if params[:no_discount]
      @nd = NoDiscount.create(params[:no_discount])
      @nd.user_id = current_user.id if current_user
      @nd.save
    end
  end
  
  def submit_wish
    if params[:no_discount]
      @nd = FgcWish.create(params[:no_discount])
      @nd.user_id = current_user.id if current_user
      @nd.save
    end
  end

  def news

  end

  def join

  end
end

