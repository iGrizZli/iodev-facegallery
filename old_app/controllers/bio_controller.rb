class BioController < ApplicationController
  before_filter :init_face_gallery
  before_filter :init_fg_contest
  before_filter :add_this_button, :only => :show
  before_filter :login_required, :only => :vote

  def index
    @bios = Bio.published.last_two_weeks.random.find(:all, :limit => (@contest_question ? 1 : 3))
    @bios = Bio.published.random.find(:all, :limit => (@contest_question ? 1 : 3)) if @bios.empty?
    #@bios = Bio.published.random.find(:all, :limit => 3)
    @contest_text = Document.find_by_alias('contest_info')

    @top_bios = Bio.tally(
      {  :at_least => 1,
          :at_most => 10000,
          :limit => 10,
          :order => "votes.count desc"
      })

    #contest
    commentable_type = "FgContestQuestion"
    commentable_id = params[:contest_id]
    if (request.post? and !params[:comment][:body].to_str.strip.empty?)
      # Get the object that you want to comment
      commentable = Comment.find_commentable(commentable_type, commentable_id)

      # Create a comment with the user submitted content
      @comment = Comment.new(params[:comment])
      # Assign this comment to the logged in user
      @comment.user_id = current_user.id

      # Add the comment
      commentable.comments << @comment

      redirect_to :back
    end
  end
  
  def list
    if params[:bio_category]
      @current_category = BioCategory.find_by_urlname params[:bio_category]
      conditions = ["bios.bio_category_id = ?", @current_category.id ]
      @current_category_urlname = @custom_body_class = @current_category.urlname

      @page_title = "#{@current_category.name} " + @page_title.to_s
    end
    
    @bios = Bio.published.paginate :page => params[:page], :order => 'bios.surname', :per_page => 20, :conditions => conditions 
  end

  def show
    @bio = Bio.published.find_by_card_id(params[:id]) or raise ActiveRecord::RecordNotFound 
    @page_title = "#{@bio.displayname} / #{@bio.bio_category.name} " + @page_title.to_s
    if request.xhr?
      render :partial => "bio_for_index", :locals => { :bio => @bio }
    else
      @more_bio = Bio.published.find_all_by_bio_category_id(@bio.bio_category_id, :order => 'rand()', :limit => 3, :conditions => "id != #{@bio.id}")
    end
  end
  
  def vote
    @bio = Bio.published.find_by_card_id(params[:id]) or raise ActiveRecord::RecordNotFound 
    unless @bio.voted_by?(current_user)
      current_user.vote_for(@bio)
      flash[:notice] = "Спасибо, Ваш голос принят!"
    end
    redirect_to bio_path(:id => @bio, :bio_category => @bio.bio_category)
  end
  
  def init_face_gallery
    #@page_title = @page_title.to_s
    @bio_categories = BioCategory.find(:all, :order => 'position')

      #@latest_comments = Comment.not_deleted.find(:all, :conditions => ["comments.commentable_type = 'Bio'"], :order => "comments.id DESC", :limit => 10)
    
  end
  
  def init_fg_contest
    @contest_question = FgContestQuestion.published.find(:all, :limit => 1).first
  end
end
