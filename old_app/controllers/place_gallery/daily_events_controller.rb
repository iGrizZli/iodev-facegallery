class PlaceGallery::DailyEventsController < ApplicationController
  def index
    @date = params[:date] || Date.today
    @events = DailyEvent.published.find(:all, :conditions => ["date LIKE ?", "#{@date}%"])
    @top_events = Event.published.find(:all, :conditions => ["date LIKE ?", "#{@date}%"])
    if params[:id]
      @event = DailyEvent.find(params[:id])
    end
    check_jw
  end

  def tooltip
    @events = DailyEvent.published.find(:all, :conditions => ["date LIKE ?", "#{params[:date]}%"])
    render :layout => false
  end

  def show
    respond_to do |wants|
      wants.html{
        @event = DailyEvent.find(params[:id], :include => :users)
        check_jw
      }
      wants.js {
        @event = DailyEvent.find(params[:id], :include => :users)
        check_jw
      }
    end
  end


  def toggle_user_event
    if current_user
      @event = current_user.daily_events.find_by_id params[:id]
      if @event
        current_user.daily_events.delete(@event)
      else
        @event = DailyEvent.find(params[:id])
        current_user.daily_events << @event if @event
        added_event = true
      end
      respond_to do |wants|
      	wants.js {
      		if added_event
      		  render :update do |page|
						  page << "$('li#daily_event_#{params[:id]}').addClass('go');"
                          page << "$('span#event-users-count-#{params[:id]}').text('#{@event.users.count}');"
                          page.replace_html "event_info#{@event.id}".to_s, :partial => 'event_info', :locals => {:event => @event}
						end
      		else
      		  render :update do |page|
						  page << "$('li#daily_event_#{params[:id]}').removeClass('go');"
                          page << "$('span#event-users-count-#{params[:id]}').text('#{@event.users.count}');"
                          page.replace_html "event_info#{@event.id}".to_s, :partial => 'event_info', :locals => {:event =>  @event}
						end
      	  end
      	}
      	wants.html{
      	  if request.headers['HTTP_REFERER'].present?
      	    redirect_to :back
    	    else
    	      redirect_to :controller => 'place_gallery/daily_events', :action => 'show', :id => @event.id
  	      end
      	}
      end
    end
  end
  
  def check_jw
    if (@events && @events.any? {|e| e.id == 3179}) || (@event && @event.id == 3179)
      @jmpage = true
    else
      @jmpage = false
    end

  end
end
