class PlaceGallery::EventsController < PlaceGallery::PlaceGalleryController
  before_filter :add_this_button, :only => :show
  
  def index
    if params[:place_category]
      @current_category = PlaceCategory.find_by_urlname params[:place_category]
      joins = :place
      conditions = ["places.place_category_id = ?", @current_category.id]
      @current_category_urlname = @current_category.urlname
      @page_title = "#{@current_category.name} / " + @page_title.to_s
    end

    @top_event = Event.upcoming.published.find :first
    @events = Event.upcoming.published.paginate :page => params[:page], :order => 'events.date ASC', :per_page => 6, :joins => joins, :conditions => ["events.id != ?", @top_event.id]
  end
  
  def show
    @event = DailyEvent.find(params[:id])
    @page_title = "#{@event.name} / " + @page_title.to_s
  end
  
end
