class PlaceGallery::IndexController < ApplicationController
  def index
    @top_event = Event.upcoming.published.find :first
    @events = Event.upcoming.published.find :all, :offset => 1, :limit => 2

    @top_report = Report.published.find :first
    @reports = Report.published.find :all, :offset => 1, :limit => 2
  end

end
