class PlaceGallery::ReportsController < ApplicationController
  before_filter :add_this_button, :only => :show
  before_filter :init_reports

  def index
    if params[:place_category]
      @current_category = PlaceCategory.find_by_urlname params[:place_category]
      joins = :place
      conditions = ["places.place_category_id = ?", @current_category.id]
      @current_category_urlname = @current_category.urlname
      @page_title = "#{@current_category.name} / " + @page_title.to_s
    end

    @top_report = Report.published.find :first
    @reports = Report.published.paginate :page => params[:page], :order => 'reports.date DESC', :per_page => 6, :joins => joins, :conditions => ["reports.id != ?", @top_report.id]
  end
  
  def show
    @report = Report.find(params[:id])
    @page_title = "#{@report.name} / " + @page_title.to_s

    get_users_on_event
  end

  def view_image
    @report = Report.find(params[:id])
    @current_image = @report.report_photos.find(params[:image_id])
    @page_title = "#{@report.name} / " + @page_title.to_s
    get_users_on_event
  end

  def init_reports
    @page_title = "Светская хроника / Magazine" + @page_title.to_s
  end

  def get_users_on_event
    photo_ids = @report.report_photos.map(&:id)
    photo_tag_ids = PhotoTag.find(:all, :conditions => ["photo_taggable_id in (?) AND photo_taggable_type = 'ReportPhoto'", photo_ids]).map(&:person_taggable_id)
    @users_on_event = User.find(:all, :conditions => ["id in (?)", photo_tag_ids ])
  end
end
