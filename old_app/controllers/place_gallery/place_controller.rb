class PlaceGallery::PlaceController < PlaceGallery::PlaceGalleryController
  before_filter :load_place
  before_filter :add_this_button, :only => [:show, :gallery]
    
  def show
    @events = DailyEvent.upcoming.published.find_all_by_place_id @place.id, :limit => 3
    @reports = Report.published.find_all_by_place_id @place.id, :limit => 3
    @news = @place.news_items.published.find :all, :limit => 2
  end

  def pan
    @page_title = "Панорама: " + @page_title.to_s
  end

  def gallery
    @page_title = "Галерея: " + @page_title.to_s
    @photos = @place.place_photos
  end

  def news
    @news = @place.news_items.published.paginate :page => params[:page], :per_page => 10
  end

  def events
    @page_title = "Афиша: " + @page_title.to_s
    @events = DailyEvent.upcoming.published.paginate_by_place_id @place.id, :page => params[:page], :per_page => 10
  end

  def reports
    @page_title = "Репортажи: " + @page_title.to_s
    @reports = Report.published.paginate_by_place_id @place.id, :page => params[:page], :per_page => 10
  end

  private
  
  def load_place
    @place = Place.published.find(params[:id])
    @page_title = "#{@place.name} / #{@place.place_category.name} / " + @page_title.to_s
  end
end
