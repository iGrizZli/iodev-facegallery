class PlaceGallery::PlaceGalleryController < ApplicationController
  before_filter :init_place_gallery
  
  def index

    if params[:place_category]
      @current_category = PlaceCategory.find_by_urlname params[:place_category]
      conditions = ["places.place_category_id = ?", @current_category.id ]
      @current_category_urlname = @current_category.urlname
      @page_title = "#{@current_category.name} / " + @page_title.to_s
      @places = Place.fgc.published.paginate :page => params[:page], :order => 'places.fgc DESC, places.name ASC', :per_page => 12, :conditions => conditions     
    else
      render :action => 'places_front'
    end
  
    
  end



  def init_place_gallery
    @page_title = "Place Gallery" + @page_title.to_s
    @place_categories = PlaceCategory.find(:all)
  end
end
