class ContestSessions::ContestPhotosController < ApplicationController
  def show
    @session = ContestSession.find(params[:contest_session_id])
    @photo = @session.contest_photos.find(params[:id])
    @other_photos = @session.contest_photos.find(:all, :conditions => ["contest_photos.id != ?", params[:id]])
    @page_title = "Фотограф: #{@session.photographer.displayname} / FG Talent Hunting"
  end

end
