class FgTalentHuntController < ApplicationController
  def index
    @page_title = "Fg Talent Hunting part 2"
    @pics = ContestPic.all
  end

  def vote
    @pic = ContestPic.find(params[:pic_id])
    if cookies[:qptc] != "d" && cookies[:qptc] != @pic.id.to_s && (ContestPicVote.count(:conditions => { :ip => request.headers["HTTP_X_FORWARDED_FOR"] }) < 8 )
      vote = @pic.contest_pic_votes.new
      vote.user_id = current_user.id if logged_in?
      vote.ip = request.headers["HTTP_X_FORWARDED_FOR"]
      cookie_val = cookies[:qptc].present? ? "d" : @pic.id.to_s
      cookies[:qptc] = { :value => cookie_val, :expires => (Date.today + 1.day).to_time}
      
      if vote.save
        
        flash[:notice] = 'Спасибо за Ваш голос!'
      else
        flash[:error] = 'Произошла ошибка'
      end
    else
      flash[:error] = 'Извините, вы уже голосовали'
    end
    redirect_to :action => :index
  end
end
