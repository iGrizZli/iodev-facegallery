class PhotoTagsController < ApplicationController
  def new
    @taggable = PhotoTag.find_photo_taggable(params[:photo_tag][:photo_taggable_type], params[:photo_tag][:photo_taggable_id])
    @person = PhotoTag.find_person_taggable(params[:photo_tag][:person_taggable_type], params[:photo_tag][:person_taggable_id])
    if @taggable and @person and current_user and not @person.tagged_on?(@taggable)
      @tag = PhotoTag.new(params[:photo_tag])
      @tag.tagger_id = current_user.id
      @tag.displayname = @person.displayname
      @tag.save
    end
  end

  def taggable_users_list
    if current_user.is_admin?
      @users = User.activated.find(:all)
    else
      @users = current_user.friends
    end
    render :layout => false
  end

  def delete
    #TODO: complete
    @tag = PhotoTag.find(params[:id])
    if @tag.person_taggable == current_user or @tag.tagger_id == current_user.id
      @tag.destroy
    end
    redirect_to :back
  end
end
