class VideosController < ApplicationController
  def index
    @videos = Video.active.all :order => 'created_at DESC'
  end

  def show
    @video = Video.active.find params[:id]
  end

end
