class Fgc::NewsController < ApplicationController
  def index
    @news = NewsItem.published.paginate :all, :page => params[:page], :per_page => 10
  end

  def show
    @news_item = NewsItem.find(params[:id])
  end

end
