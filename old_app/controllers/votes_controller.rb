class VotesController < ApplicationController
  #before_filter :login_required
  def new
    voteable_type = params[:voteable_type]
    voteable_id = params[:voteable_id]
    @voteable = voteable_type.constantize.find(voteable_id)
    @partial = @voteable.is_a?(User) ? 'user/vote' : 'votes/vote'

    flash[:ajax] = "За это вы уже голосовали" if @voteable.voted_by?(current_user)

    if logged_in?
      if current_user.can_vote_for @voteable
        if params[:vote].to_i == 1
          current_user.vote_for(@voteable)

          #email notification for users
          if @voteable.is_a?(User) and @voteable.prefers_user_vote_notifications
            UserMailer.deliver_user_vote_notification(@voteable, current_user)
          end
        elsif params[:vote].to_i == 0
          current_user.vote_against(@voteable)
        end
      else
        flash[:ajax] = "Вы не можете голосовать за свои посты и комментарии" if (@voteable.respond_to?(:user) and @voteable.user and @voteable.user.id == current_user.id) or (@voteable.class == User and @voteable == current_user)
      end
    else
      flash[:ajax] = "Чтобы голосовать, необходимо авторизоваться!"
    end
  end
end
