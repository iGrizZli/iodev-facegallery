class PasswordsController < ApplicationController
  def new
  end

  def create
    if User.find_for_forget(params[:email])
      flash[:notice] = "Ссылка для сброса пароля отправлена на Ваш email."
      redirect_to root_path
    else
      flash.now[:notice] = "Сбросить пароль не удалось, возможно пользователь с таким email не найден."
      render :action => 'new'
    end 
  end

  def edit
    if params[:id].nil?
      flash[:error] = "Не найден код для сброса пароля."
      redirect_to root_path
    end
  end

  def update
    render :text => 'test'
    User.find_and_reset_password(params[:password], params[:password_confirmation],
    params[:reset_code]) do |error, message, path, failure|
      if path
        #UserFailure.record_failure(request.remote_ip,
        #request.env['HTTP_USER_AGENT'], "passwordreset", nil) if failure
        flash[error] = message
        redirect_to send(path)
      else
        flash.now[error] = message
        render :action => 'edit', :id => params[:id]
      end
    end
  end

end
