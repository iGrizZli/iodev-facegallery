class User::BlogController < UserController
    before_filter :owner_check, :only => [ :edit, :create, :destroy, :update, :new ]
    before_filter :add_this_button, :only => :show
    before_filter :init_blog
    before_filter :login_required, :except => [ :index, :show ]

    before_filter do |c|
      c.access_check("blog_access")
    end
    
    # GET messages_url
    def index
      # return all messages
      @blog = @user.blogs.paginate :page => params[:page], :per_page => 10
    end

    # GET new_message_url
    def new
      # return an HTML form for describing a new message
      @page_title = @page_title.to_s + " / Новый пост"
    end

    # POST messages_url
    def create
      @blog = @user.blogs.build(params[:blog])
      if @blog.save
        flash[:notice] = 'Запись создана!'
        redirect_to blog_url(:id => @blog.id, :user_id => @blog.user_id)
      else
        flash[:error] = 'Не удалось создать запись в блоге'
        redirect_to url_for(:user_id => @blog.user_id, :controller => 'user/blog')
      end

    end

    # GET message_url(:id => 1)
    def show
      @blog = @user.blogs.find(params[:id])
      @page_title = @blog.title + " / " + @page_title.to_s
    end

    # GET edit_message_url(:id => 1)
    def edit
      @blog = @user.blogs.find(params[:id])
      @page_title = @page_title.to_s + " / Редактирование поста"
    end

    # PUT message_url(:id => 1)
    def update
      # find and update a specific message
      @blog = @user.blogs.find(params[:id])
      @blog.update_attributes(params[:blog])
      if @blog.save
        flash[:notice] = 'Запись сохранена!'
      end
      redirect_to blog_url(:id => @blog.id, :user_id => @blog.user_id)
    end

    # DELETE message_url(:id => 1)
    def destroy
      # delete a specific message
      @blog = @user.blogs.find(params[:id])
      @blog.destroy
      redirect_to blogs_url
    end

    def init_blog
      @page_title = @page_title.to_s + " / Блог"
    end

end
