class User::PortfolioController < UserController
  #session :cookie_only => false, :only => :upload_photo
  #skip_before_filter :verify_authenticity_token, :only => [:upload_photo]
  before_filter :login_required, :only => [ :edit, :upload_photo, :sort_photos, :delete_photo ]
  before_filter :owner_check, :only => [ :edit, :upload_photo, :sort_photos, :delete_photo ]
	before_filter :add_this_button, :only => :index
	  
  before_filter do |c|
    c.access_check("portfolio_access")
  end

  before_filter :init_portfolio

  def index
    unless @portfolio.nil?
      @photos = @portfolio.user_portfolio_photos
    end
    @page_title = @page_title.to_s + " / Портфолио"
  end

  def edit
    unless @portfolio.nil?
      @photos = @portfolio.user_portfolio_photos
    end
    if request.post?
      if @portfolio.nil?
#        @portfolio = UserPortfolio.new
#        @portfolio.user = @user
      end
      @portfolio.update_attributes(params[:user_portfolio])
      redirect_to (url_for :action => 'index')
    end
    @page_title = @page_title.to_s + " / Редактирование портфолио"
  end
  
  def upload_photo
    if request.post?
      
      @photo = @portfolio.user_portfolio_photos.new(params[:photo])
      @photo.user_portfolio = @portfolio
      @photo.user = @user
      if @photo.save
        flash[:notice] = 'Фотография загружена!'
      else
        flash[:error] = 'Не удалось загрузить фотографию'
      end
      
    end

    redirect_to (url_for :action => 'edit')
  end
  
  def sort_photos
 #   params[:sortable_photos].each_with_index do |id, index|
#      @portfolio.user_portfolio_photos.update_all(['position=?', index+1], ['id=?', id])
      @portfolio.user_portfolio_photos.update_all(['position=FIND_IN_SET(id,?)', params[:sortable_photos].join(',')], :id => params[:sortable_photos])
#   end
    render :nothing => true
  end
  
  def delete_photo
    @photo = @portfolio.user_portfolio_photos.find_by_id(params[:id])
    if @photo && @photo.destroy
      flash[:notice] = 'Фотография удалена!'
    else 
      flash[:error] = 'Произошла ошибка при удалении фотографии';
    end
    redirect_to (url_for :action => 'edit')
  end


  def init_portfolio
    @portfolio = @user.user_portfolio

    if !request.get? && @portfolio.nil? && is_owner?
      @portfolio = UserPortfolio.new
      @portfolio.user = @user
      @portfolio.save
    end
  end
  
end
