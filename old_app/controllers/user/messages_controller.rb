class User::MessagesController < UserController
    before_filter :login_required
    before_filter :owner_check, :only => [:index, :new, :create, :destroy, :set_read ]
    after_filter :set_read, :only => :index

    # GET messages_url
    def index
      # return all messages
      if !params[:sent]
        @messages = @user.messages.not_deleted_by_user.paginate :page => params[:page], :per_page => 15
        @view_sent = false
        @page_title = @page_title.to_s + " / Входящие сообщения"
      else
        @messages = @user.sent_messages.not_deleted_by_sender.paginate :page => params[:page], :per_page => 15
        @view_sent = true
        @page_title = @page_title.to_s + " / Отправленные сообщения"
      end
    end

    # GET new_message_url
    def new
      # return an HTML form for describing a new message
      @recipient = User.find_by_id(params[:to])
      @history_messages = Message.find_history(@user.id, @recipient.id).paginate :page => params[:page], :per_page => 15
      @page_title = @page_title.to_s + " / Новое сообщение"
    end

    # POST messages_url
    def create
      # create a new message
      @recipient = User.find_by_id(params[:to])
      if @recipient
        @message = Message.new;
        @message.attributes = params[:message]
        @message.sender = @user;
        @message.user_id = params[:to]
        @message.notify_by_email = 1 #unless @recipient.is_online?
        @message.deleted_by_user = 1 if @user.id == 1336
      end

      if @message.save
        flash[:notice] = 'Сообщение отправлено!'
      end
      redirect_to messages_url
    end

    # GET message_url(:id => 1)
    def show

    end

    # GET edit_message_url(:id => 1)
    def edit

    end

    # PUT message_url(:id => 1)
    def update

    end

    # DELETE message_url(:id => 1)
    def destroy
      # delete a specific message
      @message = Message.find_by_id(params[:id], :conditions => ["user_id = ? OR sender_id = ?", @user.id, @user.id])
      if @message.user == @user
        @message.deleted_by_user = 1        
      elsif @message.sender == @user
        @message.deleted_by_sender = 1
      end
      @message.save
      #@message.destroy
      redirect_to :back
    end
    
    private
    def set_read
      Message.update_all "messages.read='1', messages.notify_by_email='0'", [ "user_id = ?", @user.id ]
    end
    
    def owner_check
      (logged_in? && is_owner?) || access_denied
    end
end

