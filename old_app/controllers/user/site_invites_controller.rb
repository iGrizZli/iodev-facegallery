class User::SiteInvitesController < UserController
  before_filter :owner_check
  before_filter :check_bio
  
  def index
    @page_title = @page_title.to_s + " / Пригласить на сайт"
  end
  
  def create
    @invite = current_user.site_invites.create(params[:site_invite])
    if @invite && @invite.save
      UserMailer.deliver_site_invite(@invite)
      flash[:messages] = "Приглашение к регистрации выслано на #{@invite.email}"
      redirect_to current_user
    end 
  end
  
  
  def check_bio
    access_denied unless current_user.is_admin?#current_user.bio && current_user.is_admin?
  end
end
