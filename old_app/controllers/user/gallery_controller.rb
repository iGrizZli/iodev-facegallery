class User::GalleryController < UserController
  before_filter :owner_check, :except => [ :index, :view_album, :view_image ]
  before_filter :login_required, :except => [ :index, :view_album, :view_image ]
  before_filter :add_this_button, :only => [:view_album, :view_image]
  before_filter :init_gallery

  before_filter do |c|
    c.access_check("gallery_access")
  end
    
  before_filter :load_album
  
  def index
    @photos = @user.user_gallery_photos.latest.find(:all, :limit => 12)

  end
  
  def create_album
        #RAILS_DEFAULT_LOGGER.error('own:')
    if request.post?  
      @album = current_user.user_albums.build(params[:user_album])
      if @album.save
        flash[:notice] = 'Новый альбом создан!'
        redirect_to user_album_path(:id => @album.id, :user_id => @user.id)
      else
        flash[:error] = 'Не удалось создать альбом'
        redirect_to url_for(:user_id => @user.id, :controller => 'user/gallery')
      end
      
    end


  end
  
  def view_album
    if request.put? && (@album.user == current_user)
      @album.update_attributes(params[:user_album])
      render :json => @album
    elsif request.xhr? && (@album.user == current_user)
      render :json => @album
    end
  end
  
  def view_image
    @album.user_gallery_photos.each_with_index do |p, index|
      if p.id == params[:image_id].to_i
        @current_image = p
        @current_image_index = index
        @prev_image = @album.user_gallery_photos[index > 0 ? index-1 : @album.user_gallery_photos.length-1]
        @next_image = @album.user_gallery_photos[index < @album.user_gallery_photos.length ? index+1 : 0]
      end
    end
    if request.put? && (@album.user == current_user)
      @current_image.update_attributes(params[:user_gallery_photo])
      render :json => @current_image
    elsif request.xhr? && (@album.user == current_user)
      render :json => @current_image
    end
  end
  
  def upload_photo
     if request.post? 
      @photo = @album.user_gallery_photos.build(params[:photo])
      @photo.user = @user
      if @photo.save
        flash[:notice] = 'Фотография загружена!'
      else 
        flash[:error] = 'Произошла ошибка при загрузке';
      end
      
    end

    redirect_to user_album_path(:id => @album.id, :user_id => @user.id)
  end
  
  def delete_image
    if request.post?
      @photo = @album.user_gallery_photos.find(params[:image_id])
      if @photo.destroy
        flash[:notice] = 'Фотография удалена!'
      else
        flash[:error] = 'Произошла ошибка при удалении фотографии';
      end
      redirect_to user_album_path
    end
  end

  def delete_album
    if request.post?
      if @album.destroy
        flash[:notice] = 'Альбом удален!'
      else
        flash[:error] = 'Произошла ошибка при удалении альбома';
      end
      redirect_to :action => 'index'
    end
  end

  def load_album
    if params[:id]
      @album = @user.user_albums.find(params[:id])
      @page_title = @album.title + " / " + @page_title.to_s
    end
  end

  def init_gallery
    @page_title = @page_title.to_s + " / Галерея"
  end
end
