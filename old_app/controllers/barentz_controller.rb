class BarentzController < ApplicationController
  
  before_filter :set_logo
  
  def index
    if params[:category]
      @current_category = BarentzCategory.find_by_urlname params[:category]
      conditions = { :barentz_category_id => @current_category.id }
    end
    
    @articles = BarentzArticle.published.paginate :conditions => conditions, :page => params[:page], :per_page => 6
    @page_title = "Bio Expedition"
  end

  def show
    @article = BarentzArticle.find(params[:id])
    @page_title = @article.title + " - Bio Expedition"
  end
  
  def set_logo
    @logo_class = "variant-ne"
  end
end
