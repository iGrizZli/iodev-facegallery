class CommunityController < ApplicationController
  before_filter :init_community
  
  def index
    #@top_video = Video.active.find :first, :order => 'created_at DESC'
    #@videos = Video.active.find :all, :limit => 4, :offset => 1, :order => 'created_at DESC'
    #@videos = Video.active.find :all, :limit => 4,  :order => 'created_at DESC'

    @top_blogs = Blog.find_most_commented(3,0,1)
    @blogs = Blog.public.find(:all, :limit => 3, :order => 'created_at DESC', :conditions => ["blogs.id NOT IN(?)", @top_blogs.collect(&:id)])

    @users = User.with_avatar.activated.find(:all, :limit => 3, :order => 'activated_at DESC')
    @news = NewsItem.published.find(:all, :limit => 2)

    @best_bloggers = BestBlogger.active.find(:all, :limit => 4)
    @editors_blog = Blog.find_latest_editor_blog

    #latest_albums = UserGalleryPhoto.find(:all, :limit => 8, :select => 'DISTINCT user_album_id', :order => 'user_gallery_photos.id DESC', :joins => 'INNER JOIN users ON (users.id=user_gallery_photos.user_id AND (users.gallery_access < 1 OR ISNULL(users.gallery_access)))')
    latest_albums = UserGalleryPhoto.find(:all, :limit => 4, :select => 'DISTINCT user_album_id', :order => 'user_gallery_photos.id DESC', :joins => 'LEFT JOIN preferences ON (preferences.owner_id=user_gallery_photos.user_id AND preferences.name = \'gallery_access\')', :conditions => ["preferences.value < 1 OR ISNULL(preferences.value)"])
    @user_albums = UserAlbum.find(:all, :conditions => ["id in (?)", latest_albums.collect(&:user_album_id)]).reverse

    @fg_recommends = FgRecommendation.published.last
    @latest_comments = Comment.not_deleted.find(:all, :conditions => ["comments.commentable_type != 'ContestPhoto'"], :order => "comments.id DESC", :limit => 10)

    @top_users = User.find :all, :order => 'cached_rating DESC', :limit => 10, :conditions => 'users.id NOT IN(996,9,10,16,622,8,41,1935)'
    @fg_guest = FgGuest.published.last
    
    @barentz_articles = BarentzArticle.published.all :limit => 2
  end

  def blogs
    case params[:subaction]
    when "most_commented"
      @blogs = Blog.find_most_commented(15).paginate :page => params[:page], :per_page => 8
    when "friends"
      @blogs = Blog.find_friends_blogs(current_user).paginate :page => params[:page], :per_page => 8
    else
      @blogs = Blog.public.paginate :page => params[:page], :per_page => 8
    end
    
    
    #@blogs = Blog.find(:all, :conditions => ["user_id in (?)", current_user.friends.collect(&:id)], :include => :user).paginate :page => params[:page]
    #@blogs = Blog.find_friends_blogs(current_user).paginate :page => params[:page]

  end

  def users
    @users = User.activated.search_for(params[:q]).paginate :page => params[:page], :per_page => 18, :order => 'activated_at DESC'
  end

  def user_rating
    @users = User.activated.paginate :order => 'cached_rating DESC', :page => params[:page], :per_page => 20, :conditions => 'users.id NOT IN(996,9,10,16,622,8,41,1935)'
  end

  def newyear_contest
    @page_title = 'Новогодний конкурс'
  end

  def init_community
    @page_title = "Community" + @page_title.to_s
  end

end
