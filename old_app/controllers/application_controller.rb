# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  # Be sure to include AuthenticationSystem in Application Controller instead
  include AuthenticatedSystem

  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details
  before_filter :set_locale
  
  before_filter :track_online_user
  
  before_filter :set_jw
  
  def set_jw
    if params[:controller] == 'welcome' || (params[:controller] == 'fgc' && params[:action] == 'index')
      @logovar = 'fourth'
      @jwpage = true 
    else
      @logovar = 'second'
      @jwpage = false
    end
  end
  #before_filter :close_site 
  # Scrub sensitive parameters from your log
  filter_parameter_logging :password
  def set_locale
    if request.subdomains.first.to_s == 'en'
      I18n.locale = :en
    else
      I18n.locale = :ru
     end
  end

  def track_online_user
    if current_user
      current_user.last_activity = Time.now
      current_user.save
      #RAILS_DEFAULT_LOGGER.info("User: #{current_user.id} #{current_user.displayname} ip: #{request.env['HTTP_X_FORWARDED_FOR']}")
      if current_user.id == 1138 or current_user.id ==996 or current_user.id==736 or current_user.id==1700 or current_user.id == 1095 or current_user.id == 2257 or current_user.id == 2348 

        RAILS_DEFAULT_LOGGER.error("YIPPIE KI YAY MOTHERFUCKER " + current_user.id.to_s)
        logout_keeping_session!
      end
    end
  end

  def close_site
  	if self.class.to_s != 'SessionsController' && !logged_in?
  		render :text => '<html><head><title>502 Bad Gateway</title></head><body bgcolor="white"><center><h1>502 Bad Gateway</h1></center><hr><center>nginx/0.6.88</center></body></html>'
  	end
 end
 
 def add_this_button
   @addthis_display = true
 end
end
