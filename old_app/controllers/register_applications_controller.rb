class RegisterApplicationsController < ApplicationController
  def index

  end

  def create
    @register_application = RegisterApplication.new(params[:register_application])
    success = @register_application && verify_recaptcha(:model => @register_application, :message => "Вы неправильно ввели символы с картинки, попробуйте еще раз") && @register_application.save
    if success && @register_application.errors.empty?
      redirect_to(:action => 'created')
    else
      render :action => 'index'
    end
  end

  def created
    
  end
end
