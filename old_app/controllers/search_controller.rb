class SearchController < ApplicationController
  def index
    searched_class = case params[:search]
    when 'magazine'
      MagazineArticle
    when 'blogs'
      Blog
    when 'posts'
      Post
    when 'users'
      User
    when 'bio'
      Bio
    when 'places'
      Place
    when 'events'
      Event
    when 'reports'
      Report
    else ThinkingSphinx
    end
    @results = searched_class.search params[:q], :page => params[:page], :per_page => 20
  end

end
