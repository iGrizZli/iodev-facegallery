class ContestQuotesController < ApplicationController
  def index
    @quotes = ContestQuote.all :conditions => {:quote_contest_id => 2}
    @winner = ContestQuote.find_by_winner true, :conditions => {:quote_contest_id => 2}
  end

  def vote
    @quote = ContestQuote.find(params[:quote_id])
    unless cookies[:qvt] == "1"
      vote = @quote.contest_quote_votes.new
      vote.user_id = current_user.id if logged_in?
      vote.ip = request.headers["HTTP_X_FORWARDED_FOR"]
      cookies[:qvt] = { :value => "1", :expires => (Date.today + 1.day).to_time}
      
      if vote.save
        flash[:notice] = 'Спасибо за Ваш голос!'
      else
        flash[:error] = 'Произошла ошибка'
      end
    else
      flash[:error] = 'Извините, вы уже голосовали'
    end
    redirect_to :action => :index
  end
end
