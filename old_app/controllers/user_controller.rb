class UserController < ApplicationController
  before_filter :load_user
  before_filter :login_required, :except => [:index, :friends, :photo_tags]#[ :delete_friend, :invite_friend, :edit_profile, :edit_photo, :messages ]
  before_filter :owner_check, :except => [:index, :show, :friends, :invite_friend, :delete_friend, :photo_tags]#[ :preferences, :accept_invite, :delete_invite, :edit_profile, :edit_photo, :messages ]
  
  before_filter :only => [:friends, :friend_blogs] do |c|
    c.access_check("friends_access")
  end 
  
  def index
    @latest_blog = @user.blogs.find(:first) if @user.provides_access(:blog_access)
    @latest_portfolio_photos = @user.user_portfolio_photos.find(:all, :limit => 3) if @user.provides_access(:portfolio_access)
    @latest_photos = @user.user_gallery_photos.latest.find(:all, :limit => 3) if @user.provides_access(:gallery_access)
    if @user.provides_access(:friends_access)
      @friends_blogs = Blog.find_friends_blogs(@user).paginate :page => params[:page], :per_page => 1
      @friends_online = @user.friends.find(:all, :conditions => ["last_activity > ?", 20.minutes.ago]) #TODO: fix bug with no online users
      @friends = @user.friends.find(:all, :order => "RAND()", :limit => 3, :conditions => ( @friends_online.length > 0 ? ["users.id NOT IN(?)", @friends_online.collect(&:id)] : nil) )
      @friend_invites = FriendInvite.find_all_by_user_id_target(current_user.id, :conditions => "is_accepted IS NULL") if current_user and is_owner?
    end
    @daily_events = @user.daily_events.upcoming
    @latest_comments = Comment.not_deleted.find(:all, :conditions => ["comments.commentable_user_id = ?", @user.id], :order => "comments.created_at DESC", :limit => 10)
    @photo_tags = @user.photo_tags.find(:all, :limit => 3, :order => 'id DESC')
  end

  def edit_profile
    if request.post?
      @user.update_attributes(params[:user])
      flash[:notice] = 'Изменения профайла сохранены!'
      redirect_to user_path(@user)
    end
    @page_title = @page_title.to_s + " / Редактирование профайла"
  end

  def edit_photo
    if request.post?
      @user.update_attributes(params[:user])
      flash[:notice] = 'Фотография загружена!'
      redirect_to user_path(@user)
    end
  end
  
  def delete_photo
      @user.avatar.destroy
      @user.save
      redirect_to user_path(@user)
  end

  def photo_tags
    @photo_tags = @user.photo_tags.paginate :page => params[:page], :per_page => 12, :order => 'id DESC'
  end

  def preferences
    if request.post?
      @user.update_attributes(params[:user])
      flash[:notice] = 'Настройки сохранены!'
      redirect_to user_path(@user)
    end
    @page_title = @page_title.to_s + " / Блоги друзей"
  end

  def friend_blogs
    @friends_blogs = Blog.find_friends_blogs(@user).paginate :page => params[:page], :per_page => 8
    @page_title = @page_title.to_s + " / Блоги друзей"
  end

  def friends
    @invites = FriendInvite.find_all_by_user_id_target(current_user.id, :conditions => "is_accepted IS NULL") if current_user
    if params[:online].to_i == 1
      @friends = @user.friends.select {|f| f.is_online? }.paginate
    else
      @friends = @user.friends.paginate :page => params[:page], :per_page => 18
    end
    @page_title = @page_title.to_s + " / Друзья"
  end

  def invite_friend
    if logged_in? and !@user.friends.include? current_user
       invite = FriendInvite.find_or_create_by_user_id_and_user_id_target(:user_id => current_user.id, :user_id_target => @user.id, :message => params[:message])
    else

    end
    
    if !request.xhr?
      redirect_to @user
    end
  end


  def delete_friend
    if @user.friends.include? current_user
        @invite = FriendInvite.find_by_user_id_and_user_id_target(current_user.id, @user.id)
      if !@invite 
        @invite = FriendInvite.find_by_user_id_and_user_id_target(@user.id, current_user.id)
      end
      if @invite
        @invite.destroy
        @user.update_friend_count
        current_user.update_friend_count
      end
    end
    
    if !request.xhr?
      redirect_to @user
    end
  end
  
  def accept_invite
     @invite = FriendInvite.find_by_user_id_and_user_id_target(params[:friend_id], current_user.id)
     if @invite
       @invite.is_accepted = true
       @invite.save
       @invite.user.update_friend_count
       @invite.user_target.update_friend_count
     end
     redirect_to user_path(@user, :action => 'friends')
  end

  def delete_invite
      @invite = FriendInvite.find_by_user_id_and_user_id_target(current_user.id, params[:friend_id])
      if !@invite 
        @invite = FriendInvite.find_by_user_id_and_user_id_target(params[:friend_id], current_user.id)
      end
      if @invite
        @invite.destroy
      end
     redirect_to @user  
  end

  def load_user
    @user = User.find(params[:user_id] || params[:id])
    @page_title = "#{@user.displayname}" + @page_title.to_s
    authorized?
  end
  
  def save_givenchy
    unless @user.givenchy_status
      GivenchyStatus.create :text => params[:status], :user_id => @user.id if is_owner?
    end
          redirect_to @user
  end
  
  #def authorized?
    #doublecheck stuff
    #(current_user && @user.id == current_user.id) || access_denied
  #end
  def is_owner?
    current_user && (@user.id == current_user.id)
  end
  
  def owner_check
    logged_in? && (is_owner? || current_user.is_admin? || access_denied)
  end


    def access_check(acc)
      @user.provides_access(acc) || access_denied
    end
end
