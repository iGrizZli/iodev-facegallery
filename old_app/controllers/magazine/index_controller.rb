class Magazine::IndexController < ApplicationController
  before_filter :init_magazine
  before_filter :add_this_button, :only => [:show, :interview_show]
  
  def index
    @interviews = MagazineInterview.published.find(:all, :limit => 4)
    @issue = MagazineIssue.find(I18n.locale.to_s == "ru" ? MAGAZINE_ISSUE : MAGAZINE_ISSUE_EN)
    @fusion = MagazineArticle.published.find_all_by_magazine_section_id(3, :limit => 5)
    @fashion = MagazineArticle.published.find_all_by_magazine_section_id(4, :limit => 5)
    @beauty_section = MagazineSection.find_by_id(5)
    @travel = MagazineArticle.published.find_all_by_magazine_section_id(6, :limit => 5)
    @choice = MagazineArticle.published.find_all_by_magazine_section_id(7, :limit => 5)
    @bio_authors = MagazineArticle.published.find_all_by_magazine_section_id(8, :limit => 5)
    @letter = Document.find_by_alias("editors_letter")
  end

  def editors_letter
    @letter = Document.find_by_alias("editors_letter")
    @page_title = "#{@letter.name} /" + @page_title.to_s
  end

  def list
    if @current_section
      @articles = MagazineArticle.published.paginate_by_magazine_section_id(@current_section.id, :page => params[:page], :per_page => 6)
      #@past_articles = MagazineArticle.published.past_issues.find_all_by_magazine_section_id(@current_section.id)
      @custom_body_class = @current_section.urlname
      template = case @current_section.urlname
         when "news" then "news_index"
      end
      
      respond_to do |format|
        format.html {
          if template 
            render :action => template
          end
        }
        format.xml { render :layout => false }
      end
    end    
  end

  
  def interview_index
    @interviews = MagazineInterview.published.paginate(:all, :page => params[:page], :per_page => 12)
    @main_interviews = MagazineInterview.published.current_issue.find(:all, :limit => 4)
    @past_interviews = MagazineInterview.published.past_issues.find(:all, :offset => 4)
  end
  
  def interview_show
    @interview = MagazineInterview.find(params[:id])
    @more_interviews = MagazineInterview.published.find(:all, :limit => 4)
    @more_interviews.delete_if {|a| a.id==@interview.id}
    @article_pages = [].paginate :page => params[:page], :total_entries => @interview.pages_in_article, :per_page => 1
    
    @page_title = "#{@interview.name} / " + @page_title.to_s
  end
  
  def xml
    render :layout => false
    if @current_section
      @articles = MagazineArticle.published.find_all_by_magazine_section_id(@current_section.id)
    end 
  end
  
  def show
    @article = MagazineArticle.find(params[:id])
    @more_articles = MagazineArticle.published.find_all_by_magazine_section_id(@article.magazine_section_id, :limit => 6, :conditions => ["magazine_articles.id != ?", @article.id])
    @latest_comments = Comment.not_deleted.find(:all, :conditions => ["comments.commentable_type = 'MagazineArticle'"], :order => "comments.created_at DESC", :limit => 10)
    @article_pages = [].paginate :page => params[:page], :total_entries => @article.pages_in_article, :per_page => 1
    @page_title = "#{@article.title} / " + @page_title.to_s
      respond_to do |format|
        format.html { render :action => 'flip_template' if @article.flipping_article == true }
        format.xml { render :layout => false }
      end
  end
  
  def init_magazine
    @page_title = "Magazine" + @page_title.to_s
    @magazine_sections = MagazineSection.active.find(:all, :order => 'position')
    if params[:magazine_section]
      @current_section = MagazineSection.find_by_urlname params[:magazine_section]
      @page_title = "#{@current_section.name} / " + @page_title.to_s
      #@magazine_subsections = MagazineSubsection.find(:all, :order => 'position')
      #if @magazine_subsections
      #  @current_subsection = MagazineSubsection.find_by_urlname params[:magazine_subsection]
      #end
    end
  end
end
