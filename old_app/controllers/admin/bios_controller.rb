class Admin::BiosController < Admin::IndexController
  active_scaffold :bios do |config|
    config.label = "BIO"
    config.columns = [:id, :name, :surname, :email, :private_maillist, :card_id, :bio_category, :occupation, :birthday, :gender, :author, :quote, :text, :photo, :user, :not_on_main_page, :englishman, :company, :published_ru, :published_en, :manager]
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    
    config.columns[:bio_category].form_ui = :select
    config.columns[:user].form_ui = :select
    config.columns[:manager].form_ui = :select
    config.columns[:author].form_ui = :select
    
  end
end
