class Admin::CommentsController < Admin::IndexController
  active_scaffold :comments do |config|
    config.label = "Комментарии"
    config.columns = [:id, :body, :user]
    #config.columns.exclude :comments
    #config.create.columns.exclude :comments
    #config.update.columns.exclude :comments
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token

  end
end
