class Admin::ContestPicsController < Admin::IndexController
  active_scaffold :contest_pics do |config|
    config.label = "Конкурс BIO логотипов"
    config.columns.exclude :contest_pic_votes
    config.columns[:user].form_ui = :select
  end
end
