class Admin::ContestSessionsController < Admin::IndexController
  active_scaffold :contest_sessions do |config|
    config.label = "FG Talent Hunting"
    config.columns = [:id, :photographer, :model, :photographer_name, :model_name, :info, :contest_photos]
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    config.create.multipart = true
    config.update.multipart = true
    config.columns[:photographer].form_ui = :select
    config.columns[:model].form_ui = :select
  end
end
