class Admin::PlacesController < Admin::IndexController
  active_scaffold :places do |config|
    config.label = "Места"

    config.columns = [:id, :name, :place_category, :place_type, :address, :place_addresses,:phone, :webpage, :main_photo, :description, :place_photos, :food, :parking, :working_hours, :fgc, :fgc_discount, :fgc_text, :published_ru, :published_en]
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    config.columns[:place_category].form_ui = :select
    config.list.columns.exclude [:food, :parking, :working_hours, :description, :place_photos] #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    config.columns[:place_addresses].collapsed = true
  end
  
end
