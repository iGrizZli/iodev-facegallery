class Admin::VideosController < Admin::IndexController
  active_scaffold :videos do |config|
    config.label = "Videos"
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    
    config.columns[:user].form_ui = :select
    config.columns[:bio].form_ui = :select
  end
end
