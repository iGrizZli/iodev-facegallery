class Admin::ReportPhotosController < Admin::IndexController
  active_scaffold :report_photos do |config|
    config.label = "Фотографии репортажей"
    #config.columns = [:id, :email, :name, :login, :updated_at, :activated_at]
    #config.update.columns.exclude :id
    #config.create.columns.exclude :id
    config.columns.exclude :photo_tags, :comment_threads, :comments
    #config.actions << :sortable
  end
end
