class Admin::TripsController < Admin::IndexController
  active_scaffold :trips do |config|
    config.label = "FG Travel"
    config.columns = [:id, :title, :description, :schedule, :costs, :start_date, :end_date, :published_ru, :published_en, :users]
    config.update.columns.exclude :id
    config.create.columns.exclude :id

    #config.columns[:users].form_ui = :select
    #config.columns[:user].form_ui = :select
    #config.columns[:manager].form_ui = :select
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token

  end
end
