class Admin::BarentzArticlesController < Admin::IndexController
	active_scaffold :barentz_articles do |config|
		config.label = "Barentz articles"
    config.columns[:barentz_category].form_ui = :select
  end
end

