class Admin::EventsController < Admin::IndexController
  active_scaffold :events do |config|
    config.label = "Афиша"
    #config.columns = [:id, :email, :name, :login, :updated_at, :activated_at]
    #config.update.columns.exclude :id
    #config.create.columns.exclude :id
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    config.columns[:place].form_ui = :select
  end
end
