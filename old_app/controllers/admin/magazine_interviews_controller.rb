class Admin::MagazineInterviewsController < Admin::IndexController
  active_scaffold :magazine_interviews do |config|
    config.label = "Интервью в журнале"
    config.columns = [:name, :title, :preface, :quote, :author, :author_name, :magazine_section, :interview_photo, :text, :article_images, :published_ru, :published_en]
    config.columns.exclude :magazine_section, :comments, :name_en, :quote_en, :preface_en, :text_en, :title_en
    config.update.columns.exclude :id
    config.update.columns.add :created_at
    config.create.columns.exclude :id
    #config.create.columns.exclude :comments
    #config.update.columns.exclude :comments
    config.columns[:author].form_ui = :select
    #config.columns[:magazine_section].form_ui = :select
    config.columns[:magazine_issue].form_ui = :select
  end
end
