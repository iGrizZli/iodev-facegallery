class Admin::MagazineArticlesController < Admin::IndexController
  active_scaffold :magazine_articles do |config|
    config.label = "Статьи журнала"
    config.columns = [:id, :title, :author, :author_name, :magazine_section, :magazine_issue, :preview, :news_preview, :background, :summary, :text, :tag_list, :article_images, :flipping_article, :article_page_images, :created_at, :published_ru, :published_en]
    config.update.columns.exclude :id
    config.update.columns.add :created_at
    config.create.columns.exclude :id
    config.columns[:author].form_ui = :select
    config.columns[:magazine_section].form_ui = :select
    config.columns[:magazine_issue].form_ui = :select

    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    
  end


  def autocomplete_tag_list
    @all_tags = Tag.find(:all, :order => 'name ASC')

    re = Regexp.new("^#{params[:record][:tag_list]}", "i")
    @tags = @all_tags.find_all do |t|
      t.name.match re
    end
    respond_to do |format|
      format.html {render :layout => false}
    end

  end
end
