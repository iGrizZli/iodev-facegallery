class Admin::PreviewParserController < Admin::IndexController
  skip_before_filter :verify_authenticity_token 
  def parse_simple_format
    render :text => ActionController::Base.helpers.simple_format(params[:data])
  end
end
