class Admin::ContestQuotesController < Admin::IndexController
  active_scaffold :contest_quotes do |config|
    config.label = "Конкурс цитат"
    config.columns.exclude :contest_quote_votes
    config.columns.add :quote_contest_id
    config.columns[:bio].form_ui = :select
  end
end
