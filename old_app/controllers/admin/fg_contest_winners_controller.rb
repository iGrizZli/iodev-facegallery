class Admin::FgContestWinnersController < Admin::IndexController
  active_scaffold :fg_contest_winners do |config|
    config.label = "Победители конкурса"
    config.columns = [:comment]
    config.columns[:comment].form_ui = :select
  end
end
