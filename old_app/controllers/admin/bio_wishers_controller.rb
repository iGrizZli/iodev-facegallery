class Admin::BioWishersController < Admin::IndexController
  def index
    @bio_wishers = User.paginate :all, :order => 'bio_wished DESC',
                                 :conditions => ["NOT ISNULL(bio_wished)"], :per_page => 20,
                                 :page => params[:page]
  end
end
