class Admin::EventCategoriesController < ApplicationController
  active_scaffold :event_categories do |config|
    config.label = "Категории событий"
    config.columns = [:id, :name, :urlname]
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    
  end
end
