class Admin::DocumentsController < Admin::IndexController
    active_scaffold :documents do |config|
    config.label = "Документы"
    config.columns = [:id, :name, :text, :alias, :visible]
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token

  end
end

