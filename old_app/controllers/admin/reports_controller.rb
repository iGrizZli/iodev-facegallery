class Admin::ReportsController < Admin::IndexController
  active_scaffold :reports do |config|
    config.label = "Репортажи"
    config.columns = [:id, :name, :date, :description, :place, :fgc, :picture, :report_photos, :created_at, :published_ru, :published_en]
    #config.update.columns.include = [:updated_at, :activated_at]
    #config.create.columns.include = [:updated_at, :activated_at]
    config.update.columns.exclude :id
    config.create.columns.exclude :id, :report_photos
    config.update.columns.add :created_at
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    config.columns[:place].form_ui = :select
  end
end
