class Admin::DailyEventsController < Admin::IndexController
  active_scaffold :daily_events do |config|
    config.label = "Афиша дня"
    config.columns = [:id, :name, :date, :event_category, :picture, :text, :long_text, :place, :fgc, :johnywalker, :published_ru]
    config.columns[:place].collapsed = true
    #config.columns[:place].form_ui = :select
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    
  end
end
