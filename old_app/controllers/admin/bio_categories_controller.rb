class Admin::BioCategoriesController < Admin::IndexController
  active_scaffold :bio_categories do |config|
    config.label = "BIO категории"
    config.columns = [:id, :name, :urlname, :position]
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    
  end
end