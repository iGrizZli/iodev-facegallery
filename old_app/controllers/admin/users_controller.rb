class Admin::UsersController < Admin::IndexController
	active_scaffold :user do |config|
		config.label = "Пользователи"
		config.columns = [:id, :email, :name, :surname, :login, :password, :updated_at, :activated_at]
		config.update.columns.exclude :id
		config.create.columns.exclude :id
        config.update.columns.add :password_confirmation
		#config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
		
	end
end
