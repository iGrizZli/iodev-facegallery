class Admin::FgContestQuestionsController < Admin::IndexController
  active_scaffold :fg_contest_questions do |config|
    config.label = "Вопросы от всяких там"
    #config.columns = [:id, :email, :name, :login, :updated_at, :activated_at]
    config.columns.exclude :comments
    config.create.columns.exclude :comments
    config.update.columns.exclude :comments
    config.columns.exclude :comment_threads
    config.create.columns.exclude :comment_threads
    config.update.columns.exclude :comment_threads
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    config.columns[:winner_comment].form_ui = :select
    config.columns[:author].form_ui = :select

  end
end
