class Admin::IndexController < ApplicationController
  before_filter :login_required
  #before_filter :authorized?
  layout 'admin'
  before_filter :jquery_noconflict
  skip_before_filter :close_site



	def index
		
  end

  def jquery_noconflict
    ActionView::Helpers::PrototypeHelper.const_set(:JQUERY_VAR, 'jQuery')
  end

  def authorized?
    logged_in? && current_user.is_admin?
  end
end
