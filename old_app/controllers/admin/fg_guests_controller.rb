class Admin::FgGuestsController < Admin::IndexController
  active_scaffold :fg_guests do |config|
    config.label = "Гости недели"
    #config.columns = [:id, :email, :name, :login, :updated_at, :activated_at]
    #config.columns.exclude :comments
    #config.create.columns.exclude :comments
    #config.update.columns.exclude :comments
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token

    config.columns[:blog].form_ui = :select
    
    #config.columns[:author].form_ui = :select

  end
end
