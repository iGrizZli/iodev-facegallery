class Admin::NewsItemsController < Admin::IndexController
  active_scaffold :news_items do |config|
    config.label = "Новости"
    config.columns = [:id, :title, :text, :date, :image, :fgc, :place, :published_ru, :published_en]
    config.update.columns.exclude :id
    config.create.columns.exclude :id
    #config.columns.exclude :crypted_password, :salt, :remember_token_expires_at, :remember_token
    config.columns[:place].form_ui = :select
  end
end
