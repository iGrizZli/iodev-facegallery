class Admin::MagazineIssuesController < ApplicationController
  active_scaffold :magazine_issues do |config|
    config.label = "Выпуски журнала"
    config.update.columns.exclude :id, :text, :text_en
    config.create.columns.exclude :id, :text, :text_en
  end
end
