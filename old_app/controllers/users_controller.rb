class UsersController < ApplicationController
  
  # render new.rhtml
  def new
    @invite = SiteInvite.unused.find_by_code(params[:code])
    @user = User.new
  end
 
  def create
    logout_keeping_session!
    @user = User.new(params[:user])
    @invite = SiteInvite.unused.find_by_code(params[:invite][:code])
    success = @user && @user.save && @invite
    if success && @user.errors.empty?
      @invite.used = true
      @invite.new_user_id = @user.id
      @invite.save
      redirect_to(:action => 'created')
#      flash[:notice] = "Thanks for signing up!  We're sending you an email with your activation code."
    else
      #flash[:error]  = "Извините, не удалось создать аккаунт. Попробуйте зарегистрироваться позднее, или напишите в службу поддержки." if @user.errors.empty?
      render :action => 'new'
    end
  end


  
  def created
    
  end

  def activate
    logout_keeping_session!
    user = User.find_by_activation_code(params[:activation_code]) unless params[:activation_code].blank?
    case
    when (!params[:activation_code].blank?) && user && !user.active?
      user.user_agent = request.env["HTTP_USER_AGENT"]
      user.activate!
      #flash[:notice] = "Signup complete! Please sign in to continue."
      self.current_user = user
      redirect_to welcome_message_path
    when params[:activation_code].blank?
      flash[:error] = "Нет активационного кода. Пожалуйста, перейдите по ссылке из email-собщения."
      redirect_back_or_default('/')
    else 
      flash[:error]  = "Такой код не найден - попробуйте проверить почту и перейти по ссылке в сообщении. Может быть вы уже активировались? Попробуйте авторизоваться через форму."
      redirect_back_or_default('/')
    end
  end
  
  def welcome_message
    if params[:user] || request.post?
      if current_user.update_attributes(params[:user])
        redirect_to user_path(current_user)
      else
        current_user.reload
      end
    end
  end

  def mamba_api
    render :xml => User.activated.find(:all).to_xml(:only => [:id, :gender, :birthday, :city], :methods => [:mamba_geo, :age])
  end
end
