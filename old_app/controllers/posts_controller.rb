class PostsController < ApplicationController
    before_filter :login_required, :only => [ :edit, :create, :destroy, :update, :new ]
    before_filter :init_community

    # GET messages_url
    def index
      # return all messages
      @posts = Post.paginate_by_post_section_id @current_section.id, :page => params[:page], :per_page => 10
    end

    # GET new_message_url
    def new
      # return an HTML form for describing a new message
    end

    # POST messages_url
    def create
      # create a new message
      @new_post = Post.new;
      @new_post.update_attributes(params[:post])
      @new_post.user = current_user;
      @new_post.post_section = @current_section
      
      if @new_post.save
        flash[:notice] = 'Запись создана!'
        redirect_to post_url(:id => @new_post.id, :post_section => @current_section.urlname)
      else
        flash[:notice] = 'Не удалось создать запись'
        redirect_to posts_url(:post_section => @current_section.urlname)
      end

    end

    # GET message_url(:id => 1)
    def show
      @post = Post.find(params[:id])
      
      @page_title = "#{@post.title} / " + @page_title.to_s
    end

    # GET edit_message_url(:id => 1)
    def edit
      @post = Post.find_by_id_and_user_id(params[:id], current_user.id)
    end

    # PUT message_url(:id => 1)
    def update
      # find and update a specific message
      @post = Post.find_by_id_and_user_id(params[:id], current_user.id)
      @post.update_attributes(params[:post])
      if @post.save
        flash[:notice] = 'Запись сохранена!'
      end
      redirect_to post_url(:id => @post.id, :post_section => @current_section.urlname)
    end

    # DELETE message_url(:id => 1)
    def destroy
      # delete a specific message
      @post = Post.find_by_id_and_user_id(params[:id], current_user.id)
      @post.destroy
      redirect_to posts_url
    end
    
    def posts_front
      @posts = Post.find(:all, :limit => 8, :order => 'created_at DESC')
    end
  
    def init_community
      @page_title = "Community" + @page_title.to_s
      @post_sections = PostSection.active.find(:all)
      @current_section = PostSection.find_by_urlname(params[:post_section]) if params[:post_section]
      if @current_section
        @page_title = "#{@current_section.name} / " + @page_title.to_s
      end
    end
end
