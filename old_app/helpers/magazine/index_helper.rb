module Magazine::IndexHelper
  def text_parse_codes(text, images = nil)

    unless images.nil?
      # $1 - image id, $2 - options
      text = text.gsub(/\[img\s*?(\d+)\s*?([\s\w]*?)\]/) { |s|
        image = images.select { |v| v.id.to_s == $1 }.first
        unless $2.empty?
          if $2.include? "l"
            imgclass = "m-pic-left"
          elsif $2.include? "r"
            imgclass = "m-pic-right"
          end
          if $2.include? "b"
            linkclass = "magazine-pic-link"
          end
        end
        if image
          result = image_tag image.image.url(:thumbnail), :class => imgclass
          if linkclass
            link_to result, image.image.url, :class => linkclass
          else
            result
          end
        end
      }
      end
    # $1 - style no., $2 - options, $3 - text
    text = text.gsub(/\[hl\s*?(\d+)\s*?([\s\w]*?)\](.+?)\[\/hl\]/m) { |s|
      unless $1.empty?
        if $2.include? "l"
          divclass = "m-hl-left"
        elsif $2.include? "r"
          divclass = "m-hl-right"          
        else 
          divclass = "m-hl-center"
        end
        "<div class=\"m-hl-#{$1} #{divclass}\">#{$3}</div>"
      end
    }
    text = text.gsub(/\s-\s/, " &mdash; ")
    text
  end

  def display_author(article)
    if article.author
      t('magazine_author') + " " + link_to(article.author.displayname, user_path(article.author))
    elsif article.author_name && !article.author_name.empty?
      t('magazine_author') + " " + article.author_name
    end
  end

  def output_summary(obj, length = 1600)
    obj.summary or shorten_text(obj.text, length) or ''    
  end
end
