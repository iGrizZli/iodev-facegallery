module BarentzHelper
  def barentz_parse_codes(text, images = nil)

    unless images.nil?
      # $1 - image id, $2 - options
      text = text.gsub(/\[img\s*?(\d+)\s*?([\s\w]*?)\]/) { |s|
        image = images.select { |v| v.id.to_s == $1 }.first
        unless $2.empty?
          if $2.include? "l"
            imgclass = "m-pic-left"
          elsif $2.include? "r"
            imgclass = "m-pic-right"
          end
          if $2.include? "b"
            linkclass = "magazine-pic-link"
          end
        end
        if image
          #result = image_tag image.image.url(:thumbnail), :class => imgclass, :style => 'margin: 0 auto'
          #if linkclass
          #  link_to result, image.image.url, :class => linkclass
          #else
          #  result
          #end
          barentz_image_with_title(image)
        end
      }
      
      text = text.gsub(/\[photos\]/m) { |s|
        res = ''
        images.each do |image|
          #result = image_tag image.image.url(:thumbnail),  :style => 'margin: 0 auto'
          #res += link_to result, image.image.url, :class => 'magazine-pic-link'
          res += barentz_image_with_title(image)
        end
        res
      }
      end
    # $1 - style no., $2 - options, $3 - text
    text = text.gsub(/\[hl\](.+?)\[\/hl\]/m) { |s|

        #"<div class=\"m-hl-#{$1} #{divclass}\">#{$3}</div>"
        "<div class='statement with-icon'>
          <h2>#{$1}</h2>
        </div>"
        
 
    }
    text = text.gsub(/\s-\s/, " &mdash; ")

    text
  end
  
  def barentz_image_with_title(image)
    content_tag 'div', :class => 'gallery-pict with-icon', :style => 'background:none;padding-bottom:0' do
      title = image.title.to_s.empty? ? "&nbsp;" : "<span>#{image.title.to_s}</span>"
      res = "<h3>#{title}</h3>"
      res += link_to image_tag(image.image.url(:thumbnail)), image.image.url, :class => 'magazine-pic-link'
      res += '<div class="numbers"></div>'
      res
    end
  end
end
