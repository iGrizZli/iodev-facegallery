module User::MessagesHelper
  def replace_smilies(text)
    smilies = { ":-)" => "smile",
                ":)" => "smile",
                ":-(" => "sad",
                ":(" => "sad",
                ";-)" => "eye",
                ";)" => "eye",
                ":-P" => "tongue",
                "*LOVE*" => "love",
                "o_O" => "scared",
                ":-D" => "lol",
                ":-/" => "mad",
                
                ">:o" => "evil",
                ":-*" => "kiss",
                "=-O" => "what",
                "*GOP*" => "gopnik",
                
                "O:=)" => "angel",
                "*TIRED*" => "sleep",
                "*LOL*" => "roll",
              }
    smilies.each_pair do |key,value|
      text = text.gsub(key, "<img src='/i/smilies/#{value}.gif'>")
    end
    text
  end
end
