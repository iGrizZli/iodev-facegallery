module Admin::TripsHelper
  #def comment_threads_form_column(record, options)
  #  select :record, :comment_threads, record[:comment_threads], options
  #end
  #def comments_form_column(record, options)
  #  select :record, :comment_threads, [:comment_threads], options
  #end
#description field is shown in FCK editor format

def description_form_column(record, input_name)
fckeditor_textarea( :record, :description, :toolbarSet => 'Simple', :name=> input_name, :width => '800px', :height => '400px' )
end
def schedule_form_column(record, input_name)
fckeditor_textarea( :record, :schedule, :toolbarSet => 'Simple', :name=> input_name, :width => '800px', :height => '400px' )
end
def costs_form_column(record, input_name)
fckeditor_textarea( :record, :costs, :toolbarSet => 'Simple', :name=> input_name, :width => '800px', :height => '400px' )
end
#description text is displayed in rich format

#def description_column(record)

#sanitize(record.description)

#end
end
