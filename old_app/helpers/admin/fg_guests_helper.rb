module Admin::FgGuestsHelper
  def options_for_association(association)
    if association.name == :blog
       available_records = association_options_find(association,
         options_for_association_conditions(association))
       available_records ||= []
       available_records.sort{|a,b| b.id <=> a.id}.collect { |model|
         [ model.to_label, model.id ] }
    else
      super
    end
  end

end
