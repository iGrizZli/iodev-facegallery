module Admin::FgContestWinnersHelper
  def options_for_association_conditions(association)
    if association.name == :comment
      #['roles.id != ?', Role.find_by_name('admin').id] unless current_user.admin?
      ["comments.commentable_type='FgContestQuestion' AND comments.commentable_id = ?", params[:id]]
    else
      super
    end
  end
end
