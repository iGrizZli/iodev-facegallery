module UserHelper
  def is_owner?
    current_user && @user.id == current_user.id
  end
end
