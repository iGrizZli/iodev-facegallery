module SearchHelper
  def display_result (r)
    res = content_tag "div", t(r.class, :scope => [:search, :class_names]), :class => 'search_section cgothic'
    case r.class.to_s
      when "MagazineArticle"
        res += link_to(r.title, magazine_article_path(:id => r, :magazine_section => r.magazine_section.urlname)) 
        res += search_excerpt(r.excerpts.text)
      when "MagazineInterview"
        res += link_to(r.name + " " + r.title, magazine_interview_path(:id => r, :magazine_section => "interview")) 
        res += search_excerpt(r.excerpts.text)
      when "Bio"
        res += link_to(r.displayname, bio_path(:id => r, :bio_category => r.bio_category)) 
        res += search_excerpt(r.excerpts.text)
      when "User"
        res += link_to(r.displayname, url_for(r)) 
      when "Event"
        res += link_to(r.name, url_for(r)) 
        res += search_excerpt(r.excerpts.description)
      when "Report"
        res += link_to(r.name, url_for(r)) 
        res += search_excerpt(r.excerpts.description)
      when "Place"
        res += link_to(r.name, place_path(:id => r, :place_category => r.place_category.urlname)) 
        res += search_excerpt(r.excerpts.description)
      when "Post"
        res += link_to(r.user.displayname, r.user) + " / "
        res += link_to(r.title, post_path(:id=> r, :post_section => r.post_section.urlname)) 
        res += search_excerpt(r.excerpts.text)
      when "Blog"
        res += link_to(r.user.displayname, r.user) + " / "
        res += link_to(r.title, blog_path(:id=>r.id, :user_id => r.user_id)) 
        res += search_excerpt(r.excerpts.text)
      else
      "error"
   end
   res
  end
  
  def search_excerpt(text)
    content_tag "div", sanitize(text, :tags => %w(span)), :class => 'search_excerpt'
  end
end
