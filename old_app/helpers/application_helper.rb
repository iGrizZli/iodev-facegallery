# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def face_list
    bios_size = 10
    #@birthday_bios = Bio.find(:all, :limit => bios_size, :conditions => ["MONTH(birthday) = ? AND DAY(birthday) = ?", Date.today.month, Date.today.day])
    #selected_ids = @birthday_bios.collect(&:id)
    #bios_size -= selected_ids.length
    #cond = selected_ids.length > 0 ? ["bios.id NOT IN(?)", selected_ids] : nil
    @bios = Bio.published.random.facecontrolled.find(:all, :limit => bios_size, :include => :bio_category)
    render :partial => 'layouts/face_list'
  end 



  #OLDDD

  def page_title(name)
    @page_title = name
  end

  def change_locale_url
    'http://' + (I18n.locale.to_s == 'ru' ? 'en.' : '') + request.domain + request.request_uri
  end

  def submenu_active? (cur_page, page)
    { :class => "submenu_active" } if cur_page == page
  end
  def leftmenu_active? (par)
    { :class => "here" } if (!par[:action_name] || controller.action_name == par[:action_name]) and (!par[:controller_name] || controller.controller_name == par[:controller_name])
  end
  def category_leftmenu_active? (par1, par2)
    { :class => "here" } if (par1 == par2)
  end
  def leftmenu_active_if (condition)
    { :class => "here" } if (condition)
  end

  def online_icon_for(user)
    image_tag('/i/online-icon.png') if user.is_online?
  end
  
  def mdash(text)
    text.gsub(/\s–\s/, "&nbsp;&#8212; ")
  end

  def shorten_text(text, count = 350, end_string = ' ...')
        string = strip_tags(text)
        string = string.gsub(/\[[\s\da-zA-Z]+\]/, "")
    if string.length >= count
        shortened = string[0, count]
        splitted = shortened.split(/\s/)
        words = splitted.length
        splitted[0, words-1].join(" ") + end_string
    else
        string
    end
  end

  def convert_links(subject)
    subject.gsub(/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]/i, '<a href="\0" target="_blank">\0</a>')    
  end

  def output_comment (r)
    begin
    res = t(r.commentable.class, :scope => [:search, :class_names]) + "<span>&nbsp;&rarr;&nbsp;</span>"
    case r.commentable.class.to_s
      when "MagazineArticle"
        path = magazine_article_path(:id => r.commentable, :magazine_section => r.commentable.magazine_section.urlname, :anchor => "comment#{r.id}")
        res += link_to(h(r.commentable.title), path, :class => "set")

      when "MagazineInterview"
        path = magazine_interview_path(:id => r.commentable, :magazine_section => "interview", :anchor => "comment#{r.id}")
        res += link_to(r.commentable.name + " " + r.commentable.title, path, :class => "set")

      when "Bio"
        path = bio_path(:id => r.commentable, :bio_category => r.commentable.bio_category, :anchor => "comment#{r.id}")
        res += link_to(r.commentable.displayname, path, :class => "set")

      when "User"
        path = url_for(r.commentable, :anchor => "comment#{r.id}")
        res += link_to(h(r.commentable.displayname), path, :class => "set")
      when "UserGalleryPhoto"
        path = user_image_path(:image_id=>r.commentable.id, :id=>r.commentable.user_album.id, :user_id => r.commentable.user_id, :anchor => "comment#{r.id}")
        res += link_to(h(r.commentable.user_album.title || r.commentable.user_album.title), path,  :class => "set")
      when "Event"
        path = url_for(r.commentable, :anchor => "comment#{r.id}")
        res += link_to(r.commentable.name, path, :class => "set")

      when "Report"
        path = url_for(r.commentable, :anchor => "comment#{r.id}")
        res += link_to(r.commentable.name, path, :class => "set")

      when "Place"
        path = place_path(:id => r.commentable, :place_category => r.commentable.place_category.urlname, :anchor => "comment#{r.id}")
        res += link_to(r.commentable.name, path, :class => "set")

      when "Post"
        #res += link_to(r.commentable.user.commentable.displayname, r.commentable.user) + " / "
        path = post_path(:id=> r.commentable, :post_section => r.commentable.post_section.urlname, :anchor => "comment#{r.id}")
        res += link_to(h(r.commentable.title), path, :class => "set")

      when "Blog"
        #res += link_to(r.commentable.user.commentable.displayname, r.commentable.user) + " / "
        path = blog_path(:id=>r.commentable.id, :user_id => r.commentable.user_id, :anchor => "comment#{r.id}")
        res += link_to(h(r.commentable.title), path, :class => "set")
      when "ReportPhoto"
        #res += link_to(r.commentable.user.commentable.displayname, r.commentable.user) + " / "
        path = report_photo_path(:id=>r.commentable.report.id, :image_id => r.commentable.id, :anchor => "comment#{r.id}")
        res += link_to(h(r.commentable.report.name), path, :class => "set")
      when "FgContestQuestion"
        path = url_for(:controller => :bio, :action => :index, :anchor => "comment#{r.id}")
        #res += link_to(h("Конкурс"), path, :class => "set")
      when "Video"
        path = video_path(r.commentable, :anchor => "comment#{r.id}")
        #res += link_to(h("Конкурс"), path, :class => "set")
      else
      RAILS_DEFAULT_LOGGER.error("comment block unknown object " + r.commentable_type)
      ""
    end
     res = content_tag "p", res
     res += content_tag "p", (link_to(truncate(strip_tags(r.body), :length => 50), path, :class => 'text') + " " + link_to(h(r.user.displayname), user_path(r.user), :class => 'set'))
     res = content_tag "div", res
     res
    rescue Exception => exc
     RAILS_DEFAULT_LOGGER.error("comment block output error " + exc.message)
     ""
    end

  end


  def comment_url (r)
    begin
    case r.commentable.class.to_s
      when "MagazineArticle"
        path = magazine_article_path(:id => r.commentable, :magazine_section => r.commentable.magazine_section.urlname, :anchor => "comment#{r.id}")
      when "MagazineInterview"
        path = magazine_interview_path(:id => r.commentable, :magazine_section => "interview", :anchor => "comment#{r.id}")
      when "Bio"
        path = bio_path(:id => r.commentable, :bio_category => r.commentable.bio_category, :anchor => "comment#{r.id}")
      when "User"
        path = url_for(r.commentable, :anchor => "comment#{r.id}")
      when "UserGalleryPhoto"
        path = user_image_path(:image_id=>r.commentable.id, :id=>r.commentable.user_album.id, :user_id => r.commentable.user_id, :anchor => "comment#{r.id}")
      when "Event"
        path = url_for(r.commentable, :anchor => "comment#{r.id}")
      when "Report"
        path = url_for(r.commentable, :anchor => "comment#{r.id}")
      when "Place"
        path = place_path(:id => r.commentable, :place_category => r.commentable.place_category.urlname, :anchor => "comment#{r.id}")
      when "Post"
        path = post_path(:id=> r.commentable, :post_section => r.commentable.post_section.urlname, :anchor => "comment#{r.id}")
      when "Blog"
        path = blog_path(:id=>r.commentable.id, :user_id => r.commentable.user_id, :anchor => "comment#{r.id}")
      when "Trip"
        path = url_for(:id=>r.commentable.id, :controller => 'fgc/travel', :action => 'show', :anchor => "comment#{r.id}")
      when "ReportPhoto"
        path = report_photo_path(:id=>r.commentable.report.id, :image_id => r.commentable.id, :anchor => "comment#{r.id}")
      when "Video"
        path = video_path(r.commentable, :anchor => "comment#{r.id}")
      else
      RAILS_DEFAULT_LOGGER.error("comment url unknown object " + r.commentable_type)
      ""
    end
     "http://facegallery.ru" + path
    rescue Exception => exc
     RAILS_DEFAULT_LOGGER.error("comment url output error " + exc.message)
     ""
    end

  end

  def output_photo_thumb(photo)
    begin
    case photo.class.to_s
      when "ReportPhoto"
        link_to image_tag(photo.report_photo.url(:thumbnail)), report_photo_path(:image_id => photo.id, :id => photo.report.id)
      when "UserGalleryPhoto"
        link_to image_tag(photo.photo.url(:small)), user_image_path(:user_id => photo.user.id, :id=>photo.user_album.id, :image_id=>photo.id)
      else
        RAILS_DEFAULT_LOGGER.error("photo thumb unknown object " + photo.class.to_s)
      end
  end
  end

  def notify_popup(text)
    "$.jGrowl('#{text}');"
  end
end

