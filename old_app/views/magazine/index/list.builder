xml.instruct!
xml.images {
  @articles.each do |a|
    xml.image {
      xml.title a.title
      xml.link magazine_article_path(:magazine_section => a.magazine_section, :id=>a)
      xml.pic a.preview.url
    }
  end
}