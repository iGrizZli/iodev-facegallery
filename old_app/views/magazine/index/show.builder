xml.instruct!
xml.PageflipDataSet{
  xml.Settings (
  :PageWidth => 400,
  :PageHeight => 530,
  :AutoSize => 'true',
  :StartPage => 2,
  :EmbossedPages => 'true',
  :FlippingEnabled => 'true',
  :startAutoFlip => 'false',
  :AutoFlipDefaultInterval=>"5",
  :AutoFlipLooping=>"true",
        
  :RightToLeft=>"false",
      
  :PageScale=>"true",
      
  :PageCache=>"5",
  :UnloadPages=>"false",
        
  :ZoomAreaWidth=>"900",
  :ZoomAreaHeight=>"540",
  :ZoomFollowSpeed=>"5",
  :ZoomOnPageClick=>"true",
  :DragZoom=>"true",
      
  :ThumbnailsEnabled=>"false",
               
  :MouseControl=>"true"
  )
  xml.PageOrder{
    @article.article_page_images.each do |a|
      xml.PageData ( :PageFile => a.image.url(:small, false), :LargeFile => a.image.url(:original, false))
  
    end
  }
}