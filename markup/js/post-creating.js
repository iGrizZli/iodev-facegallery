$(document).ready(function () {
	$('ul.post-creating-btns a').click(function() {
		var class_btn = $(this).parent().attr('class');
		var add_div = $('body > div.' + class_btn)
		if (add_div.length) $('ul.post-creating-btns').before(add_div);
		$('input.publish-btn').removeAttr('disabled').removeClass('no-active');
		return false;
	});
	$('div.input-load input.load').hover(
		function () {
			$(this).parent().find('a').addClass('hover');
		},
		function () {
			$(this).parent().find('a').removeClass('hover');
	});
	$('div.add-text input.save-btn').click(function() {
		var load = $(this).parent();
		var text = load.find('textarea');
		// ��������� �����
		load.after('<div class="text-block"><p>' + text.val() + '</p></div>');
		text.val('');
		$('body').append(load);
		return false;
	});
	$('div.load.photo input.load').change(function() {
		$(this).parent().hide();
		var load = $(this).parent().parent();
		load.find('div.load-process').show();
		// ��������� �������� ����������� �� ������ � ������ ����� (����� images/shared)
		load.after('<div class="image-block"><img src="images/shared/' + $(this).val() + '"/></div>');
	});
	$('div.load.gallery input.load').change(function() {
		$(this).parent().hide();
		$(this).parent().parent().find('div.load-process').show();
		// ��������� �������� ������� ����������� �� ������ � ������ �����, ������� �������������
	});
	$('div.load.video input.save-btn').click(function() {
		var load = $(this).parent();
		var link = load.find('input[type=text]');
		// ��������� ��� ���� http://www.youtube.com/v/kh29_SERH0Y
		load.after('<div class="video-block"><object height="315" width="560"><param value="' + link.val() + '" name="movie"><param value="true" name="allowFullScreen"><param value="always" name="allowscriptaccess"><embed height="315" width="560" type="application/x-shockwave-flash" src="' + link.val() + '" allowscriptaccess="always" allowfullscreen="true"></object></div>');
		link.val('');
		return false;
	});
	$('ul.control-panel').hover(
		function() {},
		function() {
			$('body').append($(this));
	});
	$('div.content').delegate('div.add-text, div.load:not(.cover)', 'hover', function(event) {
		if (event.type === 'mouseenter') {
			var panel = $('ul.control-panel.no-edit');
			$(this).append(panel);
		}
		else {
			if (!$(this).find('ul.control-panel:hover').length) {
				var panel = $(this).find('ul.control-panel');
				$('body').append(panel);
			}
		}
	});
	$('div.content').delegate('div.text-block, div.image-block, div.video-block', 'hover', function(event) {
		if (event.type === 'mouseenter') {
			var panel = $('ul.control-panel.edit');
			$(this).append(panel);
		}
		else {
			if (!$(this).find('ul.control-panel:hover').length) {
				var panel = $(this).find('ul.control-panel');
				$('body').append(panel);
			}
		}
	});
	$('ul.control-panel li.up a').click(function() {
		var panel = $(this).parent().parent();
		var div_current = panel.parent();
		var div_prev = div_current.prev();
		if (div_prev.attr('class') != 'author-info') div_prev.before(div_current);
		$('body').append(panel);
		return false;
	});
	$('ul.control-panel li.down a').click(function() {
		var panel = $(this).parent().parent();
		var div_current = panel.parent();
		var div_next = div_current.next();
		if (div_next.attr('class') != 'post-creating-btns') div_current.before(div_next);
		$('body').append(panel);
		return false;
	});
	$('ul.control-panel li.delete a').click(function() {
		var panel = $(this).parent().parent();
		var div_current = panel.parent();
		$('body').append(panel);
		if ((div_current.attr('class') == 'text-block') || (div_current.attr('class') == 'image-block') || (div_current.attr('class') == 'video-block')) div_current.remove();
		else $('body').append(div_current);
		if (!$('.content div.add-text').length && !$('.content div.load:not(.cover)').length && !$('.content div.text-block').length && !$('.content div.image-block').length && !$('.content div.video-block').length)
			$('input.publish-btn').attr('disabled','disabled').addClass('no-active');
		return false;
	});
});