$(document).ready(function () {
	$('div.input-load input.load').hover(
		function () {
			$(this).parent().find('a').addClass('hover');
		},
		function () {
			$(this).parent().find('a').removeClass('hover');
	});
	$('div.input-edit-cover input.load').hover(
		function () {
			$(this).parent().find('a').addClass('hover');
		},
		function () {
			$(this).parent().find('a').removeClass('hover');
	});	
	$('div.profile-projects div.project-cover').hover(
		function () {
			$(this).find('div.input-edit-cover').show();
		},
		function () {
			$(this).find('div.input-edit-cover').hide();
	});	
});