// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
function insertAtCaret(areaId,text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
        "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff") strPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0,strPos);  
    var back = (txtarea.value).substring(strPos,txtarea.value.length); 
    txtarea.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();
    }
    else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}

      $(document).ready(function() {
			swapValues = [];
      $("#search_input, .signin input, input#inline_search").each(function(i){
          swapValues[i] = $(this).val();
          $(this).focus(function(){
              if ($(this).val() == swapValues[i]) {
                  $(this).val("");
              }
          }).blur(function(){
              if ($.trim($(this).val()) == "") {
                  $(this).val(swapValues[i]);
              }
          });
      });
    });

$.expr[':'].containsIgnoreCase = function(a,i,m){
return $(a).text().toUpperCase().indexOf(m[3].toUpperCase())>=0;
};

$.fn.search = function(searchElements) {
  $(this).keyup(function(){
    var searchString = $(this).val();

    if (searchString.length > 0){
      $(searchElements).hide();
      $(searchElements).filter(':containsIgnoreCase(' +searchString+ ')').show();
    }
    else {
      $(searchElements).show();
    }
  });
};

$(document).ajaxSend(function(event, request, settings) {
  if (typeof(AUTH_TOKEN) == "undefined") return;
  // settings.data is a serialized string like "foo=bar&baz=boink" (or null)
  settings.data = settings.data || "";
  settings.data += (settings.data ? "&" : "") + "authenticity_token=" + encodeURIComponent(AUTH_TOKEN);
});


        function startTagging(){
            jcrop_api = $.Jcrop('.taggable_photo', {onChange: updateTagContainer, onSelect: updateTagCoords, minSize: [45,45]});
            $('#taggable_users').load('/photo_tags/taggable_users_list', null, tagUsersLoaded);
            $('input#inline_search').search('div#taggable_users li');
            

        }
        function cancelTagging(){
            $('input#inline_search').val('');
            $('#taggable_users').html('');
            $('#tagging_container').css('display', 'none');
            jcrop_api.destroy();
        }
        function tagUsersLoaded(responseText, textStatus, XMLHttpRequest){

        }
        function updateTagContainer(c)
        {
           $('#tagging_container').css('display', 'block');
           $('#tagging_container').css('left', c.x + c.w + tagPhotoLeft + 15);
           $('#tagging_container').css('top', c.y + tagPhotoTop);
        }
        function updateTagCoords(c){
            tagCoords.x = c.x;
            tagCoords.y = c.y;
            tagCoords.w = c.w;
            tagCoords.h = c.h;
        }
        function tagUser(user_id, person_taggable_type){
            rel_x = tagCoords.x * 100 / $('.taggable_photo').width();
            rel_y = tagCoords.y * 100 / $('.taggable_photo').height();
            rel_w = tagCoords.w * 100 / $('.taggable_photo').width();
            rel_h = tagCoords.h * 100 / $('.taggable_photo').height();
            $.post( "/photo_tags/new", {'photo_tag[person_taggable_id]':user_id,
                                        'photo_tag[person_taggable_type]':person_taggable_type,
                                        'photo_tag[photo_taggable_id]':photo_taggable_id,
                                        'photo_tag[photo_taggable_type]':photo_taggable_type,
                                        'photo_tag[x]':tagCoords.x,
                                        'photo_tag[y]':tagCoords.y,
                                        'photo_tag[w]':tagCoords.w,
                                        'photo_tag[h]':tagCoords.h,
                                        'photo_tag[x_rel]':rel_x,
                                        'photo_tag[y_rel]':rel_y,
                                        'photo_tag[w_rel]':rel_w,
                                        'photo_tag[h_rel]':rel_h
                                       }, null, "script" );
        }