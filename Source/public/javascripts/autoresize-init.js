$(document).ready(function () {
	$('textarea.resize-title, textarea.resize-desc, textarea.resize-first-name, textarea.resize-last-name, textarea.resize-slogan, textarea.resize-album, textarea.resize-photo, textarea.resize-project').autoResize({
		extraSpace: 0
	});
	$('h1 a').click(function() {
		$('h1').hide();
		var textarea = $('textarea.resize-title').last();
		$('textarea.resize-title').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$('textarea.resize-title').hide();
				$('h1').show();				
			}
		});
		return false;
	});
	$('div.post-desc a').click(function() {
		$('div.post-desc').hide();
		var textarea = $('textarea.resize-desc').last();
		$('textarea.resize-desc').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$('textarea.resize-desc').hide();
				$('div.post-desc').show();				
			}
		});
		return false;
	});
	$('h2.last-name a').click(function() {
		$(this).parent().hide();
		var textarea = $('textarea.resize-last-name').last();
		$('textarea.resize-last-name').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$('textarea.resize-last-name').hide();
				$('h2.last-name').show();				
			}
		});
		return false;
	});
	$('h2.first-name a').click(function() {
		$(this).parent().hide();
		var textarea = $('textarea.resize-first-name').last();
		$('textarea.resize-first-name').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$('textarea.resize-first-name').hide();
				$('h2.first-name').show();				
			}
		});
		return false;
	});
	$('div.slogan a').click(function() {
		$(this).hide();
		var textarea = $('textarea.resize-slogan').last();
		$('div.slogan div').show();
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$('div.slogan div').hide();
				$('div.slogan a').show();				
			}
		});
		return false;
	});
	$('div.album-title h2 a').click(function() {
		$(this).parent().hide();
		var textarea = $('textarea.resize-album').last();
		$('textarea.resize-album').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$('textarea.album').hide();
				$('div.album-title h2').show();				
			}
		});
		return false;
	});
	$('div.profile-edit div.gallery li a').click(function() {
		$(this).hide();
		var textarea = $(this).parent().find('textarea.resize-photo').last();
		$(this).parent().find('textarea.resize-photo').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$(this).parent().find('textarea.resize-photo').hide();
				$(this).parent().find('a').show();				
			}
		});
		return false;
	});
	$('div.project-info h2.edit a').click(function() {
		$(this).parent().hide();
		var parent = $(this).parent().parent();
		var textarea = parent.find('textarea.resize-project').last();
		parent.find('textarea.resize-project').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				parent.find('textarea.resize-project').hide();
				parent.find('h2').show();				
			}
		});
		return false;
	});
});
