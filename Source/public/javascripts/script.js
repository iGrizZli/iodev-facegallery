﻿$(document).ready(function () {
	/* START top-menu */
	$('ul.top-menu a.dropdown, ul.top-menu-small a.dropdown').bind('mouseenter', function () {
		$(this).parent().addClass('hover');
		$(this).parent().hover(
			function () {},
			function () {
				$(this).removeClass('hover');
		});
	});
	/* END top-menu */
	/* START tabs */
	var active_tab = 0;
	$('ul.tabs a').click(function () {
		$('ul.tabs li.active').removeClass('active');
		active_tab = $(this).parent().index();
		$('div.box').hide();
		$(this).parent().addClass('active');
		$('div.box').eq(active_tab).show();
		return false;
	});
	/* END tabs */
	/* START section-info block */
	$('div.section:has(> .section-info):not(.gallery)').hover(
		function () {
			$(this).find('> .section-info').addClass('hover');
		},
		function () {
			$(this).find('> .section-info').removeClass('hover');
	});
	$('div.section.posts > ul > li:has(> .section-info)').hover(
		function () {
			$(this).find('> .section-info').addClass('hover');
		},
		function () {
			$(this).find('> .section-info').removeClass('hover');
	});
	$('div.section.album > ul > li:has(> .section-info) .section-content, ul.stream-posts > li:has(> .section-info) .section-content, ul.blog > li:has(> .section-info) .section-content, div.section.projects > ul > li:has(> .section-info) .section-content').hover(
		function () {
			$(this).parent().find('> .section-info').addClass('hover');
		},
		function () {
			$(this).parent().find('> .section-info').removeClass('hover');
	});
	/* END section-info block */
	/* START clock */
	$('ul.clock').each(function () {
		var hour = $(this).attr('data-hour');
		var min = $(this).attr('data-min');
		var hdegree = hour * 30 + (min / 2);
		var hrotate = 'rotate(' + hdegree + 'deg)';
		var mdegree = min * 6;
	    var mrotate = 'rotate(' + mdegree + 'deg)';
		$(this).find('.hour').css({'transform': mrotate + hrotate +'deg)', '-moz-transform': hrotate, '-webkit-transform': hrotate});
	    $(this).find('.min').css({'transform': mrotate + hrotate +'deg)', '-moz-transform': mrotate, '-webkit-transform': mrotate});
	});
	/* END clock */
	/* START poster-action */
	$('dl.posters a.poster-action').toggle(
		function () {
			$(this).addClass('checked');
		},
		function () {
			$(this).removeClass('checked');
	});
	/* END poster-action */
	/* START poster-spoiler */
	$('dl.posters dd .title-line h4').toggle(
		function () {
			$(this).parent().parent().find('div.poster-desc').slideToggle();
			$(this).parent().parent().parent().find('a.arrow-spoiler').addClass('open').show();
		},
		function () {
			$(this).parent().parent().find('div.poster-desc').slideToggle();
			$(this).parent().parent().parent().find('a.arrow-spoiler').removeClass('open').hide();
	});
	$('dl.posters dd').hover(
		function () {
			$(this).find('a.arrow-spoiler').css('display', 'block');
		},
		function () {
			if (!$(this).find('div.poster-desc').is(':visible')) $(this).find('a.arrow-spoiler').hide();
	});
	$('dl.posters dd a.arrow-spoiler').toggle(
		function () {
			$(this).parent().find('div.poster-desc').slideToggle();
			$(this).addClass('open');
		},
		function () {
			$(this).parent().find('div.poster-desc').slideToggle();
			$(this).removeClass('open');
			$(this).hide();
	});
	/* END poster-spoiler */
	/* START post-search */
	$('div.page-post div.post-nav a.search-btn').click(function () {
		$(this).parent().find('div.search-line').slideToggle();
		return false;
	});
	/* END post-search */
	/* START comment */
	$('ul.comment a.answer').toggle(
		function () {
			$(this).parent().find('> div.answer-form').show();
			$(this).addClass('active');
		},
		function () {
			$(this).parent().find('> div.answer-form').hide();
			$(this).removeClass('active');
	});
	/* END comment */
	/* START mail-spoiler */
	$('div.tape-mail a.msg').click(function() {
		var message = $(this).parent().parent();
		message.addClass('open');
		return false;
	});
	$('div.tape-mail a.arrow-spoiler').click(function () {
		var message = $(this).parent();
		if (message.hasClass('open')) {
			message.removeClass('open');
		}
		else {
			message.addClass('open');
		}
		return false;
	});
	/* END mail-spoiler */
	/* START search button disabled */
	$('input.text-arrow-search').keyup(function() {
		if ($(this).val() === '') $('input.arrow-search').attr('disabled','disabled').addClass('no-active');
		else $('input.arrow-search').removeAttr('disabled').removeClass('no-active');
	});
	$('#search-text').keyup(function() {
		if ($(this).val() === '') $('#search-btn').attr('disabled','disabled').addClass('no-active');
		else $('#search-btn').removeAttr('disabled').removeClass('no-active');
	});
	/* END search button disabled */
	/* START hint on places, users */
	$('div.section.places ul li > img, div.section.users ul li > img').bind('mouseenter', function() {
		$(this).parent().find('div.tip').show();
	});
	$('div.section.places ul li div.tip, div.section.users ul li div.tip').hover(
		function() {},
		function() {
		$(this).hide();
	});
	/* END hint on places, users */
	/* START popap in profile */
	$('div.profile .section.profile-main dl dd a.show-all').click(function() {
		$('#popap').show();
		var fixed = $(this).parent().find('div.popap');
		fixed.show().css('marginTop', 0);;
		var offset = $(window).scrollTop();
		$(window).scroll(function() {
			fixed.stop().animate({marginTop: $(window).scrollTop() - offset});
		});
		return false;		
	});
	$('div.profile .section.profile-main dl dd a.hide, #popap').click(function() {
		$('div.profile .section.profile-main dl dd .popap').css('marginTop', 0).hide();
		$('#popap').hide();
		return false;
	});
	/* END popap in profile */
	/* START enterform */
	var enter_top = ($(window).height() - 283)/2;
	$('div.enter-wrap').css('top',enter_top);
	$('ul.enter-menu > li a[href="#"]').click(function() {
		$('#popap-enter').show();
		return false;
	});
	$('#popap-enter a.close').click(function() {
		$('#popap-enter').hide();
		return false;
	});
	$('#enter input.enter-btn').click(function() {
        var $form = $('#enter');
        $.post($form.attr('action'), $form.serialize(), function(response){
            parseResponse($form, response)
        });
		return false;
	});
    $('#remember input.passwd-btn').click(function() {
        var $form = $('#remember');
        $.post($form.attr('action'), $form.serialize(), function(response){
            data = parseResponse($form, response)
            if(!data.errors){
                $('#popap-enter').fadeOut(2000);
                $form.input('input[type=text]').val('');
            }
        });
        return false;
    });
	$('#enter a').click(function() {
		$('#enter').hide();
		$('#remember').show();
		return false;
	});
	$('#enter').delegate('input:text, input:password', 'keyup', function(event) {
		if ($('#enter input:text').val() === '' || $('#enter input:password').val() === '' || $('#enter input:password').is('.placeholder') || $('#enter input:text').is('.placeholder')) $('#enter input.enter-btn').attr('disabled','disabled').addClass('no-active');
		else $('#enter input.enter-btn').removeAttr('disabled').removeClass('no-active');
	});
	$('#remember').delegate('input:text', 'keyup', function(event) {
		if ($('#remember input:text').val() === '') $('#remember input.passwd-btn').attr('disabled','disabled').addClass('no-active');
		else $('#remember input.passwd-btn').removeAttr('disabled').removeClass('no-active');
	});
	/* END enterform */
	/* START invite */
	var invite_top = ($(window).height() - 313)/2;
	$('div.invite-wrap').css('top',invite_top);
	$('#popap-invite a.close').click(function() {
		$('#popap-invite').hide();
		return false;
	});
	$('div.section.invite a.invite-link').click(function() {
		$('#popap-invite').show();
		return false;
	});
	$('div.section.invite-one a.invite-link').click(function() {
		$('#popap-invite .bottom-line p').html('Осталось 1 приглашение');
		$('#popap-invite').show();
		return false;
	});
	$('#popap-invite input.invite-btn').click(function() {
		$('#popap-invite').hide();
		$('#popap-invite input.text-invite').val('');
		$('input.invite-btn').attr('disabled','disabled').addClass('no-active');
		if ($('div.section.invite').is(':visible')) {
			$('div.section.invite').hide();
			$('div.section.invite-one').show();
		}
		else {
			$('div.section.invite-one').hide();
			$('div.section.invite-two').show();
		}
		return false;
	});
	$('input.text-invite').keyup(function() {
		if ($(this).val() === '') $('input.invite-btn').attr('disabled','disabled').addClass('no-active');
		else $('input.invite-btn').removeAttr('disabled').removeClass('no-active');
	});
	/* END invite */	
	/* END new message popap */
	var new_msg_top = ($(window).height() - 381)/2;
	$('div.new-msg-wrap').css('top',enter_top);
	$('#popap-new-msg input.friend').keyup(function() {
		if ($(this).val() === '') $('#popap-new-msg ul.friends').hide();
		else $('#popap-new-msg ul.friends').show();
	});
	$('#popap-new-msg ul.friends li a').click(function() {
		$('#popap-new-msg ul.friends').hide();
	});
	$('#popap-new-msg').delegate('input.friend, textarea', 'keyup', function(event) {
		if ($('#popap-new-msg input.friend').val() === '' || $('#popap-new-msg textarea').val() === '' || $('#popap-new-msg input.friend').is('.placeholder')) $('#popap-new-msg input.send-btn').attr('disabled','disabled').addClass('no-active');
		else $('#popap-new-msg input.send-btn').removeAttr('disabled').removeClass('no-active');
	});
	/* END new message popap */
	/* START calendar */
	$('#daily-events-calendar a').hover(
		function() {
			var pos = $(this).parent().position();
			$('div.calendar-tip').css('left',-pos.left-10);
			var hint = $('div.calendar-tip');
			$(this).parent().append('<b></b>').append(hint);
		},
		function() {
			var hint = $(this).parent().find('div.calendar-tip');
			$(this).parent().find('b').remove();
			$('body').append(hint);
	});
	/* END calendar */
	/* START add friends */
	$('div.profile-friends div.section.friends ul.friends ul > li a').click(function() {
		var div = $('div.friend-request-top');
		var offset = $(window).scrollTop() - 163 + ($(window).height() - div.height())/2;
		div.css('top',offset).show();
		return false;
	});
	/* END add friends */
	/* START face hint */
	$('ul.face-list li a img').hover(
		function() {
			var hint = $('#face-info');
			$(this).parent().append('<b></b>').append(hint);
		},
		function() {
			var hint = $('#face-info');
			$(this).parent().find('b').remove();
			$('body').append(hint);
	});
	/* END face hint */
	/* START deltee post */
	var post_top = ($(window).height() - 141)/2;
	$('div.post-delete-wrap').css('top',post_top);
	$('#popap-post-delete a.close').click(function() {
		$('#popap-post-delete').hide();
		return false;
	});
	$('div.profile-blog ul.blog ul li + li').click(function() {
		$('#popap-post-delete').show();
		return false;
	});
	/* END delete post */
});
