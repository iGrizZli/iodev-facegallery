function parseResponse($form, data){
    var data = $.parseJSON(data)
    $form.find('p.error').remove();
    $form.find('p.notice').remove();
    if(data.refresh){
        window.location.reload()
    }
    if(data.redirect){
        window.location.href = data.redirect
    }
    if(data.errors){
        for(hash in data.errors){
            $form.append('<p class="error">'+data.errors[hash]+'</p>')
        }
    }
    if(data.notices){
        for(hash in data.notices){
            $form.append('<p class="notice">'+data.notices[hash]+'</p>')
        }
    }

    return data
}

$(function(){
    $('input.load[type="file"]').each(function(){
        $(this).agileUploader({
            //submitRedirect: '/images/new',
            //formId: $(this).parents('form').attr('id'),
            flashVars: {
                firebug: false,
                form_action: '/images/new',
                file_limit: 3,
                max_post_size: (1000 * 1024),
                return_submit_response: true,
                max_width: 800,
                max_height: 800
            }
        })
    })
})