/********** Adriver AsyncJS loader V1.1 *************/
function debug(){}

function adriver (ph, prm, defer) {  
	try {
		this.ph = ph; 
		this.prm = {};
		for (var i in this.defaults) this.prm[i] = this.defaults[i];
		for (var i in prm) this.prm[i] = prm[i];
		for (var i in this.prm) this.req += i + "=" + this.prm[i] + "&";
		if(!adriver.items){ 
			adriver.items = {};
			this.addHandlers();
			this.checkFlash();
		}
		this.p = document.getElementById(ph);
		this.p.adriver = this;
		adriver.items[ph] = this;
		defer || this.load(); 
	}catch(e){}
}

adriver.prototype.ver = "1.1";
adriver.prototype.flashVer = [0];
adriver.prototype.domReady = false;
adriver.prototype.handlers = [];
adriver.prototype.defaults = {};
adriver.prototype.h = document.getElementsByTagName("head").item(0);
adriver.prototype.req = "http://ad.adriver.ru/cgi-bin/erle.cgi?";

		
adriver.prototype.loadScript = function(req, m){ //m - flag. set when loading from mirror
try {
		m || (req += "ph=" + this.ph + "&rnd=" + Math.round(Math.random()*10000));
		var s = document.createElement("script");
		s.setAttribute("type", "text/javascript");
		s.setAttribute("charset", "windows-1251");
		s.setAttribute("src", req);
		this.s = s;
		this.h.insertBefore(s, this.h.firstChild); 
	}catch(e){}
}

adriver.prototype.detachScript = function (){ try{ this.h.removeChild(this.s) }catch(e){}}

adriver.prototype.load = function (){	try{ this.loadScript(this.req) }catch(e){}}

adriver.prototype.onDomReady = function (f) {try{ this.domReady ? f() : this.handlers.push(f) }catch(e){}}


/************ dom ready state check **********************/

adriver.prototype.runHandlers = function(old_handler){
	try {
		if (old_handler){old_handler() }
		if (!this.domReady){
			adriver.prototype.domReady = true;
			var f; while (f = this.handlers.shift()) f();
		}
	}catch(e){}
}

adriver.prototype.addHandlers = function(){
	try {
		var d = document, t = this, safariTimeout;

		if (/WebKit/i.test(navigator.userAgent)) {
			safariTimeout = setInterval(function(){if (/loaded|complete/.test(d.readyState)) {clearInterval(safariTimeout); t.runHandlers() }}, 100);
		} else if (d.addEventListener) {
			d.addEventListener("DOMContentLoaded", function(){t.runHandlers()}, false);
		} else if (d.all && !window.opera) {
			var s = d.createElement("script");
			s.setAttribute("type", "text/javascript");
			s.setAttribute("src", "");			
			s.setAttribute("defer", "true");
			s.onreadystatechange = function(){if (this.readyState == "complete") t.runHandlers()} 
			this.h.insertBefore(s, this.h.firstChild); 
		}
		var old_handler = window.onload;
		window.onload = function(){t.runHandlers(old_handler)};
	} catch (e){}
}


adriver.prototype.checkFlash = function(req){
	debug("checking flash");
	try {
		if (req) return (this.flashVer[0] >= req);

		var d, n = navigator, m, f = 'Shockwave Flash';
		if((m = n.mimeTypes) && (m = m["application/x-shockwave-flash"]) && m.enabledPlugin && (n = n.plugins) && n[f]) {d = n[f].description}
		else if (window.ActiveXObject) { debug ("activeX flash detect"); try { d = (new ActiveXObject((f+'.'+f).replace(/ /g,''))).GetVariable('$version');} catch (e) {debug ("error in activeX detection")}} 
		debug ("version obj: " + d);
		if (d) adriver.prototype.flashVer = d.replace(/\D+/,'').split(/\D+/);
	
		debug ("flash " + this.flashVer + " detected");
	} catch (e){}
	return false;
}

//============optional components=================//

adriver.prototype.setAnchor = function (){this.anchor = this.addDiv(this.p, {position:"relative"});}

adriver.prototype.addPanel = function (id, params, inner){ 
	debug("add panel" + id);
	var panel = null;
	try {
		if (!this.panels){
			this.panels = {};
		}
		panel = this.addDiv(this.anchor, params, inner)  
		this.panels[id] = panel;
	}catch(e){}
	return panel;
}

adriver.prototype.addDiv = function (o, params, inner){
	var n = null;
	try {
		n = document.createElement("DIV");
		for (var i in params){ n.style[i] = params[i] } 
		if (inner) n.innerHTML = inner;
		o.insertBefore(n, o.firstChild); 
	}catch(e){}
	return n;
}

function adriver_dispatcher(ph, panel, command, arg){
	debug("dispatching " + command + ": " + arg + " to " + ph + "." + panel);
	try {
		var a = adriver.items[ph];
		if (a && a.dispatch) a.dispatch(panel, command, arg);
	} catch (e) {}
}

//============optional components=================//
