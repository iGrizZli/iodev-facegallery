$(document).ready(function () {
	var offset = $('#main-img').width();
	offset = (1000 - offset)/2;
	$('.mark-btn').click(function() {
		$('#main-img').imgAreaSelect({
			disable: false,
			fadeSpeed: 200,
			handles: true,
			parent: '#image-tagging',
			zIndex: 1,
			onSelectStart: function(img, selection)	{
				$('#image-tagging form').hide();
			},
			onSelectEnd: function(img, selection){
				$('input#x1').val(selection.x1 + offset);
				$('input#y1').val(selection.y1);
				$('input#w').val(selection.width);
				$('input#h').val(selection.height);
				$('#image-tagging form').css('left', selection.x2 + offset + 20);
				$('#image-tagging form').css('top', selection.y1);
				$('#image-tagging form').show();
				if (selection.width == 0 && selection.height == 0) $('#image-tagging form').hide();
			}		
		});
		return false;
	});
	$('#image-tagging form ul a').click(function() {
		$('#main-img').imgAreaSelect({
			disable: true,
			hide: true
		});
		$('#image-tagging form').hide();
		var name_face = $(this).attr('data-name');
		$('ul.mark-face li').last().append(',');
		$('ul.mark-face').append('<li><a class="name" href="#" data-left="' + $('input#x1').val() + '" data-top="' + $('input#y1').val() + '" data-width="' + $('input#w').val() + '" data-height="' + $('input#h').val() + '">' + name_face + '</a></li>');
		// ������� � ���� ��� ����������� � ���������� �������
		$('input#x1').val('');
		$('input#y1').val('');
		$('input#w').val('');
		$('input#h').val('');
		return false;
	});
	$('ul.mark-face').delegate('a', 'hover', function(event) {
		if (event.type === 'mouseenter') {
			var top = $(this).attr('data-top');
			var left = $(this).attr('data-left');
			var width = $(this).attr('data-width');
			var height = $(this).attr('data-height');
			$('#image-tagging').append('<div id="area-tagging" style="top:' + top + 'px; left: ' + left + 'px; width: ' + width + 'px; height: ' + height + 'px;"></div>');
		}
		else $('#area-tagging').remove();
	});
});
