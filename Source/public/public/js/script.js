$(document).ready(function () {
	/* START top-menu */
	$('ul.top-menu a.dropdown').bind('mouseenter', function () {
		$(this).parent().addClass('hover').find('div.dropdown-menu').show();
		$(this).parent().hover(
			function () {},
			function () {
				$(this).removeClass('hover').find('div.dropdown-menu').hide();
		});
	});
	/* END top-menu */
	/* START tabs */
	var active_tab = 0;
	$('ul.tabs a').click(function () {
		$('ul.tabs li.active').removeClass('active');
		active_tab = $(this).parent().index();
		$('div.box').hide();
		$(this).parent().addClass('active');
		$('div.box').eq(active_tab).show();
		return false;
	});
	/* END tabs */
	/* START section-info block */
	$('div.section:has(> .section-info):not(.gallery)').hover(
		function () {
			$(this).find('> .section-info').addClass('hover');
		},
		function () {
			$(this).find('> .section-info').removeClass('hover');
	});
	$('div.section.posts > ul > li:has(> .section-info)').hover(
		function () {
			$(this).find('> .section-info').addClass('hover');
		},
		function () {
			$(this).find('> .section-info').removeClass('hover');
	});
	$('div.section.album > ul > li:has(> .section-info) .section-content, ul.stream-posts > li:has(> .section-info) .section-content').hover(
		function () {
			$(this).parent().find('> .section-info').addClass('hover');
		},
		function () {
			$(this).parent().find('> .section-info').removeClass('hover');
	});
	/* END section-info block */
	/* START clock */
	$('ul.clock').each(function () {
		var hour = $(this).attr('data-hour');
		var min = $(this).attr('data-min');
		var hdegree = hour * 30 + (min / 2);
		var hrotate = 'rotate(' + hdegree + 'deg)';
		var mdegree = min * 6;
	    var mrotate = 'rotate(' + mdegree + 'deg)';
		$(this).find('.hour').css({'transform': mrotate + hrotate +'deg)', '-moz-transform': hrotate, '-webkit-transform': hrotate});
	    $(this).find('.min').css({'transform': mrotate + hrotate +'deg)', '-moz-transform': mrotate, '-webkit-transform': mrotate});
	});
	/* END clock */
	/* START poster-action */
	$('dl.posters a.poster-action').toggle(
		function () {
			$(this).addClass('checked');
		},
		function () {
			$(this).removeClass('checked');
	});
	/* END poster-action */
	/* START poster-spoiler */
	$('dl.posters dd .title-line h4').toggle(
		function () {
			$(this).parent().parent().find('div.poster-desc').slideToggle();
			$(this).parent().parent().parent().find('a.arrow-spoiler').addClass('open').show();
		},
		function () {
			$(this).parent().parent().find('div.poster-desc').slideToggle();
			$(this).parent().parent().parent().find('a.arrow-spoiler').removeClass('open').hide();
	});
	$('dl.posters dd').hover(
		function () {
			$(this).find('a.arrow-spoiler').css('display', 'block');
		},
		function () {
			if (!$(this).find('div.poster-desc').is(':visible')) $(this).find('a.arrow-spoiler').hide();
	});
	$('dl.posters dd a.arrow-spoiler').toggle(
		function () {
			$(this).parent().find('div.poster-desc').slideToggle();
			$(this).addClass('open');
		},
		function () {
			$(this).parent().find('div.poster-desc').slideToggle();
			$(this).removeClass('open');
			$(this).hide();
	});
	/* END poster-spoiler */
	/* START post-search */
	$('div.page-post div.post-nav a.search-btn').click(function () {
		$(this).parent().find('div.search-line').slideToggle();
		return false;
	});
	/* END post-search */
	/* START comment */
	$('ul.comment a.answer').toggle(
		function () {
			$(this).parent().find('> div.answer-form').show();
			$(this).addClass('active');
		},
		function () {
			$(this).parent().find('> div.answer-form').hide();
			$(this).removeClass('active');
	});
	/* END comment */
	/* START mail-spoiler */
	$('div.tape-mail a.msg').click(function() {
		var message = $(this).parent().parent();
		message.addClass('open');
		return false;
	});
	$('div.tape-mail a.arrow-spoiler').click(function () {
		var message = $(this).parent();
		if (message.hasClass('open')) {
			message.removeClass('open');
		}
		else {
			message.addClass('open');
		}
		return false;
	});
	/* END mail-spoiler */
	/* START search button disabled */
	$('input.text-arrow-search').keyup(function() {
		if ($(this).val() === '') $('input.arrow-search').attr('disabled','disabled').addClass('no-active');
		else $('input.arrow-search').removeAttr('disabled').removeClass('no-active');
	});
	/* END search button disabled */
	/* START hint on places, users */
	$('div.section.places ul li > img, div.section.users ul li > img').bind('mouseenter', function() {
		$(this).parent().find('div.tip').show();
	});
	$('div.section.places ul li div.tip, div.section.users ul li div.tip').hover(
		function() {},
		function() {
		$(this).hide();
	});
	/* END hint on places, users */
});
