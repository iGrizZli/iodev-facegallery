$(document).ready(function () {
	$('div.slideshow').each(function() {
		$(this).cycle({
			fx: 'fade', 
			speed: 1000, 
			pager: $(this).parent().find('.slideshow-nav'),
			width: 302
		});
	});
	$('.slideshow').cycle('pause');
});
