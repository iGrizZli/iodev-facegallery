$(document).ready(function () {
	$('textarea.resize-title, textarea.resize-desc').autoResize({
		extraSpace: 0
	});
	$('h1 a').click(function() {
		$('h1').hide();
		var textarea = $('textarea.resize-title').last();
		$('textarea.resize-title').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$('textarea.resize-title').hide();
				$('h1').show();				
			}
		});
		return false;
	});
	$('div.post-desc a').click(function() {
		$('div.post-desc').hide();
		var textarea = $('textarea.resize-desc').last();
		$('textarea.resize-desc').css('display', 'block');
		textarea.focus();
		textarea.focusout(function() {
			if ($(this).val() === '') {
				$('textarea.resize-desc').hide();
				$('div.post-desc').show();				
			}
		});
		return false;
	});
});
