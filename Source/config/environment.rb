# Be sure to restart your server when you modify this file

# Specifies gem version of Rails to use when vendor/rails is not present
#RAILS_GEM_VERSION = '2.3.2' unless defined? RAILS_GEM_VERSION

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')
#require 'rack/cache'

Rails::Initializer.run do |config|
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # -- all .rb files in that directory are automatically loaded.

  # Add additional load paths for your own custom dirs
  # config.load_paths += %W( #{RAILS_ROOT}/extras )
  
  config.load_paths += %W( #{RAILS_ROOT}/app/middleware )
  
  #config.action_controller.session_store = :active_record_store

  # Specify gems that this application depends on and have them installed with rake gems:install
  # config.gem "bj"
  # config.gem "hpricot", :version => '0.6', :source => "http://code.whytheluckystiff.net"
  # config.gem "sqlite3-ruby", :lib => "sqlite3"
  # config.gem "aws-s3", :lib => "aws/s3"

  # Only load the plugins named here, in the order given (default is alphabetical).
  # :all can be used as a placeholder for all plugins not explicitly named
  # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

  # Skip frameworks you're not going to use. To use Rails without a database,
  # you must remove the Active Record framework.
  # config.frameworks -= [ :active_record, :active_resource, :action_mailer ]

  # Activate observers that should always be running
  # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

  config.after_initialize do
    ActionView::Base.sanitized_allowed_tags.delete 'font'
    ActionView::Base.sanitized_allowed_tags = 'object', 'embed', 'param'
    ActionView::Base.sanitized_allowed_attributes = 'name', 'value', 'allowscriptaccess', 'allowfullscreen', 'type', 'wmode'
  end

  config.gem 'scoped_search', :source => 'http://gemcutter.org/'

  # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
  # Run "rake -D time" for a list of tasks for finding time zone names.
  config.time_zone = 'Moscow'
  #config.active_record.default_timezone = :moscow
  config.active_record.observers = :user_observer, :message_observer
  
  config.gem 'mislav-will_paginate', :version => '~> 2.3.8', :lib => 'will_paginate', :source => 'http://gems.github.com'
    
  config.gem 'mime-types', :lib => 'mime/types'
  
  config.gem 'hpricot'
  config.gem 'htmlentities'
  config.gem 'russian' 
  config.gem 'nokogiri'
  config.gem 'sanitize'
  #config.gem "newrelic_rpm"

  config.plugins = [ :vote_fu, :acts_as_commentable_with_threading, :all ] 
  #config.gem 'rack-cache'#, :lib => 'rack-cache'
  #config.middleware.use Rack::Cache,
  #   :verbose => true,
  #   :metastore   => 'file:{Rails.root}/tmp/cache/rack/meta',
  #   :entitystore => 'file:{Rails.root}/tmp/cache/rack/body'
  #config.gem 'image_science'

  # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
  # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}')]
  config.i18n.default_locale = :ru
  
  MAGAZINE_ISSUE = 5
  MAGAZINE_ISSUE_EN = 4
end
