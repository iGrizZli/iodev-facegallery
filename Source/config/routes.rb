ActionController::Routing::Routes.draw do |map|

  map.register '/register/:code', :controller => 'users', :action => 'create', :code => nil
  map.connect '/signup/done', :controller => 'users', :action => 'created'
  map.signup '/signup/:code', :controller => 'users', :action => 'new', :code => nil
  #map.connect '/fg_signup', :controller => 'users', :action => 'new'
  
  map.connect '/bio_signup/:invite_hash', :controller => 'bio_signup', :action => 'create', :invite_hash => nil, :conditions => { :method => :post }
  map.connect '/bio_signup/:invite_hash/cu', :controller => 'bio_signup', :action => 'connect_current_user', :conditions => { :method => :post }
  map.connect '/bio_signup/:invite_hash/check_login', :controller => 'bio_signup', :action => 'check_login', :conditions => { :method => :post }
  map.connect '/bio_signup/:invite_hash', :controller => 'bio_signup', :action => 'index', :invite_hash => nil, :conditions => { :method => :get }
  
  map.connect '/users/mamba_api.xml', :controller => 'users', :action => 'mamba_api', :format => :xml

  map.resources :users

  map.connect '/registration/done', :controller => 'register_applications', :action => 'created'
  #map.resources :register_applications, :as => 'registration'

  map.resource :session

	map.activate '/activate/:activation_code',
           :controller => 'users',
           :action => 'activate',
           :activation_code => nil

  #map.connect '/forgot_password', :controller => 'passwords', :action => 'create', :conditions => { :method => :post }
  #map.forgot_password '/forgot_password', :controller => 'passwords', :action => 'new'
  #map.reset_password '/reset_password/:id', :controller => 'passwords', :action => 'update', :id => nil, :conditions => { :method => :put }
  #map.reset_password '/reset_password/:id', :controller => 'passwords', :action => 'edit', :id => nil


  map.welcome_message '/welcome',
           :controller => 'users',
           :action => 'welcome_message'
  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller

  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing the them or commenting them out if you're using named routes and resources.

  map.resources :apple_questions, :as => 'apple_concierge', :controller => 'apple_concierge'

  #place_gallery

  map.event 'fgc/events/show/:id', :controller => 'place_gallery/events', :action => 'show'
  #map.connect 'fgc/events/:page', :controller => 'place_gallery/events', :action => 'index', :page=> 1, :requirements => { :page => /\d+/ }
  #map.connect 'fgc/events/:place_category/:page', :controller => 'place_gallery/index', :action => 'index', :page => 1, :requirements => { :page => /\d+/ }

  map.report 'magazine/reports/show/:id', :controller => 'place_gallery/reports', :action => 'show'
  map.report_photo 'magazine/reports/show/:id/photo/:image_id', :controller => 'place_gallery/reports', :action => 'view_image'
  map.connect 'magazine/reports/:page', :controller => 'place_gallery/reports', :action => 'index', :page=> 1, :requirements => { :page => /\d+/ }

  #map.report 'placegallery/reports/show/:id', :controller => 'place_gallery/reports', :action => 'show'
  #map.connect 'placegallery/reports/:page', :controller => 'place_gallery/reports', :action => 'index', :page=> 1, :requirements => { :page => /\d+/ }
  #map.connect 'placegallery/reports/:place_category/:page', :controller => 'place_gallery/reports', :action => 'index', :page => 1, :requirements => { :page => /\d+/ }

  #map.place 'placegallery/places/:place_category/show/:id', :controller => 'place_gallery/place', :action => 'show'
  map.connect 'placegallery/daily_events/:date/tooltip', :controller => 'place_gallery/daily_events', :action => 'tooltip', :date => nil
  map.connect 'placegallery/daily_events/:date', :controller => 'place_gallery/daily_events', :action => 'index', :date => nil
  map.connect 'placegallery/daily_events/:action/:id', :controller => 'place_gallery/daily_events'
  map.daily_event 'placegallery/daily_events/:action/:id', :controller => 'place_gallery/daily_events', :action => :show
  
  map.connect 'fgc/places/', :controller => 'place_gallery/place_gallery', :action => 'index'#, :page=> 1, :requirements => { :page => /\d+/ }
  map.connect 'fgc/places/:place_category/', :controller => 'place_gallery/place_gallery', :action => 'index'#, :page => 1, :requirements => { :page => /\d+/ }
  map.connect 'placegallery', :controller => 'place_gallery/index', :action => 'index'
  map.place 'fgc/places/:place_category/:id/:action', :controller => 'place_gallery/place', :action => 'show'
  #user
  map.connect 'user/:user_id/portfolio/:action/:id', :controller => 'user/portfolio', :id => nil
  map.connect 'user/:user_id/gallery/:action', :controller => 'user/gallery'
  map.connect 'user/:user_id/gallery/:action/:id', :controller => 'user/gallery'
  map.user_album 'user/:user_id/gallery/view_album/:id', :controller => 'user/gallery'
  map.user_image 'user/:user_id/gallery/view_album/:id/:image_id', :controller => 'user/gallery', :action => 'view_image'
  map.user_image_delete 'user/:user_id/gallery/view_album/:id/:image_id/delete', :controller => 'user/gallery', :action => 'delete_image'
  map.resources :blogs, :path_prefix => '/user/:user_id', :controller => 'user/blog'
  map.resources :messages, :path_prefix => '/user/:user_id', :controller => 'user/messages'
  map.resources :site_invites, :path_prefix => '/user/:user_id', :controller => 'user/site_invites'
  map.resources :videos



  map.connect 'user/:user_id/blog/:action', :controller => 'user/blog'
  map.connect 'user/:user_id/blog/:action/:id', :controller => 'user/blog'
  map.user 'user/:user_id/:action', :controller => 'user', :action => 'index'

  #fgc
  map.connect 'fgc/news/:action/:id', :controller => 'fgc/news', :action => 'index', :id => nil
  map.connect 'fgc/travel/:action/:id', :controller => 'fgc/travel', :action => 'index', :id => nil
  #map.fgc_places 'fgc/places/:place_category', :controller => 'fgc', :action => 'places', :place_category => nil
  map.fgc 'fgc/:action', :controller => 'fgc', :action => 'index'



  #bio
  map.connect 'facegallery/', :controller => 'bio', :action => 'index'#, :page=> 1, :requirements => { :page => /\d+/ }
  map.connect 'facegallery/:bio_category/', :controller => 'bio', :action => 'list'#, :page => 1, :requirements => { :page => /\d+/ }
  map.bio 'facegallery/:bio_category/:id', :controller => 'bio', :action => 'show'
  map.connect 'facegallery/:bio_category/:id/vote', :controller => 'bio', :action => 'vote'
  
  #magazine
  map.connect 'magazine/editors_letter/', :controller => 'magazine/index', :action => 'editors_letter'
  map.magazine_interview_index 'magazine/interview/', :controller => 'magazine/index', :action => 'interview_index',  :magazine_section => 'interview'
  map.magazine_interview 'magazine/interview/:id', :controller => 'magazine/index', :action => 'interview_show', :magazine_section => 'interview'
  map.magazine 'magazine/', :controller => 'magazine/index'
  map.magazine_section 'magazine/:magazine_section/', :controller => 'magazine/index', :action => 'list'
  map.magazine_section_xml 'magazine/:magazine_section/xml', :controller => 'magazine/index', :action => 'list', :format => 'xml', :page=> 1, :requirements => { :page => /\d+/ }
  map.magazine_article 'magazine/:magazine_section/:id', :controller => 'magazine/index', :action => 'show'#, :requirements => { :id => /\d+/ }
  map.magazine_article_xml 'magazine/:magazine_section/:id/xml', :controller => 'magazine/index', :action => 'show',:format => 'xml'

  map.magazine_subsection 'magazine/:magazine_section/:magazine_subsection', :controller => 'magazine/index', :page=> 1, :requirements => { :page => /\d+/ }
  map.connect 'magazine/', :controller => 'magazine/index', :page=> 1, :requirements => { :page => /\d+/ }

  map.connect 'about_face_gallery', :controller => 'documents', :action => 'view', :alias => 'about_face_gallery'
  map.connect 'about_the_team', :controller => 'documents', :action => 'view', :alias => 'about_the_team'
  map.connect 'partners', :controller => 'documents', :action => 'view', :alias => 'partners'
  map.connect 'contacts', :controller => 'documents', :action => 'view', :alias => 'contacts'
  map.connect 'guide', :controller => 'documents', :action => 'view', :alias => 'guide'
  map.connect 'rules', :controller => 'documents', :action => 'view', :alias => 'rules'
  map.connect 'bio_howto', :controller => 'documents', :action => 'view', :alias => 'bio_howto'

  map.connect 'survey', :controller => 'fg_survey'

  map.connect 'votes/:action', :controller => 'votes'
  #search
  map.connect 'search/:search', :controller => 'search', :search => nil
  
  #contest
  map.resources :contest_sessions, :as => 'fg_talent_hunt', :member => { :vote => :get } do |contest|
    contest.resources :photos, :controller => 'contest_sessions/contest_photos'
  end
  
  map.connect 'fg_talent_hunt2/:action', :controller => 'fg_talent_hunt', :action => 'index'
 
 
  #community 
  map.resources :posts, :as => 'community/posts/:post_section'
  map.connect '/community/posts', :controller => 'posts', :action => 'posts_front'
  map.community '/community/:action/:subaction', :controller => 'community', :subaction => nil

  map.connect '/photo_tags/:action/:id', :controller => 'photo_tags'

  map.comments 'comments/:action/:id', :controller => 'comments'

  map.root :controller => 'welcome', :action => 'index'
  map.connect '/fb/:action', :controller => 'feedback', :action => 'index'
  #map.namespace :admin do |admin|
  #  admin.connect ':controller/:action', :controller => 'index', :namespace => 'admin', :action => 'index'
  #end
  map.connect '/contest_quotes/:action', :controller => 'contest_quotes', :action => 'index'
  
  #barentz
  map.barentz_article '/barentz/show/:id', :controller => 'barentz', :action => 'show'
  map.connect '/barentz/:category', :controller => 'barentz', :action => 'index', :category => nil
  
  map.namespace :admin do |admin|
    admin.resources :bios, :active_scaffold => true
    admin.resources :bio_categories, :active_scaffold => true
    admin.resources :users, :active_scaffold => true
    admin.resources :magazine_articles, :active_scaffold => true, :collection => { :autocomplete_tag_list => :post }
    admin.resources :magazine_interviews, :active_scaffold => true
    admin.resources :events, :active_scaffold => true
    admin.resources :reports, :active_scaffold => true, :has_many => :report_photos
    admin.resources :daily_events, :active_scaffold => true
    admin.resources :feedback_messages, :controller => 'feedback', :active_scaffold => true
    admin.resources :places, :active_scaffold => true
    admin.resources :place_categories, :active_scaffold => true
    admin.resources :documents, :active_scaffold => true
    admin.resources :news_items, :active_scaffold => true
    admin.resources :daily_events, :active_scaffold => true
    admin.resources :event_categories, :active_scaffold => true
    admin.resources :magazine_sections, :active_scaffold => true
    admin.resources :article_images, :active_scaffold => true
    admin.resources :article_page_images, :active_scaffold => true
    admin.resources :magazine_issues, :active_scaffold => true
    admin.resources :report_photos, :active_scaffold => true
    admin.resources :best_bloggers, :active_scaffold => true
    admin.resources :editors, :active_scaffold => true
    admin.resources :register_applications, :active_scaffold => true
    admin.resources :contest_sessions, :active_scaffold => true
    admin.resources :contest_photos, :active_scaffold => true
    admin.resources :fg_contest_questions, :active_scaffold => true
    admin.resources :comments, :active_scaffold => true
    admin.resources :fg_recommendations, :active_scaffold => true
    admin.resources :trips, :active_scaffold => true
    admin.resources :fg_guests, :active_scaffold => true
    admin.resources :companies, :active_scaffold => true
    admin.resources :videos, :active_scaffold => true
    admin.resources :contest_quotes, :active_scaffold => true
    admin.resources :contest_pics, :active_scaffold => true
    admin.resources :barentz_articles, :active_scaffold => true
    admin.resources :barentz_images, :active_scaffold => true
    admin.resources :barentz_categories, :active_scaffold => true
    admin.resources :bio_wishers
  end
  map.connect 'admin/', :controller => 'admin/index'
  map.connect 'admin/fg_survey_report', :controller => 'admin/fg_survey_report'
  map.connect 'resize/*path', :controller => 'resize', :action => 'index' 

  # NEW ROUTES
  map.logout '/logout', :controller => 'sessions', :action => 'destroy'
  map.forgot_password '/forgot_password', :controller => 'sessions', :action => 'forgot_password'
  map.change_password '/reset_password/:id', :controller => 'sessions', :action => 'change_password', :conditions => { :method => :put }
  map.reset_password '/reset_password/:id', :controller => 'sessions', :action => 'reset_password'
  map.registration '/registration', :controller => 'users', :action => 'registration'

  map.connect '/:action', :controller => 'stream'
  #map.connect ':controller/:action/:id'
  #map.connect ':controller/:action/:id.:format'
end

