Paperclip.interpolates :user_id do |attachment, style|
  attachment.instance.user_id
end
Paperclip.interpolates :user_album_id do |attachment, style|
  attachment.instance.user_album_id
end
Paperclip.interpolates :place_id do |attachment, style|
  attachment.instance.place_id
end
Paperclip.interpolates :report_id do |attachment, style|
  attachment.instance.report_id
end
Paperclip.interpolates :magazine_article_id do |attachment, style|
  attachment.instance.magazine_article_id
end