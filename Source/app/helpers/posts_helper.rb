module PostsHelper
  def single_post?
    controller.action_name == 'show'
  end
end
