module Admin::BiosHelper
  def birthday_form_column(record, input_name)
    date_select :record, :birthday, :name => input_name, :start_year => Time.now.year, :end_year => 1894, :include_blank => true, :default => { :day => nil, :month => nil, :year => nil }
  end
  
  def gender_form_column(record, input_name)
    radio_button(:record, :gender, "m", :name => input_name) + " мужик " + radio_button(:record, :gender, "f", :name => input_name) + " немужик "
  end
  
  def text_form_column(record, input_name)
    text_area(:record, :text, :name => input_name) + "<script>jQuery('textarea').markItUp(mySettings);</script>"
  end
end
