# By Henrik Nyh <http://henrik.nyh.se> 2008-01-30.
# Free to modify and redistribute with credit.
 
# modified by Dave Nolan <http://textgoeshere.org.uk> 2008-02-06
# Ellipsis appended to text of last HTML node
# Ellipsis inserted after final word break
 
# modified by Mark Dickson <mark@sitesteaders.com> 2008-12-18
# Option to truncate to last full word
# Option to include a 'more' link
# Check for nil last child
 
# modified by Ken-ichi Ueda <http://kueda.net> 2009-09-02
# Rails 2.3 compatability (chars -> mb_chars), via Henrik
# Hpricot 0.8 compatability (avoid dup on Hpricot::Elem)
 
require "rubygems"
require "hpricot"
#require "nokogiri"

module TextHelper
 
  def html_truncate111(input, num_words = 15, truncate_string = "...")
  	doc = Nokogiri::HTML(input)
  
  	current = doc.children.first
  	count = 0
   
  	while true
  		# we found a text node
  		if current.is_a?(Nokogiri::XML::Text)
  			count += current.text.split.length
  			# we reached our limit, let's get outta here!
  			break if count > num_words
  			previous = current
  		end
  
  		if current.children.length > 0
  			# this node has children, can't be a text node,
  			# lets descend and look for text nodes
  			current = current.children.first
  		elsif !current.next.nil?
  			#this has no children, but has a sibling, let's check it out
  			current = current.next
  		else 
  			# we are the last child, we need to ascend until we are
  			# either done or find a sibling to continue on to
  			n = current
  			while !n.is_a?(Nokogiri::HTML::Document) and n.parent.next.nil?
  				n = n.parent
  			end
  
  			# we've reached the top and found no more text nodes, break
  			if n.is_a?(Nokogiri::HTML::Document)
  				break;
  			else
  				current = n.parent.next
  			end
  		end
  	end
  
  	if count >= num_words
  	  unless count == num_words
    		new_content = current.text.split
    		
        # If we're here, the last text node we counted eclipsed the number of words
        # that we want, so we need to cut down on words.  The easiest way to think about
        # this is that without this node we'd have fewer words than the limit, so all
        # the previous words plus a limited number of words from this node are needed.
        # We simply need to figure out how many words are needed and grab that many.
        # Then we need to -subtract- an index, because the first word would be index zero.
        
        # For example, given:
        # <p>Testing this HTML truncater.</p><p>To see if its working.</p>
        # Let's say I want 6 words.  The correct returned string would be:
        # <p>Testing this HTML truncater.</p><p>To see...</p>
        # All the words in both paragraphs = 9
        # The last paragraph is the one that breaks the limit.  How many words would we
        # have without it? 4.  But we want up to 6, so we might as well get that many.
        # 6 - 4 = 2, so we get 2 words from this node, but words #1-2 are indices #0-1, so
        # we subtract 1.  If this gives us -1, we want nothing from this node. So go back to
        # the previous node instead.
        index = num_words-(count-new_content.length)-1
        if index >= 0
          new_content = new_content[0..index]
    		  current.content = new_content.join(' ') + truncate_string
  		  else
  		    current = previous
  		    current.content = current.content + truncate_string
  	    end
  	  end
  
  		# remove everything else
  		while !current.is_a?(Nokogiri::HTML::Document)
  			while !current.next.nil?
  				current.next.remove
  			end
  			current = current.parent
  		end
  	end
  
  	# now we grab the html and not the text.
  	# we do first because nokogiri adds html and body tags
  	# which we don't want
  	doc.root.children.first.inner_html
  end

  
  # Like the Rails _truncate_ helper but doesn't break HTML tags, entities, and optionally. words.
  def truncate_html(text, options={})
    return if text.nil?
    
    max_length = options[:max_length] || 30
    ellipsis = options[:ellipsis] || "..."
    words = options[:words] || true
    # use :link => link_to('more', post_path), or something to that effect
    
    doc = Hpricot(text.to_s)
    ellipsis_length = Hpricot(ellipsis).inner_text.mb_chars.length
    content_length = doc.inner_text.mb_chars.length
    actual_length = max_length - ellipsis_length
 
    if content_length > max_length
      truncated_doc = doc.truncate(actual_length)
      
      if words
        word_length = actual_length - (truncated_doc.inner_html.mb_chars.length - truncated_doc.inner_html.rindex(' '))
        truncated_doc = doc.truncate(word_length)
      end
 
      last_child = truncated_doc.children.last
      if last_child.inner_html.nil?
        truncated_doc.inner_html + ellipsis + options[:link] if options[:link]
      else
        #last_child.inner_html = last_child.inner_html.gsub(/\W.[^\s]+$/, "") + ellipsis
        #last_child.inner_html += options[:link] if options[:link]
        truncated_doc
      end
    else
      if options[:link]
        last_child = doc.children.last
        if last_child.inner_html.nil?
          doc.inner_html + options[:link]
        else
          last_child.inner_html = last_child.inner_html.gsub(/\W.[^\s]+$/, "") + options[:link]
          doc
        end
      else
        text.to_s
      end
    end
  end
 
 
end
 
module HpricotTruncator
  module NodeWithChildren
    def truncate(max_length)
      return self if inner_text.mb_chars.length <= max_length
      truncated_node = if self.is_a?(Hpricot::Doc)
        self.dup
      else
        self.class.send(:new, self.name, self.attributes)
      end
      truncated_node.children = []
      each_child do |node|
        remaining_length = max_length - truncated_node.inner_text.mb_chars.length
        break if remaining_length <= 0
        truncated_node.children << node.truncate(remaining_length)
      end
      truncated_node
    end
  end
 
  module TextNode
    def truncate(max_length)
      # We're using String#scan because Hpricot doesn't distinguish entities.
      Hpricot::Text.new(content.scan(/&#?[^\W_]+;|./).first(max_length).join)
    end
  end
 
  module IgnoredTag
    def truncate(max_length)
      self
    end
  end
end
 
Hpricot::Doc.send(:include, HpricotTruncator::NodeWithChildren)
Hpricot::Elem.send(:include, HpricotTruncator::NodeWithChildren)
Hpricot::Text.send(:include, HpricotTruncator::TextNode)
Hpricot::BogusETag.send(:include, HpricotTruncator::IgnoredTag)
Hpricot::Comment.send(:include, HpricotTruncator::IgnoredTag)




