class PlacePhoto < ActiveRecord::Base
  belongs_to :place
  has_attached_file :photo, :styles => { :original => ["850x520>", :jpg], :thumbnail => ["50x50^", :jpg] }, :convert_options => { :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity center -extent 50x50" },
                            :url => "/images/places/:place_id/gallery/:id_:style.:extension",
                            :path => ":rails_root/public/images/places/:place_id/gallery/:id_:style.:extension"
  validates_attachment_presence :photo
end
