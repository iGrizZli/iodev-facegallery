class BarentzArticle < ActiveRecord::Base
	has_many :barentz_images
	belongs_to :barentz_category
  acts_as_commentable

  has_attached_file :image, :styles => { :original => ["440x350>", :jpg], :mainpage => ["110x60^", :jpg], :thumbnail => ["200x120^", :jpg] }, :convert_options => { :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity center -extent 200x120", :mainpage => "-unsharp 0.3x0.3+5+0 -gravity center -extent 110x60" },
                            :url => "/images/barentz/articles/:id/:style.:extension",
                            :path => ":rails_root/public/images/barentz/articles/:id/:style.:extension"
                            
  named_scope :published, :conditions => { :published => true }
  default_scope :order => "id DESC"
  
  def to_param
    "#{id}-#{title.parameterize}"
  end
end
