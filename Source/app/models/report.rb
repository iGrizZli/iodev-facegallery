class Report < ActiveRecord::Base
  belongs_to :place
  has_many :report_photos
  acts_as_commentable
  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  named_scope :fgc, :conditions => { :fgc => true }

  default_scope :order => 'created_at DESC'
  
  localizes :name, :description
  
  has_attached_file :picture, :styles => { :original => ["335x300>", :jpg], :thumbnail => ["240x110^", :jpg], :mainpage => ["110x60^", :jpg] }, :convert_options => { :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity north -extent 240x110", :mainpage => "-unsharp 0.3x0.3+5+0 -gravity north -extent 110x60" },
                              :url => "/images/reports/:id/:style.:extension",
                              :path => ":rails_root/public/images/reports/:id/:style.:extension"
                              
  define_index do
    indexes :name
    indexes description
    indexes place.name, :as => :place_name
    indexes place.address, :as => :place_address
    where "reports.published_ru = '1'"
    set_property :field_weights => {
      :name => 10,
      :place_name    => 6,
      :place_address    => 4,
      :description    => 2
    }
  end

  def self.latest_for_small_block
    Report.published.find :all, :limit => 2
  end
end
