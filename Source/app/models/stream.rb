class Stream < ActiveRecord::Base
  has_many :stream_categories
  has_many :posts
end
