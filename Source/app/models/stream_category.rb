class StreamCategory < ActiveRecord::Base
  belongs_to :stream
  has_many :posts
end
