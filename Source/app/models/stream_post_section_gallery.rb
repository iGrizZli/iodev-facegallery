class StreamPostSectionGallery < StreamPostSection
  has_many :stream_post_section_photos, :order => "position", :dependent => :destroy
end