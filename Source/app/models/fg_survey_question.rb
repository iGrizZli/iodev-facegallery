class FgSurveyQuestion < ActiveRecord::Base
  default_scope :order => 'position'
  
  def options_hash
    if self.options
      h = Hash.new
      self.options.split("|").each do |o|
        s = o.split(":")
        h[s[0]] = s[1]
      end
      h
    end
  end
end
