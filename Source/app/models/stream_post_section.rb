class StreamPostSection < ActiveRecord::Base
  belongs_to :post, :class_name => 'StreamSection'
  validates_presence_of :post
  validates_presence_of :position

  def self.factory(type, params=nil)
    section_model = case type
      when 'text' then StreamPostSectionText
      when 'photo' then StreamPostSectionPhoto
      when 'gallery' then StreamPostSectionGallery
      when 'audio' then StreamPostSectionAudio
      when 'video' then StreamPostSectionVideo
    end
    section_model.new(params)
  end


  def up
    if self.position > 1
      self.post.sections.where(:position => (self.position - 1)).each{|sec| sec.position= sec.position + 1}
      self.position= self.position - 1
    end
  end

  def down
    if self.position < self.post.sections.size
      self.post.sections.where(:position => (self.position + 1)).each{|sec| sec.position= sec.position - 1}
      self.position= self.position + 1
    end
  end

end