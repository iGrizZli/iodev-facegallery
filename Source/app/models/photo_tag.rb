class PhotoTag < ActiveRecord::Base
  belongs_to :photo_taggable, :polymorphic => true
  belongs_to :person_taggable, :polymorphic => true

  def self.find_photo_taggable(class_str, taggable_id)
    class_str.constantize.find(taggable_id)
  end

  def self.find_person_taggable(class_str, taggable_id)
    class_str.constantize.find(taggable_id)
  end
  
end
