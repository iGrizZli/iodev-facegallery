class Video < ActiveRecord::Base
  belongs_to :user
  belongs_to :bio
  
  acts_as_commentable
  
  named_scope :active, :conditions => { :active => true }
  
  has_attached_file :thumbnail, :styles => { :original => ["120x66>", :jpg] },
                             :url => "/images/videos/:id/:style.:extension",
                             :path => ":rails_root/public/images/videos/:id/:style.:extension"
end
