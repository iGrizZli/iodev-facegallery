class ReportPhoto < ActiveRecord::Base
  belongs_to :report

  acts_as_list :scope => :report
  acts_as_commentable
  has_attached_file :report_photo, :styles => { :original => ["1024x1024>", :jpg], :original_w => ["1024x1024>", :jpg], :small_w => ["450x450>", :jpg], :thumbnail => ["120x150>", :jpg] }, :convert_options => { :thumbnail => "-unsharp 0.3x0.3+5+0", :original_w => "#{Rails.root}/photo-watermark.png -gravity southeast -composite", :small_w => "#{Rails.root}/photo-watermark.png -gravity southeast -composite"},
                                   :url => "/images/reports/:report_id/gallery/:id_:style.:extension",
                                   :path => ":rails_root/public/images/reports/:report_id/gallery/:id_:style.:extension"
  validates_attachment_presence :report_photo
  has_many :photo_tags, :as => :photo_taggable, :dependent => :destroy

  default_scope :order => 'position'
end
