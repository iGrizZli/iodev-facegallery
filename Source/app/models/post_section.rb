class PostSection < ActiveRecord::Base
  has_many :posts
  
  localizes :name
  
  default_scope :order => 'position'
  named_scope :active, :conditions => {:active => 1}
  
  def to_param
    "#{urlname}"
  end
end
