class UserMailer < ActionMailer::Base
  helper :application
  

  def signup_notification(user)
    setup_email(user)
    @subject    += I18n.translate('please_activate')
    @body[:url]  = "http://#{@locale_subdomain}#{SITE_URL}/activate/#{user.activation_code}"
  end
  
  def activation(user)
    setup_email(user)
    @subject    += I18n.translate('account_activated')
    @body[:url]  = "http://#{@locale_subdomain}#{SITE_URL}/"
  end
  
  def message_notification(message)
    setup_email(message.user)
    #@recipients  = "needto@gmail.com"
    @body[:message] = message
    @body[:reply_url]  = "http://#{SITE_URL}/user/#{message.user.id}/messages/new?to=#{message.sender.id}"
    @body[:edit_profile_url]  = "http://#{SITE_URL}/user/#{message.user.id}/edit_profile"
    @subject += "Новое сообщение от: #{message.sender.displayname}"
    RAILS_DEFAULT_LOGGER.error('sent notification to ' + @recipients)
  end

  def user_vote_notification(user, voter)
    content_type "text/html"
    @body[:user] = user
    setup_email(user)

    #@recipients  = "needto@gmail.com"
    @body[:voter] = voter
    @body[:edit_profile_url]  = "http://#{SITE_URL}/user/#{user.id}/edit_profile"
    @subject += "За вас проголосовали"
    RAILS_DEFAULT_LOGGER.error('sent notification to ' + @recipients)
  end

  def forgot_password(user)
    setup_email(user)
    @subject += 'Вы запросили сброс пароля / You have requested to change your password.'
    @body[:url] = "http://#{@locale_subdomain}#{SITE_URL}/reset_password/#{user.password_reset_code}"
  end
  
  def blog_comment_notification(commentable, comment)
    setup_email(commentable.user)
    content_type "text/html"
    @subject += "Комментарий в Вашем блоге \"#{commentable.title}\""
    @body[:commentable] = commentable
    @body[:comment] = comment
    @body[:user] = commentable.user
    @body[:url] = "http://#{SITE_URL}/user/#{commentable.user.id}/blogs/#{commentable.id}#comment#{comment.id}"
    @body[:edit_profile_url]  = "http://#{SITE_URL}/user/#{commentable.user.id}/edit_profile"
  end
  
  def gallery_photo_comment_notification(commentable, comment)
    setup_email(commentable.user)
    content_type "text/html"
    @subject += "Комментарий к Вашей фотографии"
    @body[:commentable] = commentable
    @body[:comment] = comment
    @body[:user] = commentable.user
    #@body[:url] = "http://#{SITE_URL}/user/#{commentable.user.id}/gallery/view_album/#{commentable.user_album_id}/#{commentable.id}#comment#{comment.id}"
    @body[:edit_profile_url]  = "http://#{SITE_URL}/user/#{commentable.user.id}/edit_profile"
  end

  def bio_comment_notification(commentable, comment)
    setup_email(commentable.user)
    content_type "text/html"
    @subject += "Комментарий к Вашему BIO"
    @body[:commentable] = commentable
    @body[:comment] = comment
    @body[:user] = commentable.user
    #@body[:url] = "http://#{SITE_URL}/facegallery/#{commentable.user.id}/gallery/view_album/#{commentable.user_album_id}/#{commentable.id}#comment#{comment.id}"
    @body[:edit_profile_url]  = "http://#{SITE_URL}/user/#{commentable.user.id}/edit_profile"
  end

  def comment_comment_notification(commentable, comment, parent_comment)
    setup_email(parent_comment.user)
    content_type "text/html"
    @subject += "Ответ на Ваш комментарий"
    @body[:commentable] = commentable
    @body[:comment] = comment
    @body[:user] = parent_comment.user
    @body[:edit_profile_url]  = "http://#{SITE_URL}/user/#{parent_comment.user.id}/edit_profile"
  end

  def site_invite(invite)
      @from        = "info@facegallery.ru"
      @subject     = "FaceGallery.ru - Приглашение на сайт"
      @sent_on     = Time.now
      @recipients  = "#{invite.email}"
      content_type "text/html"
      @body[:user] = invite.user
      @body[:url] = "http://#{SITE_URL}/signup/#{invite.code}"
  end

  def concierge_notification(question)
      @from        = "info@facegallery.ru"
      @subject     = "FaceGallery.ru - Вопрос к Apple Concierge"
      @sent_on     = Time.now
      @recipients  = "kirill@apple-concierge.com"
      content_type "text/html"
      @body[:question] = question
  end

  def weekly_digest(user)
    setup_email(user)
    #content_type "text/html"
content_type  "multipart/mixed"
      part "text/html" do |p|
        p.body = render_message 'weekly_digest', :edit_profile_url => "http://#{SITE_URL}/user/#{user.id}/edit_profile"
      end
    @subject += "FaceGallery Digest 25012012"
    #@subject += "FG Event. День открытых дверей в Клубе ораторов"
    #@body[:edit_profile_url]  = "http://#{SITE_URL}/user/#{user.id}/edit_profile"
  end

  def event(user)
    setup_email(user)
    #content_type "text/html"
content_type  "multipart/mixed"
      part "text/html" do |p|
        p.body = render_message 'event', :edit_profile_url => "http://#{SITE_URL}/user/#{user.id}/edit_profile"
      end
    @subject = "Выходные с Face Gallery"
    #@body[:edit_profile_url]  = "http://#{SITE_URL}/user/#{user.id}/edit_profile"
  end

  def kupivip(email_address)
    @recipients  = email_address
    @from        = "info@facegallery.ru"
    @subject     = "FaceGallery: KupiLuxe.ru - Private Luxury"
    @sent_on     = Time.now
    content_type "text/html"

  end

  protected
    def setup_email(user)
      @recipients  = "#{user.email}"
      @from        = "info@facegallery.ru"
      @subject     = "FaceGallery.ru - "
      @sent_on     = Time.now
      @body[:user] = user
      @locale_subdomain = I18n.locale.to_s == 'en' ? 'en.' : ''
    end
end
