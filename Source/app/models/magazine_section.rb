class MagazineSection < ActiveRecord::Base
  has_many :magazine_articles
  
  default_scope :order => 'position'
  localizes :articles_in_flash, :cols_in_flash
  named_scope :active, :conditions => { :active => true}
  
  
  def to_param
    "#{urlname}"
  end
  
  def articles_in_flash
    read_attribute(:articles_in_flash) || 0
  end
  
  def cols_in_flash
    read_attribute(:cols_in_flash) || 0
  end
  
  def picwidth
    read_attribute(:picwidth) || 0
  end
  
  def picheight
    read_attribute(:picheight) || 0
  end
  
end
