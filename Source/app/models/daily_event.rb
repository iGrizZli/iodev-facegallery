class DailyEvent < ActiveRecord::Base
  belongs_to :place
  belongs_to :event_category
  
  has_and_belongs_to_many :users
  
  default_scope :order => 'date ASC'
  named_scope :upcoming,  lambda { { :conditions => ["daily_events.date >= ?", Date.today] } }
  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  named_scope :fgc, :conditions => { :fgc => true }
  named_scope :johnywalker, :conditions => { :johnywalker => true }

  has_attached_file :picture, :styles => { :thumbnail => ["130x80^", :jpg], :small => ["195x120^", :jpg] }, :convert_options => { :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity north -extent 130x80", :small => "-unsharp 0.3x0.3+5+0 -gravity north -extent 195x120" },
                                    :url => "/images/daily_events/:id/:style.:extension",
                                    :path => ":rails_root/public/images/daily_events/:id/:style.:extension"
  
  def self.events_today(limit=5)
    DailyEvent.find(:all, :conditions => ["date LIKE ? AND date > ?", "#{Date.today}%", 1.minute.ago], :limit => limit)
  end
end
