class Post < ActiveRecord::Base
  belongs_to :post_section
  belongs_to :user

  validates_presence_of     :title
  validates_length_of       :title,    :within => 1..255
  validates_presence_of     :text  
  
  default_scope :order => 'created_at DESC'
  named_scope :random, :order => 'rand()'  
  
  acts_as_taggable
  acts_as_commentable

  define_index do
    indexes title
    indexes text
    indexes [user.name, user.surname], :as => :user_name
    set_property :field_weights => {
      :title => 10,
      :user_name    => 6,
      :text    => 5,
    }
  end

  def to_param
    "#{id}-#{title.parameterize}"
  end
end
