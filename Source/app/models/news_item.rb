class NewsItem < ActiveRecord::Base
  localizes :title, :text
  
  default_scope :order => 'date DESC'

  belongs_to :place

  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  named_scope :fgc, :conditions => { :fgc => true }
    
  has_attached_file :image, :styles => { :original => ["500x250^", :jpg], :thumbnail => ["230x115^", :jpg], :mainpage => ["110x60^", :jpg] }, :convert_options => { :all => "-unsharp 0.3x0.3+5+0", :original => "-gravity north -extent 500x250", :thumbnail => "-gravity north -extent 230x115", :mainpage => "-gravity north -extent 110x60" },
                                    :url => "/images/news_items/:id/:style.:extension",
                                    :path => ":rails_root/public/images/news_items/:id/:style.:extension"
  
end
