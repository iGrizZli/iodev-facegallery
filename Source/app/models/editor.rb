class Editor < ActiveRecord::Base
  belongs_to :user
  has_attached_file :photo, :styles => { :original => ["180x270>", :jpg], :mainpage => ["110x60^", :jpg] }, :convert_options => { :all => "-unsharp 0.3x0.3+5+0", :mainpage => "-gravity north -extent 110x60" },
                            :url => "/images/editors/:id/:style.:extension",
                            :path => ":rails_root/public/images/editors/:id/:style.:extension"
  named_scope :active, :conditions => { :active => true }
  def latest_blog
    if user
      user.blogs.find(:first)
    end
  end
end
