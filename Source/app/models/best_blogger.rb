class BestBlogger < ActiveRecord::Base
  belongs_to :user
  has_attached_file :photo, :styles => { :original => ["115x150>", :jpg], :small => ["65x55^", :jpg] }, :convert_options => { :all => "-unsharp 0.3x0.3+5+0", :small => "-gravity north -extent 65x55" },
                            :url => "/images/best_bloggers/:id/:style.:extension",
                            :path => ":rails_root/public/images/best_bloggers/:id/:style.:extension"
  named_scope :active, :conditions => { :active => true }
  def latest_blog
    if user
      user.blogs.find(:first)
    end
  end
end
