class ArticlePageImage < ActiveRecord::Base
  belongs_to :magazine_article
  default_scope :order => '`article_page_images`.position'
  has_attached_file :image, :styles => { :original => ["1240x1060>", :jpg], :small => ["440x550>", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 ", :small => "-unsharp 0.3x0.3+2+0.05" },
                            :url => "/images/magazine/articles/:magazine_article_id/pages/:id_:style.:extension",
                            :path => ":rails_root/public/images/magazine/articles/:magazine_article_id/pages/:id_:style.:extension"
  validates_attachment_presence :image
  
  has_attached_file :spread_image, :styles => { :original => ["1600x1060>", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 " },
                            :url => "/images/magazine/articles/:magazine_article_id/pages/spread_:id_:style.:extension",
                            :path => ":rails_root/public/images/magazine/articles/:magazine_article_id/pages/spread_:id_:style.:extension"
end
