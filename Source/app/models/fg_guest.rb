class FgGuest < ActiveRecord::Base
  belongs_to :blog
  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  has_attached_file :photo, :styles => { :original => ["180x400>", :jpg] },
                                    :url => "/images/fg_guests/:id/:style.:extension",
                                    :path => ":rails_root/public/images/fg_guests/:id/:style.:extension"
end
