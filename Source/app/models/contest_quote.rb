class ContestQuote < ActiveRecord::Base
  belongs_to :bio
  has_many :contest_quote_votes
end
