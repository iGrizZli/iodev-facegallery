class MagazineArticle < ActiveRecord::Base
  belongs_to :magazine_section
  belongs_to :magazine_issue
  acts_as_commentable
  acts_as_taggable
  
  belongs_to :author, :class_name => 'User', :foreign_key => 'author_id'
  has_many :article_images
  has_many :article_page_images
  
  localizes :title, :text
  default_scope :order => 'created_at DESC'  
  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  
  named_scope :current_issue, lambda { { :conditions => { :magazine_issue_id => (I18n.locale.to_s == "ru" ? MAGAZINE_ISSUE : MAGAZINE_ISSUE_EN) } } }
  named_scope :past_issues, lambda { { :conditions => [ "magazine_issue_id != ?", (I18n.locale.to_s == "ru" ? MAGAZINE_ISSUE : MAGAZINE_ISSUE_EN) ] } }
  
  has_attached_file :preview, :styles => { :original => ["300x250>", :jpg], :thumbnail => ["55x55^", :jpg], :mainpage => ["110x60^", :jpg] }, :convert_options => { :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity center -extent 55x55",  :mainpage => "-unsharp 0.3x0.3+5+0 -gravity center -extent 110x60" },
                            :url => "/images/magazine/articles/:id/:style.:extension",
                            :path => ":rails_root/public/images/magazine/articles/:id/:style.:extension"
  has_attached_file :news_preview, :styles => { :original => ["230x115^", :jpg] }, :convert_options => { :all => "-unsharp 0.3x0.3+5+0", :original => "-gravity north -extent 230x115" },
                            :url => "/images/magazine/articles/:id/:style_news.:extension",
                            :path => ":rails_root/public/images/magazine/articles/:id/:style_news.:extension"
  has_attached_file :background, :styles => { :original => ["550x2000>", :jpg] },
                            :url => "/images/magazine/articles/:id/:style_bg.:extension",
                            :path => ":rails_root/public/images/magazine/articles/:id/:style_bg.:extension"
                            
  define_index do
    indexes :title
    indexes text
    where "published_ru = '1'"
    
    set_property :field_weights => {
      :title => 10,
      :text    => 6,
    }
  end

  def to_param
    "#{id}-#{title.parameterize}"
  end
  
  def pages_in_article
    text.scan(/\[page\s*?(\d+)\s*?\]/m).size  
  end
    
  def text_page(page_number)
    if pages_in_article > 0
      page_number = page_number || 1
      res = ""
      text.scan(/\[page\s*?(\d+)\s*?\](.+?)\[\/page\]/m){|page, text| res = text if page.to_i == page_number.to_i}
      res
    else
      text
    end
  end
  
  def name
    nil
  end

end
