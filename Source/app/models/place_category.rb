class PlaceCategory < ActiveRecord::Base
  has_many :places
  
  localizes :name
  default_scope :order => 'position'
  
  def to_param
    "#{urlname}"
  end
end
