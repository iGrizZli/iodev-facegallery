class StreamPost < ActiveRecord::Base
  belongs_to :stream
  belongs_to :stream_category

  belongs_to :user

  has_many :sections, :class_name => 'StreamPostSection', :uniq => true, :order => "position", :dependent => :destroy,  :after_add => :set_default_section_position, :after_remove => :recount_sections_positions

  has_attached_file :cover, :styles => {
                        :original => ["490x>", :jpg],
                        :index_big => ["300x170!", :jpg],
                        :index_small => ["110x90!", :jpg],
                        :lenta => ["190x130!", :jpg],
                        :thumb => ["50x50!", :jpg],
                        :convert_options => {
                            :original => "-strip -quality 100 -unsharp 0.3x0.3+4+0",
                            :index_big => "-gravity north -extent 300x170 -strip -quality 100 -unsharp 0.3x0.3+4+0",
                            :index_small => "-gravity north -extent 110x90 -strip -quality 100 -unsharp 0.3x0.3+4+0",
                            :lenta => "-gravity north -extent 190x130 -strip -quality 100 -unsharp 0.3x0.3+4+0",
                            :thumb => "-gravity north -extent 50x50 -strip -quality 100 -unsharp 0.3x0.3+4+0"
                        }},
                    :url => "/stream_i/:class/:id/:style.:extension",
                    :path => ":rails_root/public/stream_i/:class/:id/:style.:extension"

  def set_default_section_position (section)
    section.position= sections.size
  end

  def recount_sections_positions (section)
    sections.where('position > :position', :position => section.position).each{|sec| sec.position= sec.position-1 }
  end

end