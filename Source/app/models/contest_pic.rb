class ContestPic < ActiveRecord::Base
  belongs_to :user
  acts_as_list
  has_many :contest_pic_votes
  
  has_attached_file :pic, :styles => { :original => ["1024x1024>", :jpg], :smaller => ["450x450>", :jpg], :small => ["120x150>", :jpg], :thumbnail => ["75x75^", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 ", :small => "-unsharp 0.3x0.3+5+0", :thumbnail => "-unsharp 0.3x0.3+5+0 -gravity north -extent 75x75" },
                                :url => "/images/contests/bio_logos/:id.:style.:extension",
                                :path => ":rails_root/public/images/contests/bio_logos/:id.:style.:extension"
  validates_attachment_presence :pic
end
