class MessageObserver < ActiveRecord::Observer
  def before_create(message)
      unless message.user.is_online? or message.user.no_message_notifications == true
        UserMailer.deliver_message_notification(message)
        message.notify_by_email = false
      end
  end
end
