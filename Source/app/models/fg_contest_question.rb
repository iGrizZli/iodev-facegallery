class FgContestQuestion < ActiveRecord::Base
  acts_as_commentable
  
  belongs_to :author, :class_name => 'Bio', :foreign_key => 'author_id'
  has_many :fg_contest_winners
  
  default_scope :order => 'created_at DESC'  
  named_scope :published, lambda { { :conditions => {"published_#{I18n.locale}".to_sym => true} } }
  #belongs_to :winner_comment, lambda { { :class_name => 'Comment', :conditions => ["commentable_type='FgContestQuestion', commentable_id = ?", self.id] } }
  belongs_to :winner_comment,  :class_name => 'Comment'#, :conditions => {:commentable_type=>'FgContestQuestion'}
  
  localizes :text
end
