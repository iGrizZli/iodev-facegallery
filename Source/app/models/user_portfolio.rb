class UserPortfolio < ActiveRecord::Base
  belongs_to :user
  has_many :user_portfolio_photos, :order => "position"

end
