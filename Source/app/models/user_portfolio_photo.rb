class UserPortfolioPhoto < ActiveRecord::Base
  belongs_to :user_portfolio
  belongs_to :user
  
  has_attached_file :photo, :styles => { :original => ["1024x1024>", :jpg], :small => ["120x150>", :jpg], :thumbnail => ["80x80^", :jpg] }, :convert_options => { :all => "-limit memory 16000000 -limit map 160000000 ", :small => "-unsharp 0.3x0.3+5+0", :thumbnail => "-gravity north -extent 80x80" },
                            :url => "/images/users/portfolios/:user_id/:id_:style.:extension",
                            :path => ":rails_root/public/images/users/portfolios/:user_id/:id_:style.:extension"
  validates_attachment_presence :photo
  default_scope :order => 'position'
end
