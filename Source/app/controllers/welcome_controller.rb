class WelcomeController < ApplicationController
  def index

    #bios1_size = 14
    #bios2_size = 16
    #bios3_size = 26
    ##@birthday_bios = Bio.find_all_by_birthday(Date.today)
    #@birthday_bios = Bio.find(:all, :conditions => ["MONTH(birthday) = ? AND DAY(birthday) = ?", Date.today.month, Date.today.day])
    #selected_ids = @birthday_bios.collect(&:id)
    #bios1_size -= selected_ids.length
    #cond = selected_ids.length > 0 ? ["bios.id NOT IN(?)", selected_ids] : nil
    #@bios1 = Bio.published.random.facecontrolled.find(:all, :limit => bios1_size, :include => :bio_category, :conditions => cond)
    #selected_ids = selected_ids + @bios1.collect(&:id)
    #cond = selected_ids.length > 0 ? ["bios.id NOT IN(?)", selected_ids] : nil
    #@bios2 = Bio.published.random.facecontrolled.find(:all, :limit => bios2_size, :include => :bio_category, :conditions => cond)
    #selected_ids = selected_ids + @bios2.collect(&:id)
    #cond = selected_ids.length > 0 ? ["bios.id NOT IN(?)", selected_ids] : nil
    #@bios3 = Bio.published.random.facecontrolled.find(:all, :limit => bios3_size, :include => :bio_category, :conditions => cond)
    #
    #if @bios1.length > 3
    #  @bios0 = @bios1[-2,2]
    #  #RAILS_DEFAULT_LOGGER.error("0: " + @bios0.to_s)
    #  @bios1 -= @bios0
    #  #RAILS_DEFAULT_LOGGER.error("1: " + @bios1.to_s)
    #else
    #  @bios0 = []
    #end

    @bio = Bio.published.random.facecontrolled.find(:first, :include => [:bio_category, :author, :comments])
    @posts = Post.find(:all, :limit => 3, :include => [:post_section, :user], :order => 'created_at DESC')
    @tribune = MagazineArticle.published.find(:all, :limit => 4, :order => 'created_at DESC', :include => [:author, :magazine_section])
    @competition = Post.find(:first, :offset => 10, :include => [:post_section, :user], :order => 'created_at DESC')
    
    @top_blogs = Blog.find_most_commented(3,0,1)
    @news = NewsItem.published.find :all, :limit => 5
    @events = DailyEvent.upcoming.fgc.published.find :all, :limit => 5
    @reports = Report.published.find :all, :limit => 2
    @magazine_toparticle = MagazineArticle.published.find(:first)
    @magazine_interview = MagazineInterview.find(:first)
    if @magazine_toparticle
      @magazine_articles = MagazineArticle.published.find :all, :limit => 3, :conditions => ["magazine_articles.id != ?", @magazine_toparticle.id]
      @more_magazine_articles = MagazineArticle.published.find :all, :limit => 7, :conditions => ["magazine_articles.id != ? AND magazine_articles.id NOT IN(?)", @magazine_toparticle.id, @magazine_articles.collect(&:id)]
    end

    @barentz_articles = BarentzArticle.published.all :limit => 2
    
    @editors_blog = Blog.find_latest_editor_blog
    @best_blogger = BestBlogger.active.find(:first, :order => 'RAND()')
  end
end

