# This controller handles the login/logout function of the site.  
class SessionsController < ApplicationController

  # render new.rhtml
  def new
  end

  def create
    logout_keeping_session!
    user = User.authenticate(params[:email], params[:password])
    if user
      # Protects against session fixation attacks, causes request forgery
      # protection if user resubmits an earlier form using back
      # button. Uncomment if you understand the tradeoffs.
      # reset_session
      self.current_user = user
      user.user_agent = request.env["HTTP_USER_AGENT"]
      user.save
      new_cookie_flag = (params[:remember_me] == "1")
      handle_remember_cookie! new_cookie_flag
      #redirect_back_or_default('/')
      response = { :refresh => true }
      #flash[:notice] = "Logged in successfully"
    else
      note_failed_signin
      @email       = params[:email]
      @remember_me = params[:remember_me]
      response = { :errors => ["Авторизоваться не получилось. Возможно ошибка в email или пароле."] }
    end
    render :text => response.to_json
  end

  def destroy
    logout_killing_session!
    #flash[:notice] = "You have been logged out."
    redirect_back_or_default('/')
  end

  def forgot_password
    if User.find_for_forget(params[:email])
      response = { :notices => ["Ссылка для сброса пароля отправлена на Ваш email."] }
    else
      response = { :notices => ["Сбросить пароль не удалось, возможно пользователь с таким email не найден."] }
    end
    render :text => response.to_json
  end

  def reset_password
    if params[:id].nil?
      flash[:error] = "Не найден код для сброса пароля."
      redirect_to root_path
    end
  end

  def change_password
    User.find_and_reset_password(params[:password], params[:password_confirmation],
                                 params[:reset_code]) do |error, message, path, failure|
      if path
        flash[error] = message
        redirect_to send(path)
      else
        flash.now[error] = message
        render :action => 'reset_password', :id => params[:id]
      end
    end
  end
protected
  # Track failed login attempts
  def note_failed_signin
    #flash[:error] = "Couldn't log you in as '#{params[:email]}'"
    logger.warn "Failed login for '#{params[:email]}' from #{request.remote_ip} at #{Time.now.utc}"
  end
end
