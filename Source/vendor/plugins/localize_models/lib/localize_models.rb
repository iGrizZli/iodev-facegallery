# LocalizeModels
class ActiveRecord::Base

  class << self

    #Localize model attributes
    #Inspired by Xavier Defrang (http://defrang.com/articles/2005/12/02/playing-with-rails-i18n)
    def localizes(*attribute_names)

      # parse out options hash
      options = attribute_names.pop if attribute_names.last.kind_of? Hash
      options ||= {}

      attribute_names_string = "[" + attribute_names.map {|attribute_name| ":#{attribute_name}"}.join(", ") + "]"
      class_eval %{
        @@localized_attributes = #{attribute_names_string}

        def self.localized_attributes
          @@localized_attributes
        end
      }

      attribute_names.each do |attribute_name|
        class_eval %{

          #Accessor that proxies to the right accessor for the current locale
          def #{attribute_name}
            unless I18n.locale == I18n.default_locale
              localized_method = "#{attribute_name}_\#{I18n.locale}"
              value = send(localized_method.to_sym) if respond_to?(localized_method.to_sym)
              return value ? value : read_attribute(:#{attribute_name})
            end
            read_attribute(:#{attribute_name})
          end

          #Accessor before typecasting that proxies to the right accessor for the current locale
          def #{attribute_name}_before_type_cast
            unless I18n.locale == I18n.default_locale
              localized_method = "#{attribute_name}_\#{I18n.locale}_before_type_cast"
              value = send(localized_method.to_sym) if respond_to?(localized_method.to_sym)
              return value ? value : read_attribute_before_type_cast('#{attribute_name}')
            end
            read_attribute_before_type_cast('#{attribute_name}')
          end

          #Write to appropriate localized attribute
          def #{attribute_name}=(value)
            unless I18n.locale == I18n.default_locale
              localized_method = "#{attribute_name}_\#{I18n.locale}"
              write_attribute(localized_method.to_sym, value) if respond_to?(localized_method.to_sym)
            else
              write_attribute(:#{attribute_name}, value)
            end
          end

          #Read base language attribute directly
          def _#{attribute_name}
            read_attribute(:#{attribute_name})
          end

          #Read base language attribute directly without typecasting
          def _#{attribute_name}_before_type_cast
            read_attribute_before_type_cast('#{attribute_name}')
          end

          #Write base language attribute directly
          def _#{attribute_name}=(value)
            write_attribute(:#{attribute_name}, value)
          end
        }
      end

      #Returns the localized_name of the supplied attribute for the
      #current locale
      #Useful when you have to build up sql by hand or for AR::Base::find conditions
      def localized_attribute_name(attribute_name)
        unless I18n.locale == I18n.default_locale
          "#{attribute_name}_#{I18n.locale}"
        else
          attribute_name.to_s
        end
      end

        # Enables dynamic finders like find_by_user_name(user_name) and find_by_user_name_and_password(user_name, password) that are turned into
        # find(:first, :conditions => ["user_name = ?", user_name]) and  find(:first, :conditions => ["user_name = ? AND password = ?", user_name, password])
        # respectively. Also works for find(:all), but using find_all_by_amount(50) that are turned into find(:all, :conditions => ["amount = ?", 50]).
        #
        # It's even possible to use all the additional parameters to find. For example, the full interface for find_all_by_amount
        # is actually find_all_by_amount(amount, options).

    end

    alias_method :localises, :localizes
  end

end