unless defined?(Globalize) || 1
    raise <<-ERROR
The 'globalize' plugin could not be loaded. You can install Globalize by typing
the following at the rails root directory:
"svn export svn://svn.globalize-rails.org/globalize/branches/for-1.1 vendor/plugins/globalize".
ERROR
else
  require 'localize_models'
end