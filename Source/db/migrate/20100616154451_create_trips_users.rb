class CreateTripsUsers < ActiveRecord::Migration
  def self.up
    create_table :trips_users, :id => false do |t|
      t.column :trip_id, :integer, :null => false
      t.column :user_id, :integer, :null => false
	end
	add_index :trips_users, [:trip_id, :user_id], :unique => true
  end

  def self.down
    drop_table :trips_users
  end
end
