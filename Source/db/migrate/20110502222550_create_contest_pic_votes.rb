class CreateContestPicVotes < ActiveRecord::Migration
  def self.up
    create_table :contest_pic_votes do |t|
      t.integer :contest_pic_id
      t.string :ip
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :contest_pic_votes
  end
end
