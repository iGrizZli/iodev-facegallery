class CreateGivenchyStatuses < ActiveRecord::Migration
  def self.up
    create_table :givenchy_statuses do |t|
      t.integer :user_id
      t.string :text

      t.timestamps
    end
  end

  def self.down
    drop_table :givenchy_statuses
  end
end
