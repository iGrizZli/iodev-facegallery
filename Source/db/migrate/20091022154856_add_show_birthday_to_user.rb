class AddShowBirthdayToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :show_birthday, :integer
  end

  def self.down
    remove_column :users, :show_birthday
  end
end
