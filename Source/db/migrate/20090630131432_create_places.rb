class CreatePlaces < ActiveRecord::Migration
  def self.up
    create_table :places do |t|
      t.string :name, :null => false
      t.text :description
      t.string :address
      t.string :phone
      t.string :webpage
      t.boolean :fgc

      t.timestamps
    end
  end

  def self.down
    drop_table :places
  end
end
