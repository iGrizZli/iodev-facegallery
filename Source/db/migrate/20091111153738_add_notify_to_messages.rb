class AddNotifyToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :notify_by_email, :boolean, :default => 0
  end

  def self.down
    remove_column :messages, :notify_by_email
  end
end
