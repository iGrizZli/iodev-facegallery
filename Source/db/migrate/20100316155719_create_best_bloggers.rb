class CreateBestBloggers < ActiveRecord::Migration
  def self.up
    create_table :best_bloggers do |t|
      t.integer :user_id
      t.boolean :active
      t.integer :position

      t.timestamps
    end
  end

  def self.down
    drop_table :best_bloggers
  end
end
