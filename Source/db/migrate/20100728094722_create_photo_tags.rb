class CreatePhotoTags < ActiveRecord::Migration
  def self.up
    create_table :photo_tags do |t|
      t.integer :x
      t.integer :y
      t.integer :w
      t.integer :h
      t.float :x_rel
      t.float :y_rel
      t.float :w_rel
      t.float :h_rel
      t.string :displayname
      t.integer :tagger_id
      t.boolean :tag_confirmed, :default => 0
      t.references :photo_taggable, :polymorphic => true
      t.references :person_taggable, :polymorphic => true


      t.timestamps
    end
  end

  def self.down
    drop_table :photo_tags
  end
end
