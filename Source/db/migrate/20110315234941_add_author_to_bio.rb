class AddAuthorToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :author_id, :integer
  end

  def self.down
    remove_column :bios, :author_id
  end
end
