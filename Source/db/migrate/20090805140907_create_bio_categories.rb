class CreateBioCategories < ActiveRecord::Migration
  def self.up
    create_table :bio_categories do |t|
      t.string :name
      t.string :urlname
      t.integer :position

      t.timestamps
    end
  end

  def self.down
    drop_table :bio_categories
  end
end
