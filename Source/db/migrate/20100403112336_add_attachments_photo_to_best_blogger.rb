class AddAttachmentsPhotoToBestBlogger < ActiveRecord::Migration
  def self.up
    add_column :best_bloggers, :photo_file_name, :string
    add_column :best_bloggers, :photo_content_type, :string
    add_column :best_bloggers, :photo_file_size, :integer
    add_column :best_bloggers, :photo_updated_at, :datetime
  end

  def self.down
    remove_column :best_bloggers, :photo_file_name
    remove_column :best_bloggers, :photo_content_type
    remove_column :best_bloggers, :photo_file_size
    remove_column :best_bloggers, :photo_updated_at
  end
end
