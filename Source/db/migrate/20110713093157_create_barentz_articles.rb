class CreateBarentzArticles < ActiveRecord::Migration
  def self.up
    create_table :barentz_articles do |t|
      t.string :title
      t.text :text
      t.boolean :published
      t.integer :barentz_category_id

      t.timestamps
    end
  end

  def self.down
    drop_table :barentz_articles
  end
end
