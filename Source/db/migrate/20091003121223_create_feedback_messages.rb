class CreateFeedbackMessages < ActiveRecord::Migration
  def self.up
    create_table :feedback_messages do |t|
      t.string :subject
      t.string :contacts
      t.text :text
      t.string :ip
      t.string :browser

      t.timestamps
    end
  end

  def self.down
    drop_table :feedback_messages
  end
end
