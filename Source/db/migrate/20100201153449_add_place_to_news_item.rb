class AddPlaceToNewsItem < ActiveRecord::Migration
  def self.up
    add_column :news_items, :place_id, :integer
  end

  def self.down
    remove_column :news_items, :place_id
  end
end
