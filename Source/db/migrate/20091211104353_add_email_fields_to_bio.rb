class AddEmailFieldsToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :invite_hash, :string
    add_column :bios, :sent_invite, :boolean
  end

  def self.down
    remove_column :bios, :sent_invite
    remove_column :bios, :invite_hash
  end
end
