class CreateEmails < ActiveRecord::Migration
  def self.up
    create_table :emails do |t|
      t.string :from, :to
      t.integer :priority, :default => 100
      t.integer :last_send_attempt, :default => 0
      t.text :mail
      t.timestamps
    end
    add_index :emails, [:priority, :id]
  end

  def self.down
    drop_table :emails
  end
end
