class CreatePlaceCategories < ActiveRecord::Migration
  def self.up
    create_table :place_categories do |t|
      t.string :name, :null => false
      t.integer :position
      t.timestamps
    end
  end

  def self.down
    drop_table :place_categories
  end
end
