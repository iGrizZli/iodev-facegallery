class AddFgcToEventsAndReports < ActiveRecord::Migration
  def self.up
    add_column :events, :fgc, :boolean
    add_column :reports, :fgc, :boolean
  end

  def self.down
    remove_column :reports, :fgc
    remove_column :events, :fgc
  end
end
