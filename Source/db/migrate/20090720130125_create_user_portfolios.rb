class CreateUserPortfolios < ActiveRecord::Migration
  def self.up
    create_table :user_portfolios do |t|
      t.integer :user_id
      t.text :text

      t.timestamps
    end
  end

  def self.down
    drop_table :user_portfolios
  end
end
