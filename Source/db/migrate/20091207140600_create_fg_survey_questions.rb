class CreateFgSurveyQuestions < ActiveRecord::Migration
  def self.up
    create_table :fg_survey_questions do |t|
      t.integer :survey_id
      t.integer :position
      t.text :question
      t.text :options
      t.boolean :force_text_input
      t.timestamps
    end
  end

  def self.down
    drop_table :fg_survey_questions
  end
end
