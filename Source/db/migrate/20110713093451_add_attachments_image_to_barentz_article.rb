class AddAttachmentsImageToBarentzArticle < ActiveRecord::Migration
  def self.up
    add_column :barentz_articles, :image_file_name, :string
    add_column :barentz_articles, :image_content_type, :string
    add_column :barentz_articles, :image_file_size, :integer
    add_column :barentz_articles, :image_updated_at, :datetime
  end

  def self.down
    remove_column :barentz_articles, :image_file_name
    remove_column :barentz_articles, :image_content_type
    remove_column :barentz_articles, :image_file_size
    remove_column :barentz_articles, :image_updated_at
  end
end
