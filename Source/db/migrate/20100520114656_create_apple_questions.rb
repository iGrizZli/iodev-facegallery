class CreateAppleQuestions < ActiveRecord::Migration
  def self.up
    create_table :apple_questions do |t|
      t.integer :user_id
      t.text :question
      t.text :answer
      t.boolean :visible, :default => true

      t.timestamps
    end
  end

  def self.down
    drop_table :apple_questions
  end
end
