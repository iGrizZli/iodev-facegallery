class CreateNoDiscounts < ActiveRecord::Migration
  def self.up
    create_table :no_discounts do |t|
      t.integer :bio_id
      t.text :place_name
      t.text :comments
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :no_discounts
  end
end
