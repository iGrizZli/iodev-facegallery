class AddFgcToDailyEvent < ActiveRecord::Migration
  def self.up
    add_column :daily_events, :fgc, :boolean
  end

  def self.down
    remove_column :daily_events, :fgc
  end
end
