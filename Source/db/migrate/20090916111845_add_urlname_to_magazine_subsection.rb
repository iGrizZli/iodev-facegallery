class AddUrlnameToMagazineSubsection < ActiveRecord::Migration
  def self.up
    add_column :magazine_subsections, :urlname, :string
  end

  def self.down
    remove_column :magazine_subsections, :urlname
  end
end
