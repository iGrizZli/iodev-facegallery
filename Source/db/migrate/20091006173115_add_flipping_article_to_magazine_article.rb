class AddFlippingArticleToMagazineArticle < ActiveRecord::Migration
  def self.up
    add_column :magazine_articles, :flipping_article, :boolean
  end

  def self.down
    remove_column :magazine_articles, :flipping_article
  end
end
