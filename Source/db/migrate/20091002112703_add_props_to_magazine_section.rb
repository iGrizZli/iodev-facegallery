class AddPropsToMagazineSection < ActiveRecord::Migration
  def self.up
    add_column :magazine_sections, :articles_in_flash, :integer
    add_column :magazine_sections, :cols_in_flash, :integer
    add_column :magazine_sections, :picwidth, :integer
    add_column :magazine_sections, :picheight, :integer
  end

  def self.down
    remove_column :magazine_sections, :picwidth
    remove_column :magazine_sections, :picheight
    remove_column :magazine_sections, :cols_in_flash
    remove_column :magazine_sections, :articles_in_flash
  end
end
