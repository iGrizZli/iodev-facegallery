class AddLongTextToDailyEvent < ActiveRecord::Migration
  def self.up
    add_column :daily_events, :long_text, :text
  end

  def self.down
    remove_column :daily_events, :long_text
  end
end
