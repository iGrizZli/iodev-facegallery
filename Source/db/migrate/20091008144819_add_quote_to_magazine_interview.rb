class AddQuoteToMagazineInterview < ActiveRecord::Migration
  def self.up
    add_column :magazine_interviews, :quote, :text
    add_column :magazine_interviews, :quote_en, :text
  end

  def self.down
    remove_column :magazine_interviews, :quote_en
    remove_column :magazine_interviews, :quote
  end
end
