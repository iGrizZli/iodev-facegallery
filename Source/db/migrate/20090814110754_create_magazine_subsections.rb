class CreateMagazineSubsections < ActiveRecord::Migration
  def self.up
    create_table :magazine_subsections do |t|
      t.string :name
      t.integer :position
      t.integer :magazine_section_id
      t.boolean :active

      t.timestamps
    end
  end

  def self.down
    drop_table :magazine_subsections
  end
end
