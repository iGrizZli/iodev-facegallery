class ChangeCommentableTypeInComments < ActiveRecord::Migration
  def self.up
    change_column :comments, :commentable_type, :string, :limit => 255, :default => "", :null => false
  end

  def self.down
    change_column :comments, :commentable_type, :string, :limit => 15, :default => "", :null => false
  end
end
