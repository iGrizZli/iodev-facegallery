class AddBirthdayToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :birthday, :date
    add_column :bios, :gender, :string, :limit => 1
  end

  def self.down
    remove_column :bios, :gender
    remove_column :bios, :birthday
  end
end
