class CreateContestPhotos < ActiveRecord::Migration
  def self.up
    create_table :contest_photos do |t|
      t.integer :contest_session_id
      t.integer :position
      
      t.timestamps
    end
  end

  def self.down
    drop_table :contest_photos
  end
end
