class AddPublishedToEvents < ActiveRecord::Migration
  def self.up
    add_column :events, :published_ru, :boolean
    add_column :events, :published_en, :boolean
  end

  def self.down
    remove_column :events, :published_en
    remove_column :events, :published_ru
  end
end
