class AddUrlnameToPlaceCategory < ActiveRecord::Migration
  def self.up
    add_column :place_categories, :urlname, :string, :null => false
  end

  def self.down
    remove_column :place_categories, :urlname
  end
end
