class CreateContestSessions < ActiveRecord::Migration
  def self.up
    create_table :contest_sessions do |t|
      t.string :photographer_name
      t.string :model_name
      t.text :info
      t.integer :position
      t.integer :photographer_id
      t.integer :model_id
      
      t.timestamps
    end
  end

  def self.down
    drop_table :contest_sessions
  end
end
