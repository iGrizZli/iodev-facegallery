class AddNoMessageNotificationsToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :no_message_notifications, :boolean
  end

  def self.down
    remove_column :users, :no_message_notifications
  end
end
