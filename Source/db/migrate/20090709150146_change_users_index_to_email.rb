class ChangeUsersIndexToEmail < ActiveRecord::Migration
  def self.up
    add_index :users, :email, :unique => true
    remove_index :users, :login
  end

  def self.down
    add_index :users, :login, :unique => true
    remove_index :users, :email
  end
end
