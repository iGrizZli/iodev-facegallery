class AddEnglishToPlaces < ActiveRecord::Migration
  def self.up
    add_column :places, :name_en, :string
    add_column :places, :description_en, :text
    add_column :places, :address_en, :string
    add_column :places, :phone_en, :string
    add_column :places, :food_en, :string
    add_column :places, :working_hours_en, :string
    add_column :places, :parking_en, :string
    
    add_column :place_categories, :name_en, :string
  end

  def self.down
    remove_column :place_categories, :name_en
    
    remove_column :places, :parking_en
    remove_column :places, :working_hours_en
    remove_column :places, :food_en
    remove_column :places, :phone_en
    remove_column :places, :address_en
    remove_column :places, :description_en
    remove_column :places, :name_en
  end
end
