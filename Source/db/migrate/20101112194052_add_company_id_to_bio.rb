class AddCompanyIdToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :company_id, :integer
  end

  def self.down
    remove_column :bios, :company_id
  end
end
