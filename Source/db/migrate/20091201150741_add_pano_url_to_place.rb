class AddPanoUrlToPlace < ActiveRecord::Migration
  def self.up
    add_column :places, :pano_url, :string
  end

  def self.down
    remove_column :places, :pano_url
  end
end
