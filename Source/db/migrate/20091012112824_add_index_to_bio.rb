class AddIndexToBio < ActiveRecord::Migration
  def self.up
    add_index :bios, :published_ru
    add_index :bios, :published_en
  end

  def self.down
    remove_index :bios, :published_ru
    remove_index :bios, :published_en
  end
end
