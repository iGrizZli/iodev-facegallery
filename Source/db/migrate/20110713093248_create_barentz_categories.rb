class CreateBarentzCategories < ActiveRecord::Migration
  def self.up
    create_table :barentz_categories do |t|
      t.string :title
      t.string :urlname
      t.boolean :visible
      t.integer :position

      t.timestamps
    end
  end

  def self.down
    drop_table :barentz_categories
  end
end
