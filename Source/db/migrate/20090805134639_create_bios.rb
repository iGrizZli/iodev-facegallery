class CreateBios < ActiveRecord::Migration
  def self.up
    create_table :bios do |t|
      t.string :name
      t.string :surname
      t.string :occupation
      t.integer :bio_category_id
      t.text :text
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :bios
  end
end
