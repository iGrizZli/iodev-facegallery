class CreateFgGuests < ActiveRecord::Migration
  def self.up
    create_table :fg_guests do |t|
      t.string :short_text
      t.integer :blog_id
      t.boolean :published_ru
      t.boolean :published_en
      t.timestamps
    end
  end

  def self.down
    drop_table :fg_guests
  end
end
