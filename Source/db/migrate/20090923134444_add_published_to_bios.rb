class AddPublishedToBios < ActiveRecord::Migration
  def self.up
    add_column :bios, :published_ru, :boolean
    add_column :bios, :published_en, :boolean
  end

  def self.down
    remove_column :bios, :published_en
    remove_column :bios, :published_ru
  end
end
