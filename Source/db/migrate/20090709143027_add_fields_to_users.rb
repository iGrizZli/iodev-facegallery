class AddFieldsToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :city, :string
    add_column :users, :occupation, :string
    add_column :users, :education, :string
    add_column :users, :interests, :string
    add_column :users, :about, :string
    add_column :users, :birthday, :date
    add_column :users, :quote, :string
    add_column :users, :phone, :string    
  end

  def self.down
    remove_column :users, :city
    remove_column :users, :occupation
    remove_column :users, :education
    remove_column :users, :interests
    remove_column :users, :about
    remove_column :users, :birthday
    remove_column :users, :quote
    remove_column :users, :phone
  end
end
