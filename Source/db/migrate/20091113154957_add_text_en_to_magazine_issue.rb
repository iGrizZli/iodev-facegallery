class AddTextEnToMagazineIssue < ActiveRecord::Migration
  def self.up
    add_column :magazine_issues, :text_en, :text
  end

  def self.down
    remove_column :magazine_issues, :text_en
  end
end
