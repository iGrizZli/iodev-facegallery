class CreateFgContestWinners < ActiveRecord::Migration
  def self.up
    create_table :fg_contest_winners do |t|
      t.integer :fg_contest_question_id, :nil => false
      t.integer :comment_id, :nil => false

      t.timestamps
    end
    add_index :fg_contest_winners, :fg_contest_question_id
  end

  def self.down
    drop_table :fg_contest_winners
  end
end
