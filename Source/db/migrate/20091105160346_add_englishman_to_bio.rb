class AddEnglishmanToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :englishman, :boolean
  end

  def self.down
    remove_column :bios, :englishman
  end
end
