class CreateFgContestQuestions < ActiveRecord::Migration
  def self.up
    create_table :fg_contest_questions do |t|
      t.text :text
      t.text :text_en
      t.boolean :published_ru
      t.boolean :published_en
      t.integer :author_id

      t.timestamps
    end
  end

  def self.down
    drop_table :fg_contest_questions
  end
end
