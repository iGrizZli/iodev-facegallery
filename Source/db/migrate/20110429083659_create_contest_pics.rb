class CreateContestPics < ActiveRecord::Migration
  def self.up
    create_table :contest_pics do |t|
      t.integer :user_id
      t.integer :position
      
      t.timestamps
    end
  end

  def self.down
    drop_table :contest_pics
  end
end
