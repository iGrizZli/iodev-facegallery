class AddPreferencesFieldsToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :profile_access, :integer
    add_column :users, :friends_access, :integer
    add_column :users, :portfolio_access, :integer
    add_column :users, :gallery_access, :integer
    add_column :users, :blog_access, :integer
    
    remove_column :users, :preferences
  end

  def self.down
    remove_column :users, :profile_access
    remove_column :users, :friends_access
    remove_column :users, :portfolio_access
    remove_column :users, :gallery_access
    remove_column :users, :blog_access
    
    add_column :users, :preferences, :text
  end
end
