class AddNotOnMainPageToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :not_on_main_page, :boolean
  end

  def self.down
    remove_column :bios, :not_on_main_page
  end
end
