class AddAttachmentsPreviewToMagazineArticle < ActiveRecord::Migration
  def self.up
    add_column :magazine_articles, :preview_file_name, :string
    add_column :magazine_articles, :preview_content_type, :string
    add_column :magazine_articles, :preview_file_size, :integer
    add_column :magazine_articles, :preview_updated_at, :datetime
  end

  def self.down
    remove_column :magazine_articles, :preview_file_name
    remove_column :magazine_articles, :preview_content_type
    remove_column :magazine_articles, :preview_file_size
    remove_column :magazine_articles, :preview_updated_at
  end
end
