class AddBioWishedToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :bio_wished, :datetime
  end

  def self.down
    remove_column :users, :bio_wished
  end
end
