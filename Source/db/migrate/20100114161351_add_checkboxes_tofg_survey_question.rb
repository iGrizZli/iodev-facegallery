class AddCheckboxesTofgSurveyQuestion < ActiveRecord::Migration
  def self.up
    add_column :fg_survey_questions, :checkbox, :boolean
  end

  def self.down
    remove_column :fg_survey_questions, :checkbox
  end
end
