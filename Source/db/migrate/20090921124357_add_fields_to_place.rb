class AddFieldsToPlace < ActiveRecord::Migration
  def self.up
    add_column :places, :food, :string
    add_column :places, :working_hours, :string
    add_column :places, :parking, :string
  end

  def self.down
    remove_column :places, :parking
    remove_column :places, :working_hours
    remove_column :places, :food
  end
end
