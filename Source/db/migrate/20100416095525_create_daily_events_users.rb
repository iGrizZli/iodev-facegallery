class CreateDailyEventsUsers < ActiveRecord::Migration
  def self.up
    create_table :daily_events_users, :id => false do |t|
      t.column :user_id, :integer, :null => false
      t.column :daily_event_id, :integer, :null => false
		end
		add_index :daily_events_users, [:user_id, :daily_event_id], :name => "fk_user_daily_event"
  end

  def self.down
    drop_table :daily_events_users
  end
end
