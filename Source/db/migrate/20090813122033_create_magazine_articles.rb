class CreateMagazineArticles < ActiveRecord::Migration
  def self.up
    create_table :magazine_articles do |t|
      t.string :title
      t.text :text
      t.integer :author_id
      t.integer :magazine_section_id
      t.integer :magazine_subsection_id

      t.timestamps
    end
  end

  def self.down
    drop_table :magazine_articles
  end
end
