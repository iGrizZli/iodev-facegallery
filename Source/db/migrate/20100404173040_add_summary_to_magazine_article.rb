class AddSummaryToMagazineArticle < ActiveRecord::Migration
  def self.up
    add_column :magazine_articles, :summary, :text
  end

  def self.down
    remove_column :magazine_articles, :summary
  end
end
