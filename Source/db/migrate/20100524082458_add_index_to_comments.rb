class AddIndexToComments < ActiveRecord::Migration
  def self.up
    add_index :comments, :commentable_id
    add_index :comments, :commentable_type
  end

  def self.down
    remove_index :comments, :commentable_type
    remove_index :comments, :commentable_id
  end
end
