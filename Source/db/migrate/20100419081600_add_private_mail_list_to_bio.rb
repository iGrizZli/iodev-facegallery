class AddPrivateMailListToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :private_maillist, :boolean, :default => false
  end

  def self.down
    remove_column :bios, :private_maillist
  end
end
