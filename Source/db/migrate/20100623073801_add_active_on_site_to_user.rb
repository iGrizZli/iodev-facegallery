class AddActiveOnSiteToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :active_on_site, :boolean
  end

  def self.down
    remove_column :users, :active_on_site
  end
end
