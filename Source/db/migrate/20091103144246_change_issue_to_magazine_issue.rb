class ChangeIssueToMagazineIssue < ActiveRecord::Migration
  def self.up
    rename_column :magazine_articles, :issue, :magazine_issue_id
    rename_column :magazine_interviews, :issue, :magazine_issue_id
  end

  def self.down
    rename_column :magazine_articles, :magazine_issue_id, :issue
    rename_column :magazine_interviews, :magazine_issue_id, :issue
  end
end
