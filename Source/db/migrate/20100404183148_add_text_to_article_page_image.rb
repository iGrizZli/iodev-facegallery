class AddTextToArticlePageImage < ActiveRecord::Migration
  def self.up
    add_column :article_page_images, :text, :text
  end

  def self.down
    remove_column :article_page_images, :text
  end
end
