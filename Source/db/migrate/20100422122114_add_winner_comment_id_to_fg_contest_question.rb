class AddWinnerCommentIdToFgContestQuestion < ActiveRecord::Migration
  def self.up
    add_column :fg_contest_questions, :winner_comment_id, :integer
  end

  def self.down
    remove_column :fg_contest_questions, :winner_comment_id
  end
end
