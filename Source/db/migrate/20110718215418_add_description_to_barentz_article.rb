class AddDescriptionToBarentzArticle < ActiveRecord::Migration
  def self.up
    add_column :barentz_articles, :description, :string
  end

  def self.down
    remove_column :barentz_articles, :description
  end
end
