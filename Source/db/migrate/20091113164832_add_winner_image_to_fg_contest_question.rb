class AddWinnerImageToFgContestQuestion < ActiveRecord::Migration
  def self.up
    add_column :fg_contest_questions, :winner_image, :string
  end

  def self.down
    remove_column :fg_contest_questions, :winner_image
  end
end
