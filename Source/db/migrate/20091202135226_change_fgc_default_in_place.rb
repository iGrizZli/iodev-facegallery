class ChangeFgcDefaultInPlace < ActiveRecord::Migration
  def self.up
    change_column :places, :fgc, :boolean, :null => false, :default => 0
  end

  def self.down
    change_column :places, :fgc, :boolean, :null => true
  end
end
