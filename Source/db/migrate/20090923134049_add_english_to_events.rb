class AddEnglishToEvents < ActiveRecord::Migration
  def self.up
    add_column :events, :name_en, :string
    add_column :events, :description_en, :text
  end

  def self.down
    remove_column :events, :description_en
    remove_column :events, :name_en
  end
end
