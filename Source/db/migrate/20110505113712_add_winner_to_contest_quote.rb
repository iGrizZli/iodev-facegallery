class AddWinnerToContestQuote < ActiveRecord::Migration
  def self.up
    add_column :contest_quotes, :winner, :boolean, :default => false
  end

  def self.down
    remove_column :contest_quotes, :winner
  end
end
