class AddIssueToMagazineArticle < ActiveRecord::Migration
  def self.up
    add_column :magazine_articles, :issue, :integer
    add_column :magazine_interviews, :issue, :integer
  end

  def self.down
    remove_column :magazine_interviews, :issue
    remove_column :magazine_articles, :issue
  end
end
