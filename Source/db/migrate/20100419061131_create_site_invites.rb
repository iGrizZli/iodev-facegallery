class CreateSiteInvites < ActiveRecord::Migration
  def self.up
    create_table :site_invites do |t|
      t.integer :user_id
      t.string :code
      t.string :email
      t.boolean :used, :default => false
      t.integer :new_user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :site_invites
  end
end
