class AddUrlnameToMagazineSection < ActiveRecord::Migration
  def self.up
    add_column :magazine_sections, :urlname, :string
  end

  def self.down
    remove_column :magazine_sections, :urlname
  end
end
