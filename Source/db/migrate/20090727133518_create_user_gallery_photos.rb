class CreateUserGalleryPhotos < ActiveRecord::Migration
  def self.up
    create_table :user_gallery_photos do |t|
      t.string :title
      t.text :description
      t.integer :order
      t.integer :user_id
      t.integer :user_album_id

      t.timestamps
    end
  end

  def self.down
    drop_table :user_gallery_photos
  end
end
