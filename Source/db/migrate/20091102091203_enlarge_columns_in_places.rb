class EnlargeColumnsInPlaces < ActiveRecord::Migration
  def self.up
    change_column :places, :phone, :text
    change_column :places, :address, :text
    change_column :places, :phone_en, :text
    change_column :places, :address_en, :text
  end

  def self.down
    change_column :places, :phone, :string
    change_column :places, :address, :string
    change_column :places, :phone_en, :string
    change_column :places, :address_en, :string
  end
end
