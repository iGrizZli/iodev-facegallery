class CreateFgSurveyAnswers < ActiveRecord::Migration
  def self.up
    create_table :fg_survey_answers do |t|
      t.integer :user_id
      t.string :session_id
      t.text :answers

      t.timestamps
    end
  end

  def self.down
    drop_table :fg_survey_answers
  end
end
