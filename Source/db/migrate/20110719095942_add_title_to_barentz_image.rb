class AddTitleToBarentzImage < ActiveRecord::Migration
  def self.up
    add_column :barentz_images, :title, :string
  end

  def self.down
    remove_column :barentz_images, :title
  end
end
