class AddPublishedToPlace < ActiveRecord::Migration
  def self.up
    add_column :places, :published_ru, :boolean
    add_column :places, :published_en, :boolean
  end

  def self.down
    remove_column :places, :published_en
    remove_column :places, :published_ru
  end
end
