class AddEnglishToDocuments < ActiveRecord::Migration
  def self.up
    add_column :documents, :name_en, :string
    add_column :documents, :text_en, :text
  end

  def self.down
    remove_column :documents, :text_en
    remove_column :documents, :name_en
  end
end
