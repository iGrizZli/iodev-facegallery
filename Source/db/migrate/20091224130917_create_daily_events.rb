class CreateDailyEvents < ActiveRecord::Migration
  def self.up
    create_table :daily_events do |t|
      t.string :name
      t.datetime :date
      t.text :text
      t.integer :place_id
      t.boolean :highlight
      t.boolean :published_ru
      t.boolean :published_en

      t.timestamps
    end
  end

  def self.down
    drop_table :daily_events
  end
end
