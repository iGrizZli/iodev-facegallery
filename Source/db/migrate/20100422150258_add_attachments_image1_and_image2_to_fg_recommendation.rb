class AddAttachmentsImage1AndImage2ToFgRecommendation < ActiveRecord::Migration
  def self.up
    add_column :fg_recommendations, :image1_file_name, :string
    add_column :fg_recommendations, :image1_content_type, :string
    add_column :fg_recommendations, :image1_file_size, :integer
    add_column :fg_recommendations, :image1_updated_at, :datetime
    add_column :fg_recommendations, :image2_file_name, :string
    add_column :fg_recommendations, :image2_content_type, :string
    add_column :fg_recommendations, :image2_file_size, :integer
    add_column :fg_recommendations, :image2_updated_at, :datetime
  end

  def self.down
    remove_column :fg_recommendations, :image1_file_name
    remove_column :fg_recommendations, :image1_content_type
    remove_column :fg_recommendations, :image1_file_size
    remove_column :fg_recommendations, :image1_updated_at
    remove_column :fg_recommendations, :image2_file_name
    remove_column :fg_recommendations, :image2_content_type
    remove_column :fg_recommendations, :image2_file_size
    remove_column :fg_recommendations, :image2_updated_at
  end
end
