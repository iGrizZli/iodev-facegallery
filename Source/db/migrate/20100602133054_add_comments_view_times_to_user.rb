class AddCommentsViewTimesToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :comments_view_times, :text
  end

  def self.down
    remove_column :users, :comments_view_times
  end
end
