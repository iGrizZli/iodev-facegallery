class CreatePlacePhotos < ActiveRecord::Migration
  def self.up
    create_table :place_photos do |t|
      t.integer :place_id
      t.integer :position

      t.timestamps
    end
  end

  def self.down
    drop_table :place_photos
  end
end
