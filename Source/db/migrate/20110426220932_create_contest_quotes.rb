class CreateContestQuotes < ActiveRecord::Migration
  def self.up
    create_table :contest_quotes do |t|
      t.string :quote
      t.integer :bio_id

      t.timestamps
    end
  end

  def self.down
    drop_table :contest_quotes
  end
end
