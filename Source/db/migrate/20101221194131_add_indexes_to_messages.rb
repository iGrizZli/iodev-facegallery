class AddIndexesToMessages < ActiveRecord::Migration
  def self.up
    add_index :messages, :sender_id
    add_index :messages, :deleted_by_sender
  end

  def self.down
    remove_index :messages, :deleted_by_sender
    remove_index :messages, :sender_id
  end
end
