class AddJwToDailyEvent < ActiveRecord::Migration
  def self.up
    add_column :daily_events, :johnywalker, :boolean, :default => false
  end

  def self.down
    remove_column :daily_events, :johnywalker
  end
end
