class CreateFgRoomQuestions < ActiveRecord::Migration
  def self.up
    create_table :fg_room_questions do |t|
      t.integer :user_id
      t.boolean :belstaff
      t.boolean :andrew_mackenzie
      t.boolean :tom_rebel
      t.boolean :dkny
      t.boolean :ring
      t.boolean :john_richmond
      t.boolean :gaetano_navarra
      t.boolean :stone_island
      t.boolean :byblos
      t.boolean :john_varvatos
      t.boolean :golden_goose
      t.boolean :k_by_karl
      t.boolean :faith_connexion
      t.boolean :jc_de_castelbajac
      t.boolean :guliano_fujiwara
      t.boolean :bad_spirit
      t.boolean :marithe_francois_girbaud
      t.boolean :paul_joe
      t.boolean :bill_tornade
      t.boolean :blauer
      t.boolean :parasuco
      t.boolean :jan_carlos
      t.boolean :woolrich
      t.boolean :amen
      t.boolean :mauro_grifoni
      t.boolean :coast_weber_ahaus
      t.boolean :messagerie
      t.boolean :daniele_alessandrini
      t.text :other

      t.timestamps
    end
  end

  def self.down
    drop_table :fg_room_questions
  end
end
