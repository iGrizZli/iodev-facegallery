class CreateRegisterApplications < ActiveRecord::Migration
  def self.up
    create_table :register_applications do |t|
      t.string :name
      t.string :surname
      t.date :birthday
      t.string :city
      t.string :email
      t.string :phone
      t.string :education
      t.string :work_area
      t.string :company
      t.text :achievements
      t.text :interests
      t.text :why

      t.timestamps
    end
  end

  def self.down
    drop_table :register_applications
  end
end
