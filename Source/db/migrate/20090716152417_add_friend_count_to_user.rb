class AddFriendCountToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :friend_count, :integer, :default => 0
  end

  def self.down
    remove_column :users, :friend_count
  end
end
