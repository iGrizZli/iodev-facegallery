class CreateContestQuoteVotes < ActiveRecord::Migration
  def self.up
    create_table :contest_quote_votes do |t|
      t.integer :contest_quote_id
      t.string :ip
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :contest_quote_votes
  end
end
