class AddEnglishToReports < ActiveRecord::Migration
  def self.up
    add_column :reports, :name_en, :string
    add_column :reports, :description_en, :text
  end

  def self.down
    remove_column :reports, :description_en
    remove_column :reports, :name_en
  end
end
