class AddIndexesToBlog < ActiveRecord::Migration
  def self.up
    add_index :blogs, :created_at
    add_index :blogs, :user_id
  end

  def self.down
    remove_index :blogs, :created_at
    remove_index :blogs, :user_id
  end
end
