class AddDeleteflagsToMessage < ActiveRecord::Migration
  def self.up
    add_column :messages, :deleted_by_user, :boolean
    add_column :messages, :deleted_by_sender, :boolean
  end

  def self.down
    remove_column :messages, :deleted_by_sender
    remove_column :messages, :deleted_by_user
  end
end
