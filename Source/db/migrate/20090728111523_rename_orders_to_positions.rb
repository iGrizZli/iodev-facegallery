class RenameOrdersToPositions < ActiveRecord::Migration
  def self.up
    rename_column(:user_portfolio_photos, :order, :position)
    rename_column(:user_gallery_photos, :order, :position)
    rename_column(:user_albums, :order, :position)
  end

  def self.down
    rename_column(:user_portfolio_photos, :position, :order)
    rename_column(:user_gallery_photos, :position, :order)
    rename_column(:user_albums, :position, :order)
  end
end
