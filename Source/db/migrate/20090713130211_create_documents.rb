class CreateDocuments < ActiveRecord::Migration
  def self.up
    create_table :documents do |t|
      t.string :name
      t.text :text
      t.string :alias
      t.boolean :visible
      
      t.timestamps
    end
  end

  def self.down
    drop_table :documents
  end
end
