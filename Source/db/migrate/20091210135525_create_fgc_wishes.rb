class CreateFgcWishes < ActiveRecord::Migration
  def self.up
    create_table :fgc_wishes do |t|
      t.integer :user_id
      t.text :comments

      t.timestamps
    end
  end

  def self.down
    drop_table :fgc_wishes
  end
end
