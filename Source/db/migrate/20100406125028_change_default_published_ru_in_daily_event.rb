class ChangeDefaultPublishedRuInDailyEvent < ActiveRecord::Migration
  def self.up
    change_column :daily_events, :published_ru, :boolean, :default => true
  end

  def self.down
    change_column :daily_events, :published_ru, :boolean, :default => nil
  end
end
