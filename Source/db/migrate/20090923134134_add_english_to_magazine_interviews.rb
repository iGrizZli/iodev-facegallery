class AddEnglishToMagazineInterviews < ActiveRecord::Migration
  def self.up
    add_column :magazine_interviews, :name_en, :string
    add_column :magazine_interviews, :title_en, :text
    add_column :magazine_interviews, :preface_en, :text
    add_column :magazine_interviews, :text_en, :text
  end

  def self.down
    remove_column :magazine_interviews, :text_en
    remove_column :magazine_interviews, :preface_en
    remove_column :magazine_interviews, :title_en
    remove_column :magazine_interviews, :name_en
  end
end
