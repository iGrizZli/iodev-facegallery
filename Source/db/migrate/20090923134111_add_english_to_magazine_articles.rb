class AddEnglishToMagazineArticles < ActiveRecord::Migration
  def self.up
    add_column :magazine_articles, :title_en, :string
    add_column :magazine_articles, :text_en, :text
  end

  def self.down
    remove_column :magazine_articles, :text_en
    remove_column :magazine_articles, :title_en
  end
end
