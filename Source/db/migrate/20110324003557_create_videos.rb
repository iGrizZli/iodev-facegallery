class CreateVideos < ActiveRecord::Migration
  def self.up
    create_table :videos do |t|
      t.string :title
      t.text :description
      t.text :embed_code
      t.integer :user_id
      t.integer :bio_id
      t.boolean :active

      t.timestamps
    end
    add_index :videos, :active
  end

  def self.down
    drop_table :videos
  end
end
