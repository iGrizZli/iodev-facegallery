class CreateMagazineIssues < ActiveRecord::Migration
  def self.up
    create_table :magazine_issues do |t|
      t.string :title
      t.text :text

      t.timestamps
    end
  end

  def self.down
    drop_table :magazine_issues
  end
end
