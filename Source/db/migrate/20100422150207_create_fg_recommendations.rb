class CreateFgRecommendations < ActiveRecord::Migration
  def self.up
    create_table :fg_recommendations do |t|
      t.string :section1, :default => 'MAGAZINE'
      t.string :title1
      t.string :link1
      t.string :section2, :default => 'BIO'
      t.string :title2
      t.string :link2
      t.boolean :published_ru
      t.boolean :published_en

      t.timestamps
    end
  end

  def self.down
    drop_table :fg_recommendations
  end
end
