class AddQuoteToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :quote, :string
  end

  def self.down
    remove_column :bios, :quote
  end
end
