class CreateStreamsModel < ActiveRecord::Migration
  def self.up
    create_table :streams do |t|
      t.string :slug
      t.string :title
    end

    create_table :stream_categories do |t|
      t.string :slug
      t.string :title
      t.integer :stream_id
    end

    create_table :stream_posts do |t|
      t.string :slug
      t.string :title
      t.text   :preview
      t.boolean :is_draft, :default => true

      t.integer :stream_id
      t.integer :stream_category_id
      t.integer :user_id

      t.string   :cover_file_name
      t.string   :cover_content_type
      t.integer  :cover_file_size
      t.datetime :cover_updated_at


      t.datetime :published_at
      t.timestamps
    end

    create_table :stream_post_sections do |t|
      t.string  :type
      t.integer :post_id
      t.integer :position
      t.text    :text
      t.text    :source_text
      t.string  :link
    end

    create_table :stream_post_section_photos do |t|
      t.integer :position
      t.text    :title

      t.string   :photo_file_name
      t.string   :photo_content_type
      t.integer  :photo_file_size
      t.datetime :photo_updated_at
    end
  end

  def self.down
    drop_table :stream_post_section_photos
    drop_table :stream_post_sections
    drop_table :stream_posts
    drop_table :stream_categories
    drop_table :streams
  end
end
