class CreateTrips < ActiveRecord::Migration
  def self.up
    create_table :trips do |t|
      t.string :title
      t.text :description
      t.text :schedule
      t.text :costs
      t.date :start_date
      t.date :end_date
      t.string :country
      t.boolean :published_ru
      t.boolean :published_en

      t.timestamps
    end
  end

  def self.down
    drop_table :trips
  end
end
