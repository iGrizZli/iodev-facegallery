class AddPublishedToMagazineInterviews < ActiveRecord::Migration
  def self.up
    add_column :magazine_interviews, :published_ru, :boolean
    add_column :magazine_interviews, :published_en, :boolean
  end

  def self.down
    remove_column :magazine_interviews, :published_en
    remove_column :magazine_interviews, :published_ru
  end
end
