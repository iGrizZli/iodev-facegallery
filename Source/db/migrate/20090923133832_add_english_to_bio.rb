class AddEnglishToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :name_en, :string
    add_column :bios, :surname_en, :string
    add_column :bios, :occupation_en, :string
    add_column :bios, :text_en, :text
    add_column :bios, :quote_en, :string
    
    add_column :bio_categories, :name_en, :string
  end

  def self.down
    remove_column :bios, :quote_en
    remove_column :bios, :text_en
    remove_column :bios, :occupation_en
    remove_column :bios, :surname_en
    remove_column :bios, :name_en
    
    remove_column :bio_categories, :name_en
  end
end
