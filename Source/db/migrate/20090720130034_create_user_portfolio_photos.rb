class CreateUserPortfolioPhotos < ActiveRecord::Migration
  def self.up
    create_table :user_portfolio_photos do |t|
      t.integer :user_portfolio_id
      t.string :title
      t.text :description
      t.integer :order

      t.timestamps
    end
  end

  def self.down
    drop_table :user_portfolio_photos
  end
end
