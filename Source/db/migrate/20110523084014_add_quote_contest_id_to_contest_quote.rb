class AddQuoteContestIdToContestQuote < ActiveRecord::Migration
  def self.up
    add_column :contest_quotes, :quote_contest_id, :integer
  end

  def self.down
    remove_column :contest_quotes, :quote_contest_id
  end
end
