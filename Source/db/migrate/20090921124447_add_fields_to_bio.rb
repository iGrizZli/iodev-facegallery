class AddFieldsToBio < ActiveRecord::Migration
  def self.up
    add_column :bios, :email, :string
    add_column :bios, :card_id, :integer
  end

  def self.down
    remove_column :bios, :card_id
    remove_column :bios, :email
  end
end
