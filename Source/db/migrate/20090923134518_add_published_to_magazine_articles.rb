class AddPublishedToMagazineArticles < ActiveRecord::Migration
  def self.up
    add_column :magazine_articles, :published_ru, :boolean
    add_column :magazine_articles, :published_en, :boolean
  end

  def self.down
    remove_column :magazine_articles, :published_en
    remove_column :magazine_articles, :published_ru
  end
end
