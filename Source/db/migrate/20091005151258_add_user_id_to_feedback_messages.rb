class AddUserIdToFeedbackMessages < ActiveRecord::Migration
  def self.up
    add_column :feedback_messages, :user_id, :integer
  end

  def self.down
    remove_column :feedback_messages, :user_id
  end
end
