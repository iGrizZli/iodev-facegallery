class AddActiveToPostSection < ActiveRecord::Migration
  def self.up
    add_column :post_sections, :active, :boolean, :default => 1
  end

  def self.down
    remove_column :post_sections, :active
  end
end
