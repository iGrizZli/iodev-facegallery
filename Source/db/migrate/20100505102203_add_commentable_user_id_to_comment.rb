class AddCommentableUserIdToComment < ActiveRecord::Migration
  def self.up
    add_column :comments, :commentable_user_id, :integer
    add_index "comments", ["commentable_user_id"]
  end

  def self.down
    remove_index :comments, :commentable_user_id
    remove_column :comments, :commentable_user_id
  end
end
