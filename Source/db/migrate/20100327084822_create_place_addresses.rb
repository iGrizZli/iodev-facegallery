class CreatePlaceAddresses < ActiveRecord::Migration
  def self.up
    create_table :place_addresses do |t|
      t.integer :place_id
      t.text :address
      t.text :phone
      t.string :webpage
      t.string :food
      t.string :parking
      t.string :working_hours

      t.timestamps
    end
  end

  def self.down
    drop_table :place_addresses
  end
end
