class AddDisableToFgSurveyQuestion < ActiveRecord::Migration
  def self.up
    add_column :fg_survey_questions, :disable_checkbox, :string
    add_column :fg_survey_questions, :textarea, :boolean
  end

  def self.down
    remove_column :fg_survey_questions, :textarea
    remove_column :fg_survey_questions, :disable_checkbox
  end
end
