class AddUserIdToUserPortfolioPhoto < ActiveRecord::Migration
  def self.up
    add_column :user_portfolio_photos, :user_id, :integer
  end

  def self.down
    remove_column :user_portfolio_photos, :user_id
  end
end
