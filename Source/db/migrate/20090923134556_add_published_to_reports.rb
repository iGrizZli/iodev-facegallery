class AddPublishedToReports < ActiveRecord::Migration
  def self.up
    add_column :reports, :published_ru, :boolean
    add_column :reports, :published_en, :boolean
  end

  def self.down
    remove_column :reports, :published_en
    remove_column :reports, :published_ru
  end
end
