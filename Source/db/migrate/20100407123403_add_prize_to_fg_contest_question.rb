class AddPrizeToFgContestQuestion < ActiveRecord::Migration
  def self.up
    add_column :fg_contest_questions, :prize, :text
  end

  def self.down
    remove_column :fg_contest_questions, :prize
  end
end
