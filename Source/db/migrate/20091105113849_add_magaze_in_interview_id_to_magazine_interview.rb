class AddMagazeInInterviewIdToMagazineInterview < ActiveRecord::Migration
  def self.up
    add_column :article_images, :magazine_interview_id, :integer
  end

  def self.down
    remove_column :article_images, :magazine_interview_id
  end
end
