class AddLocalesToMagazineSection < ActiveRecord::Migration
  def self.up
    add_column :magazine_sections, :articles_in_flash_en, :integer
    add_column :magazine_sections, :cols_in_flash_en, :integer
  end

  def self.down
    remove_column :magazine_sections, :cols_in_flash_en
    remove_column :magazine_sections, :articles_in_flash_en
  end
end
