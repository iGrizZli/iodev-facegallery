class CreateMagazineInterviews < ActiveRecord::Migration
  def self.up
    create_table :magazine_interviews do |t|
      t.string :name
      t.string :title
      t.text :preface
      t.text :text
      t.integer :author_id

      t.timestamps
    end
  end

  def self.down
    drop_table :magazine_interviews
  end
end
