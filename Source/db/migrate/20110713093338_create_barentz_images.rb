class CreateBarentzImages < ActiveRecord::Migration
  def self.up
    create_table :barentz_images do |t|
      t.integer :barentz_article_id

      t.timestamps
    end
  end

  def self.down
    drop_table :barentz_images
  end
end
