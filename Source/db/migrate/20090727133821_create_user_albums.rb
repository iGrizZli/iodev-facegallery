class CreateUserAlbums < ActiveRecord::Migration
  def self.up
    create_table :user_albums do |t|
      t.integer :user_id
      t.string :title
      t.integer :order

      t.timestamps
    end
  end

  def self.down
    drop_table :user_albums
  end
end
