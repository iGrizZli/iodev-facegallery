class AddEnglishToPostSections < ActiveRecord::Migration
  def self.up
    add_column :post_sections, :name_en, :string
  end

  def self.down
    remove_column :post_sections, :name_en
  end
end
