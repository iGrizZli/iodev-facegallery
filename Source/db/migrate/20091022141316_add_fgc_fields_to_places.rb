class AddFgcFieldsToPlaces < ActiveRecord::Migration
  def self.up
    add_column :places, :fgc_discount, :string
    add_column :places, :fgc_text, :text
  end

  def self.down
    remove_column :places, :fgc_text
    remove_column :places, :fgc_discount
  end
end
