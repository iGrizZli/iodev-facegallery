class AddPlaceCategoryIdToPlaces < ActiveRecord::Migration
  def self.up
    add_column :places, :place_category_id, :integer
  end

  def self.down
    remove_column :places, :place_category_id
  end
end
