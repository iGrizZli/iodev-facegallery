class AddEventCategoryToDailyEvent < ActiveRecord::Migration
  def self.up
    add_column :daily_events, :event_category_id, :integer
  end

  def self.down
    remove_column :daily_events, :event_category_id
  end
end
