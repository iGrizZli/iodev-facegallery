class AddCachedRatingToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :cached_rating, :integer, :nil => false, :default => 0
  end

  def self.down
    remove_column :users, :cached_rating
  end
end
