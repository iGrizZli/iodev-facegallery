class CreateMagazineSections < ActiveRecord::Migration
  def self.up
    create_table :magazine_sections do |t|
      t.string :name
      t.integer :position
      t.string :controller
      t.boolean :active

      t.timestamps
    end
  end

  def self.down
    drop_table :magazine_sections
  end
end
