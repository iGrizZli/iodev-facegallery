class CreateReports < ActiveRecord::Migration
  def self.up
    create_table :reports do |t|
      t.string :name
      t.datetime :date
      t.text :description
      t.integer :place_id

      t.timestamps
    end
  end

  def self.down
    drop_table :reports
  end
end
