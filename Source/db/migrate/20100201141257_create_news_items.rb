class CreateNewsItems < ActiveRecord::Migration
  def self.up
    create_table :news_items do |t|
      t.string :title
      t.string :title_en
      t.text :text
      t.text :text_en
      t.boolean :fgc
      t.datetime :date
      t.boolean :published_ru
      t.boolean :published_en

      t.timestamps
    end
  end

  def self.down
    drop_table :news_items
  end
end
