class AddReadToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :read, :boolean
    add_index :messages, :user_id
    add_index :messages, :read
  end

  def self.down
    remove_index :messages, :user_id
    remove_index :messages, :read
    remove_column :messages, :read
  end
end
