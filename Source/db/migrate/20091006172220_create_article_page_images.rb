class CreateArticlePageImages < ActiveRecord::Migration
  def self.up
    create_table :article_page_images do |t|
      t.integer :position
      t.integer :magazine_article_id
      t.boolean :spread

      t.timestamps
    end
  end

  def self.down
    drop_table :article_page_images
  end
end
