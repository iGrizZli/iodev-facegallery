class AddAuthorNameToMagazineArticles < ActiveRecord::Migration
  def self.up
    add_column :magazine_articles, :author_name, :string
    add_column :magazine_interviews, :author_name, :string
  end

  def self.down
    remove_column :magazine_articles, :author_name
    remove_column :magazine_interviews, :author_name
  end
end
