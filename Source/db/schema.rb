# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120213020742) do

  create_table "admin_users", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "apple_questions", :force => true do |t|
    t.integer  "user_id"
    t.text     "question"
    t.text     "answer"
    t.boolean  "visible",    :default => true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "article_images", :force => true do |t|
    t.integer  "magazine_article_id"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "magazine_interview_id"
  end

  create_table "article_page_images", :force => true do |t|
    t.integer  "position"
    t.integer  "magazine_article_id"
    t.boolean  "spread"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "spread_image_file_name"
    t.string   "spread_image_content_type"
    t.integer  "spread_image_file_size"
    t.datetime "spread_image_updated_at"
    t.text     "text"
  end

  create_table "barentz_articles", :force => true do |t|
    t.string   "title"
    t.text     "text"
    t.boolean  "published"
    t.integer  "barentz_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "description"
  end

  create_table "barentz_categories", :force => true do |t|
    t.string   "title"
    t.string   "urlname"
    t.boolean  "visible"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "barentz_images", :force => true do |t|
    t.integer  "barentz_article_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "title"
  end

  create_table "best_bloggers", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "active"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "bio_categories", :force => true do |t|
    t.string   "name"
    t.string   "urlname"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name_en"
  end

  create_table "bios", :force => true do |t|
    t.string   "name"
    t.string   "surname"
    t.string   "occupation"
    t.integer  "bio_category_id"
    t.text     "text"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "quote"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "email"
    t.integer  "card_id"
    t.string   "name_en"
    t.string   "surname_en"
    t.string   "occupation_en"
    t.text     "text_en"
    t.string   "quote_en"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.boolean  "not_on_main_page"
    t.boolean  "englishman"
    t.date     "birthday"
    t.string   "gender",             :limit => 1
    t.string   "invite_hash"
    t.boolean  "sent_invite"
    t.integer  "manager_id"
    t.boolean  "private_maillist",                :default => false
    t.integer  "company_id"
    t.integer  "author_id"
  end

  add_index "bios", ["published_en"], :name => "index_bios_on_published_en"
  add_index "bios", ["published_ru"], :name => "index_bios_on_published_ru"

  create_table "blogs", :force => true do |t|
    t.string   "title"
    t.text     "text"
    t.integer  "visibility"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blogs", ["created_at"], :name => "index_blogs_on_created_at"
  add_index "blogs", ["user_id"], :name => "index_blogs_on_user_id"

  create_table "comments", :force => true do |t|
    t.string   "title",               :limit => 50, :default => ""
    t.text     "body"
    t.datetime "created_at",                                           :null => false
    t.integer  "commentable_id",                    :default => 0,     :null => false
    t.string   "commentable_type",                  :default => "",    :null => false
    t.integer  "user_id",                           :default => 0,     :null => false
    t.integer  "commentable_user_id"
    t.string   "subject"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.datetime "updated_at"
    t.boolean  "deleted",                           :default => false
  end

  add_index "comments", ["commentable_id"], :name => "index_comments_on_commentable_id"
  add_index "comments", ["commentable_type"], :name => "index_comments_on_commentable_type"
  add_index "comments", ["commentable_user_id"], :name => "index_comments_on_commentable_user_id"
  add_index "comments", ["parent_id"], :name => "index_comments_on_parent_id"
  add_index "comments", ["user_id"], :name => "fk_comments_user"

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contest_photos", :force => true do |t|
    t.integer  "contest_session_id"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "contest_pic_votes", :force => true do |t|
    t.integer  "contest_pic_id"
    t.string   "ip"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contest_pics", :force => true do |t|
    t.integer  "user_id"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "pic_file_name"
    t.string   "pic_content_type"
    t.integer  "pic_file_size"
    t.datetime "pic_updated_at"
  end

  create_table "contest_quote_votes", :force => true do |t|
    t.integer  "contest_quote_id"
    t.string   "ip"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contest_quotes", :force => true do |t|
    t.string   "quote"
    t.integer  "bio_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "winner",           :default => false
    t.integer  "quote_contest_id"
  end

  create_table "contest_sessions", :force => true do |t|
    t.string   "photographer_name"
    t.string   "model_name"
    t.text     "info"
    t.integer  "position"
    t.integer  "photographer_id"
    t.integer  "model_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "daily_events", :force => true do |t|
    t.string   "name"
    t.datetime "date"
    t.text     "text"
    t.integer  "place_id"
    t.boolean  "highlight"
    t.boolean  "published_ru",         :default => true
    t.boolean  "published_en"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_category_id"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.text     "long_text"
    t.boolean  "fgc"
    t.boolean  "johnywalker",          :default => false
  end

  create_table "daily_events_users", :id => false, :force => true do |t|
    t.integer "user_id",        :null => false
    t.integer "daily_event_id", :null => false
  end

  add_index "daily_events_users", ["user_id", "daily_event_id"], :name => "fk_user_daily_event"

  create_table "documents", :force => true do |t|
    t.string   "name"
    t.text     "text"
    t.string   "alias"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name_en"
    t.text     "text_en"
  end

  create_table "editors", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "emails", :force => true do |t|
    t.string   "from"
    t.string   "to"
    t.integer  "priority",          :default => 100
    t.integer  "last_send_attempt", :default => 0
    t.text     "mail"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "emails", ["priority", "id"], :name => "index_emails_on_priority_and_id"

  create_table "event_categories", :force => true do |t|
    t.string   "name"
    t.string   "name_en"
    t.string   "urlname"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "date"
    t.integer  "place_id"
    t.boolean  "picture"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "event_picture_file_name"
    t.string   "event_picture_content_type"
    t.integer  "event_picture_file_size"
    t.datetime "event_picture_updated_at"
    t.string   "name_en"
    t.text     "description_en"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.boolean  "fgc"
  end

  create_table "feedback_messages", :force => true do |t|
    t.string   "subject"
    t.string   "contacts"
    t.text     "text"
    t.string   "ip"
    t.string   "browser"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  create_table "fg_contest_questions", :force => true do |t|
    t.text     "text"
    t.text     "text_en"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "winner_image"
    t.text     "prize"
    t.integer  "winner_comment_id"
  end

  create_table "fg_contest_winners", :force => true do |t|
    t.integer  "fg_contest_question_id"
    t.integer  "comment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "fg_contest_winners", ["fg_contest_question_id"], :name => "index_fg_contest_winners_on_fg_contest_question_id"

  create_table "fg_guests", :force => true do |t|
    t.string   "short_text"
    t.integer  "blog_id"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "fg_recommendations", :force => true do |t|
    t.string   "section1",            :default => "MAGAZINE"
    t.string   "title1"
    t.string   "link1"
    t.string   "section2",            :default => "BIO"
    t.string   "title2"
    t.string   "link2"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image1_file_name"
    t.string   "image1_content_type"
    t.integer  "image1_file_size"
    t.datetime "image1_updated_at"
    t.string   "image2_file_name"
    t.string   "image2_content_type"
    t.integer  "image2_file_size"
    t.datetime "image2_updated_at"
  end

  create_table "fg_room_questions", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "belstaff"
    t.boolean  "andrew_mackenzie"
    t.boolean  "tom_rebel"
    t.boolean  "dkny"
    t.boolean  "ring"
    t.boolean  "john_richmond"
    t.boolean  "gaetano_navarra"
    t.boolean  "stone_island"
    t.boolean  "byblos"
    t.boolean  "john_varvatos"
    t.boolean  "golden_goose"
    t.boolean  "k_by_karl"
    t.boolean  "faith_connexion"
    t.boolean  "jc_de_castelbajac"
    t.boolean  "guliano_fujiwara"
    t.boolean  "bad_spirit"
    t.boolean  "marithe_francois_girbaud"
    t.boolean  "paul_joe"
    t.boolean  "bill_tornade"
    t.boolean  "blauer"
    t.boolean  "parasuco"
    t.boolean  "jan_carlos"
    t.boolean  "woolrich"
    t.boolean  "amen"
    t.boolean  "mauro_grifoni"
    t.boolean  "coast_weber_ahaus"
    t.boolean  "messagerie"
    t.boolean  "daniele_alessandrini"
    t.text     "other"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fg_survey_answers", :force => true do |t|
    t.integer  "user_id"
    t.string   "session_id"
    t.text     "answers"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fg_survey_questions", :force => true do |t|
    t.integer  "survey_id"
    t.integer  "position"
    t.text     "question"
    t.text     "options"
    t.boolean  "force_text_input"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "disable_checkbox"
    t.boolean  "textarea"
    t.boolean  "checkbox"
  end

  create_table "fgc_wishes", :force => true do |t|
    t.integer  "user_id"
    t.text     "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friend_invites", :force => true do |t|
    t.integer  "user_id",        :null => false
    t.integer  "user_id_target", :null => false
    t.string   "code"
    t.text     "message"
    t.boolean  "is_accepted"
    t.datetime "accepted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "givenchy_statuses", :force => true do |t|
    t.integer  "user_id"
    t.string   "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "magazine_articles", :force => true do |t|
    t.string   "title"
    t.text     "text"
    t.integer  "author_id"
    t.integer  "magazine_section_id"
    t.integer  "magazine_subsection_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "preview_file_name"
    t.string   "preview_content_type"
    t.integer  "preview_file_size"
    t.datetime "preview_updated_at"
    t.string   "title_en"
    t.text     "text_en"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.boolean  "flipping_article"
    t.string   "news_preview_file_name"
    t.string   "news_preview_content_type"
    t.integer  "news_preview_file_size"
    t.datetime "news_preview_updated_at"
    t.string   "author_name"
    t.integer  "magazine_issue_id"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.text     "summary"
  end

  create_table "magazine_interviews", :force => true do |t|
    t.string   "name"
    t.string   "title"
    t.text     "preface"
    t.text     "text"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "interview_photo_file_name"
    t.string   "interview_photo_content_type"
    t.integer  "interview_photo_file_size"
    t.datetime "interview_photo_updated_at"
    t.string   "name_en"
    t.text     "title_en"
    t.text     "preface_en"
    t.text     "text_en"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.text     "quote"
    t.text     "quote_en"
    t.string   "author_name"
    t.integer  "magazine_issue_id"
  end

  create_table "magazine_issues", :force => true do |t|
    t.string   "title"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "text_en"
  end

  create_table "magazine_sections", :force => true do |t|
    t.string   "name"
    t.integer  "position"
    t.string   "controller"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "urlname"
    t.integer  "articles_in_flash"
    t.integer  "cols_in_flash"
    t.integer  "picwidth"
    t.integer  "picheight"
    t.integer  "articles_in_flash_en"
    t.integer  "cols_in_flash_en"
  end

  create_table "magazine_subsections", :force => true do |t|
    t.string   "name"
    t.integer  "position"
    t.integer  "magazine_section_id"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "urlname"
  end

  create_table "messages", :force => true do |t|
    t.string   "subject"
    t.text     "text"
    t.integer  "user_id"
    t.integer  "sender_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "read"
    t.boolean  "deleted_by_user"
    t.boolean  "deleted_by_sender"
    t.boolean  "notify_by_email",   :default => false
  end

  add_index "messages", ["deleted_by_sender"], :name => "index_messages_on_deleted_by_sender"
  add_index "messages", ["read"], :name => "index_messages_on_read"
  add_index "messages", ["sender_id"], :name => "index_messages_on_sender_id"
  add_index "messages", ["user_id"], :name => "index_messages_on_user_id"

  create_table "news_items", :force => true do |t|
    t.string   "title"
    t.string   "title_en"
    t.text     "text"
    t.text     "text_en"
    t.boolean  "fgc"
    t.datetime "date"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "place_id"
  end

  create_table "no_discounts", :force => true do |t|
    t.integer  "bio_id"
    t.text     "place_name"
    t.text     "comments"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photo_tags", :force => true do |t|
    t.integer  "x"
    t.integer  "y"
    t.integer  "w"
    t.integer  "h"
    t.float    "x_rel"
    t.float    "y_rel"
    t.float    "w_rel"
    t.float    "h_rel"
    t.string   "displayname"
    t.integer  "tagger_id"
    t.boolean  "tag_confirmed",        :default => false
    t.integer  "photo_taggable_id"
    t.string   "photo_taggable_type"
    t.integer  "person_taggable_id"
    t.string   "person_taggable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "place_addresses", :force => true do |t|
    t.integer  "place_id"
    t.text     "address"
    t.text     "phone"
    t.string   "webpage"
    t.string   "food"
    t.string   "parking"
    t.string   "working_hours"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "place_categories", :force => true do |t|
    t.string   "name",       :null => false
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "urlname",    :null => false
    t.string   "name_en"
  end

  create_table "place_photos", :force => true do |t|
    t.integer  "place_id"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "places", :force => true do |t|
    t.string   "name",                                       :null => false
    t.text     "description"
    t.text     "address"
    t.text     "phone"
    t.string   "webpage"
    t.boolean  "fgc",                     :default => false, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "place_category_id"
    t.string   "main_photo_file_name"
    t.string   "main_photo_content_type"
    t.integer  "main_photo_file_size"
    t.datetime "main_photo_updated_at"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.string   "food"
    t.string   "working_hours"
    t.string   "parking"
    t.string   "name_en"
    t.text     "description_en"
    t.text     "address_en"
    t.text     "phone_en"
    t.string   "food_en"
    t.string   "working_hours_en"
    t.string   "parking_en"
    t.string   "fgc_discount"
    t.text     "fgc_text"
    t.string   "pano_url"
    t.string   "place_type"
  end

  create_table "post_sections", :force => true do |t|
    t.string   "name"
    t.integer  "position"
    t.string   "urlname"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name_en"
    t.boolean  "active",     :default => true
  end

  create_table "posts", :force => true do |t|
    t.string   "title"
    t.text     "text"
    t.integer  "user_id"
    t.integer  "post_section_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "preferences", :force => true do |t|
    t.string   "name",       :null => false
    t.integer  "owner_id",   :null => false
    t.string   "owner_type", :null => false
    t.integer  "group_id"
    t.string   "group_type"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "preferences", ["owner_id", "owner_type", "name", "group_id", "group_type"], :name => "index_preferences_on_owner_and_name_and_preference", :unique => true

  create_table "register_applications", :force => true do |t|
    t.string   "name"
    t.string   "surname"
    t.date     "birthday"
    t.string   "city"
    t.string   "email"
    t.string   "phone"
    t.string   "education"
    t.string   "work_area"
    t.string   "company"
    t.text     "achievements"
    t.text     "interests"
    t.text     "why"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "report_photos", :force => true do |t|
    t.integer  "report_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "report_photo_file_name"
    t.string   "report_photo_content_type"
    t.integer  "report_photo_file_size"
    t.datetime "report_photo_updated_at"
    t.integer  "position"
  end

  create_table "reports", :force => true do |t|
    t.string   "name"
    t.datetime "date"
    t.text     "description"
    t.integer  "place_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.string   "name_en"
    t.text     "description_en"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.boolean  "fgc"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "site_invites", :force => true do |t|
    t.integer  "user_id"
    t.string   "code"
    t.string   "email"
    t.boolean  "used",        :default => false
    t.integer  "new_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stream_categories", :force => true do |t|
    t.string  "slug"
    t.string  "title"
    t.integer "stream_id"
  end

  create_table "stream_post_section_photos", :force => true do |t|
    t.integer  "position"
    t.text     "title"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "stream_post_sections", :force => true do |t|
    t.string  "type"
    t.integer "post_id"
    t.integer "position"
    t.text    "text"
    t.text    "source_text"
    t.string  "link"
  end

  create_table "stream_posts", :force => true do |t|
    t.string   "slug"
    t.string   "title"
    t.text     "preview"
    t.boolean  "is_draft",           :default => true
    t.integer  "stream_id"
    t.integer  "stream_category_id"
    t.integer  "user_id"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.datetime "published_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "streams", :force => true do |t|
    t.string "slug"
    t.string "title"
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type"], :name => "index_taggings_on_taggable_id_and_taggable_type"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "trips", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.text     "schedule"
    t.text     "costs"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "country"
    t.boolean  "published_ru"
    t.boolean  "published_en"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trips_users", :id => false, :force => true do |t|
    t.integer "trip_id", :null => false
    t.integer "user_id", :null => false
  end

  add_index "trips_users", ["trip_id", "user_id"], :name => "index_trips_users_on_trip_id_and_user_id", :unique => true

  create_table "user_albums", :force => true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_gallery_photos", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "position"
    t.integer  "user_id"
    t.integer  "user_album_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "user_portfolio_photos", :force => true do |t|
    t.integer  "user_portfolio_id"
    t.string   "title"
    t.text     "description"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "user_id"
  end

  create_table "user_portfolios", :force => true do |t|
    t.integer  "user_id"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "login",                     :limit => 40
    t.string   "name",                      :limit => 100, :default => ""
    t.string   "email",                     :limit => 100
    t.string   "crypted_password",          :limit => 40
    t.string   "salt",                      :limit => 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token",            :limit => 40
    t.datetime "remember_token_expires_at"
    t.string   "activation_code",           :limit => 40
    t.datetime "activated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "city"
    t.string   "occupation"
    t.string   "education"
    t.string   "interests"
    t.string   "about"
    t.date     "birthday"
    t.string   "quote"
    t.string   "phone"
    t.string   "surname"
    t.string   "displayname"
    t.integer  "friend_count",                             :default => 0
    t.integer  "profile_access"
    t.integer  "friends_access"
    t.integer  "portfolio_access"
    t.integer  "gallery_access"
    t.integer  "blog_access"
    t.datetime "last_activity"
    t.string   "user_agent"
    t.integer  "show_birthday"
    t.string   "password_reset_code",       :limit => 40
    t.boolean  "no_message_notifications"
    t.text     "comments_view_times"
    t.boolean  "active_on_site"
    t.integer  "cached_rating",                            :default => 0
    t.datetime "bio_wished"
    t.string   "gender"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

  create_table "videos", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.text     "embed_code"
    t.integer  "user_id"
    t.integer  "bio_id"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "thumbnail_file_name"
    t.string   "thumbnail_content_type"
    t.integer  "thumbnail_file_size"
    t.datetime "thumbnail_updated_at"
  end

  add_index "videos", ["active"], :name => "index_videos_on_active"

  create_table "votes", :force => true do |t|
    t.boolean  "vote",          :default => false
    t.integer  "voteable_id",                      :null => false
    t.string   "voteable_type",                    :null => false
    t.integer  "voter_id"
    t.string   "voter_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["voteable_id", "voteable_type"], :name => "fk_voteables"
  add_index "votes", ["voter_id", "voter_type", "voteable_id", "voteable_type"], :name => "uniq_one_vote_only", :unique => true
  add_index "votes", ["voter_id", "voter_type"], :name => "fk_voters"

end
