  desc "FGRoom stats"
  task :collect_fgroom_stats => :environment do
    @columns = FgRoomQuestion.connection.select_rows("SELECT COLUMN_NAME FROM information_schema.`COLUMNS` C where table_name = 'fg_room_questions'");
    @columns.each do |col|
      count = FgRoomQuestion.connection.select_value("SELECT count(*) FROM fg_room_questions where #{col}='1'");
      puts col[0].to_s + ': ' + count
    end
    
  end